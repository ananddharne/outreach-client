import Vue from 'vue'
import Router from 'vue-router'
import Base from 'components/Base'
import Login from 'components/Login'
import Pagebuilder from 'components/Pagebuilder'
import Mobile from 'components/Mobile'
import Page404 from 'components/Page404'
import PageNotVerified from 'components/PageNotVerified'

import { metadataStore, adminStore } from 'stores'
import { modals } from 'helpers'
import api from 'api'

import CampaignsPanel from 'components/Sections/CampaignsPanel'
import CampaignsDetailsPanel from 'components/Sections/CampaignsDetailsPanel'
import JobsPanel from 'components/Sections/Jobs/JobsPanel'
import ListsPanel from 'components/Sections/ListsPanel'
import TemplatesPanel from 'components/Sections/TemplatesPanel'
import TemplatesDetailsPanel from 'components/Sections/TemplatesDetailsPanel'
import SequencesPanel from 'components/Sections/SequencesPanel'
import SequencesDetailsPanel from 'components/Sections/SequencesDetailsPanel'
import DebugPanel from 'components/Sections/DebugPanel'
import CandidateManager from 'components/Sections/CandidateManager'

// Sourcing
import AccountSetup from 'components/Sections/Sourcing/AccountSetup'
import SourcedReqsPanel from 'components/Sections/Sourcing/SourcedReqsPanel'

// Candidates
import CandidatesPanel from 'components/Sections/Candidates/CandidatesPanel'
import CandidatesProfilePanel from 'components/Sections/Candidates/CandidatesProfilePanel'
import CandidatesReview from 'components/Sections/Candidates/CandidatesReview'

// Find Candidates
import FindCandidatesBase from 'components/Sections/FindCandidates/FindCandidatesBase'
import FindCandidatesPanel from 'components/Sections/FindCandidates/FindCandidatesPanel'

// Reports
import ReportsBase from 'components/Sections/Reports/ReportsBase'
import JobsReports from 'components/Sections/Reports/JobsReports'
import CandidatesReports from 'components/Sections/Reports/CandidatesReports'
import CampaignsReports from 'components/Sections/Reports/CampaignsReports'
import SequencesReports from 'components/Sections/Reports/SequencesReports'
import SequencesDetailsReports from 'components/Sections/Reports/SequencesDetailsReports'
import PagesReports from 'components/Sections/Reports/PagesReports'

// Settings
import SettingsProfilePanel from 'components/Sections/Settings/SettingsProfilePanel'
import SettingsCompanyPanel from 'components/Sections/Settings/SettingsCompanyPanel'
import SettingsOrganizationPanel from 'components/Sections/Settings/SettingsOrganizationPanel'
import SettingsUsersPanel from 'components/Sections/Settings/SettingsUsersPanel'
import SettingsSignaturesPanel from 'components/Sections/Settings/SettingsSignaturesPanel'
import SettingsNotificationsPanel from 'components/Sections/Settings/SettingsNotificationsPanel'
import SettingsFieldsPanel from 'components/Sections/Settings/SettingsFieldsPanel'
import SettingsApprovalTimerPanel from 'components/Sections/Settings/SettingsApprovalTimerPanel'

import Unsubscribe from 'components/Unsubscribe'
import Stirista from 'components/Stirista'

// Inbox
import InboxBase from 'components/Sections/Inbox/InboxBase'

Vue.use(Router)

let appRouter = new Router({
  mode: 'hash',
  routes: [
    {
      path: '/',
      redirect: '/login'
    },
    {
      path: '/login',
      component: Login
    },
    {
      path: '/login/:action',
      component: Login
    },
    {
      path: '/outreach',
      redirect: '/outreach/candidate-manager'
    },
    {
      path: '/outreach/jobs',
      component: Base,
      children: [{
        path: '',
        component: JobsPanel
      }]
    },
    {
      path: '/outreach/jobs/:job_id',
      component: Base,
      children: [{
        path: '',
        component: JobsPanel
      }]
    },
    {
      path: '/outreach/account-setup',
      component: Base,
      children: [{
        path: '',
        component: AccountSetup
      }]
    },
    {
      path: '/outreach/sourced-reqs',
      component: Base,
      children: [{
        path: '',
        component: SourcedReqsPanel
      }]
    },
    {
      path: '/outreach/sourced-reqs/:req_id/candidates',
      component: Base,
      children: [{
        name: 'sourced_candidates',
        path: '',
        component: CandidatesPanel
      }]
    },
    {
      path: '/outreach/sourced-reqs/:job_id',
      component: Base,
      children: [{
        path: '',
        component: SourcedReqsPanel
      }]
    },
    {
      path: '/outreach/candidates',
      component: Base,
      children: [{
        name: 'candidates_panel',
        path: '',
        component: CandidatesPanel
      }]
    },
    {
      path: '/outreach/candidate-manager',
      component: Base,
      children: [{
        path: '',
        component: CandidateManager
      }]
    },
    {
      path: '/outreach/candidate-review',
      component: Base,
      children: [{
        path: '',
        name: 'candidates_review',
        component: CandidatesReview
      }]
    },
    {
      path: '/outreach/candidates/candidate-details/:candidate_id',
      component: Base,
      children: [{
        path: '',
        component: CandidatesProfilePanel
      }]
    },
    {
      path: '/outreach/lists',
      component: Base,
      children: [{
        path: '',
        component: ListsPanel
      }]
    },
    {
      path: '/outreach/lists/new',
      component: Base,
      children: [{
        path: '',
        component: ListsPanel
      }]
    },
    {
      path: '/outreach/templates',
      component: Base,
      children: [{
        path: '',
        component: TemplatesPanel
      }]
    },
    {
      path: '/outreach/sequences',
      component: Base,
      children: [{
        path: '',
        component: SequencesPanel
      }]
    },
    {
      path: '/outreach/sequences/:sequence_id',
      components: {
        default: Base
      },
      children: [{
        path: '',
        component: SequencesDetailsPanel
      }]
    },
    {
      path: '/outreach/campaigns',
      component: Base,
      children: [{
        path: '',
        component: CampaignsPanel
      }]
    },
    {
      path: '/outreach/campaigns/:campaign_id',
      components: {
        default: Base
      },
      children: [{
        path: '',
        component: CampaignsDetailsPanel
      }]
    },
    {
      path: '/outreach/templates',
      component: Base,
      children: [{
        path: '',
        component: TemplatesPanel
      }]
    },
    {
      path: '/outreach/templates/new',
      components: {
        default: Base
      },
      children: [{
        path: '',
        component: TemplatesDetailsPanel
      }]
    },
    {
      path: '/outreach/templates/:template_id',
      components: {
        default: Base
      },
      children: [{
        path: '',
        component: TemplatesDetailsPanel
      }]
    },
    {
      path: '/outreach/debug',
      component: Base,
      children: [{
        path: '',
        component: DebugPanel
      }]
    },
    {
      path: '/reports',
      redirect: '/reports/jobs'
    },
    {
      path: '/reports/jobs',
      components: {
        default: ReportsBase
      },
      children: [{
        path: '',
        component: JobsReports
      }]
    },
    {
      path: '/reports/jobs/applicants/:job_id',
      components: {
        default: ReportsBase
      },
      children: [{
        name: 'reports_job_applicants',
        path: '',
        component: CandidatesPanel
      }]
    },
    {
      path: '/reports/candidates',
      redirect: '/reports/candidates/funnel'
    },
    {
      path: '/reports/candidates/:report_type',
      components: {
        default: ReportsBase
      },
      children: [{
        path: '',
        component: CandidatesReports
      }]
    },
    {
      path: '/reports/candidates/snapshot/:status',
      components: {
        default: ReportsBase
      },
      children: [{
        name: 'reports_candidates_snapshot',
        path: '',
        component: CandidatesPanel
      }]
    },
    {
      path: '/reports/candidates/funnel/:status',
      components: {
        default: ReportsBase
      },
      children: [{
        name: 'reports_candidates_funnel',
        path: '',
        component: CandidatesPanel
      }]
    },
    {
      path: '/reports/campaigns',
      components: {
        default: ReportsBase
      },
      children: [{
        path: '',
        component: CampaignsReports
      }]
    },
    {
      path: '/reports/sequences',
      components: {
        default: ReportsBase
      },
      children: [{
        path: '',
        component: SequencesReports
      }]
    },
    {
      path: '/reports/sequences/:sequence_id',
      components: {
        default: ReportsBase
      },
      children: [{
        path: '',
        component: SequencesDetailsReports
      }]
    },
    {
      path: '/reports/sequences/:sequence_id/:step_id/:status',
      components: {
        default: ReportsBase
      },
      children: [{
        path: '',
        component: CandidatesPanel,
        name: 'reports_sequence_candidates'
      }]
    },
    {
      path: '/reports/pages',
      components: {
        default: ReportsBase
      },
      children: [{
        path: '',
        component: PagesReports
      }]
    },
    {
      path: '/reports/pages/:page_id/:status',
      components: {
        default: ReportsBase
      },
      children: [{
        path: '',
        component: CandidatesPanel,
        name: 'reports_pages_candidates'
      }]
    },
    {
      path: '/find-candidates',
      components: {
        default: FindCandidatesBase
      },
      children: [{
        path: '',
        component: FindCandidatesPanel
      }]
    },
    {
      path: '/outreach/settings',
      component: Base,
      children: [
        { path: '', redirect: 'profile' },
        { path: 'profile', component: SettingsProfilePanel },
        { path: 'company', component: SettingsCompanyPanel },
        { path: 'organization', component: SettingsOrganizationPanel },
        { path: 'users', component: SettingsUsersPanel },
        { path: 'signatures', component: SettingsSignaturesPanel },
        { path: 'notifications', component: SettingsNotificationsPanel },
        { path: 'fields', component: SettingsFieldsPanel },
        { path: 'approval-timer', component: SettingsApprovalTimerPanel }
      ]
    },
    {
      path: '/pagebuilder',
      component: Pagebuilder
    },
    {
      path: '/pagebuilder/:page_id',
      component: Pagebuilder
    },
    {
      path: '/inbox',
      component: InboxBase
    },
    {
      path: '/inbox/:conversation_id',
      component: InboxBase
    },
    // If the hash contains the access_token parameter, assume it is trying to authenticate with auth0
    {
      path: '/(access_token=.*)',
      component: Login
    },
    {
      path: '/(error=.*)',
      component: Login
    },
    // When clicking unsubscribe link
    {
      path: '/unsubscribe',
      component: Unsubscribe
    },
    {
      path: '/unsubscribe/:page',
      component: Unsubscribe
    },
    // When an email is not verified
    {
      path: '/not-verified',
      component: PageNotVerified
    },
    {
      path: '/mobile',
      component: Mobile
    },
    {
      path: '/stirista',
      component: Stirista
    },
    // Anything else that doesn't match will be sent to the 404 page
    {
      path: '*',
      component: Page404
    }
  ]
})

// Don't allow navigation if there are unsaved changes.
// But if user confirms it's okay to navigate, clear flag for unsaved changes and perform navigation
appRouter.beforeEach((to, from, next) => {
  // Only allow sourced req admins to go to stirista import
  let storeData = adminStore.get()
  if (!storeData.is_system_admin && to.path == '/stirista') {
    let apiInterval = setInterval(() => {
      if (Object.keys(api.getActiveRequests()).length < 1) {
        clearInterval(apiInterval)
        storeData = adminStore.get()
        storeData.is_system_admin ? next() : next('/404')
      }
    }, 100)
  }

  if (metadataStore.getHasUnsavedChanges() && !metadataStore.getDebugMode()) {
    modals.show({
      component: 'Confirm',
      props: {
        message: 'You currently have unsaved changes. Do you want to save your changes before you leave?',
        confirm () {
          metadataStore.runSaveChangesFunction()
          metadataStore.setHasUnsavedChanges(false)

          // Pretty crappy way to wait until requests finish saving (would be much better with promises),
          // but had to be retrofitted when we introduced saving on route change
          let apiInterval = setInterval(() => {
            if (Object.keys(api.getActiveRequests()).length < 1) {
              clearInterval(apiInterval)
              next()
            }
          }, 100)
        },
        cancel () {
          metadataStore.setHasUnsavedChanges(false)
          next()
        }
      }

    })
  } else {
    next()
  }
})

// Update intercom after a route navigation
appRouter.afterEach((to, from) => {
  window.Intercom('update')
})

export default appRouter
