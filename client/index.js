import 'babel-polyfill'
import './promise-polyfill'
import { app } from './app'

app.$mount('#app')
