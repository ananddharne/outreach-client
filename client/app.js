import Vue from 'vue'
import App from './components/App'
import router from './router/router.js'
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'
import MobileDetect from 'mobile-detect'

// Plugins
import './lib/froala/main.js'
import './lib/froala/plugins.js'
import VueGmaps from 'vue-gmaps'

// Initialize the googlemaps api
Vue.use(VueGmaps, {
  key: 'AIzaSyC_EetH9JpwSXo8IN3kllzv-vK8kjvp7VE'
})

// Define custom directives

// By default our svg-inliner includes quotes around svgs so they'll work with CSS, but in our template
// syntax by default they'll already get quotes, so this removes them and sets the element's src
Vue.directive('svg-src', function (el, binding) {
  let result = binding.value.slice(1, -1)
  el.src = result
})

// v-show uses the 'display' CSS rule, which sets a hidden element's height to 0 and can cause page jumping in certain circumstances. By using `visibility` instead, we can preserve its location in the DOM. NOTE: Gets fucky when the element or children has a transition rule, must specify with a CSS rule.
Vue.directive('visible', function (el, binding) {
  el.style.visibility = binding.value ? 'visible' : 'hidden'
})

// If we are on prod, disable the vue dev tools and initialize sentry
if (window.location.host == 'talent.gethappie.me') {
  Vue.config.devtools = false
  Vue.config.debug = false
  Vue.config.silent = true

  const ravenOptions = {
    // Adapted from https://gist.github.com/impressiver/5092952
    ignoreErrors: [
      // Random plugins/extensions
      'top.GLOBALS',
      'originalCreateNotification',
      'canvas.contentDocument',
      'atomicFindClose',
      // Facebook borked
      'fb_xd_fragment',
      // ISP "optimizing" proxy - `Cache-Control: no-transform` seems to
      // reduce this. (thanks @acdha)
      // See http://stackoverflow.com/questions/4113268
      'bmi_SafeAddOnload',
      'EBCallBackMessageReceived',
      // See http://toolbar.conduit.com/Developer/HtmlAndGadget/Methods/JSInjection.aspx
      'conduitPage',
      // Seems to happen when sourcers use private browsing to log in to multiple accounts,
      // or when people try to use their phones.
      // See https://stackoverflow.com/a/42467951/6646559
      'QuotaExceededError'
    ],
    ignoreUrls: [
      // Facebook flakiness
      /graph\.facebook\.com/i,
      // Facebook blocked
      /connect\.facebook\.net\/en_US\/all\.js/i,
      // Chrome extensions
      /extensions\//i,
      /^chrome:\/\//i,
      // Firefox extensions
      /^resource:\/\//i
    ]
  }

  Raven
  .config('https://7ab85314dbab4a5fa2f4b599aec782a7@sentry.io/209844', ravenOptions)
  .addPlugin(RavenVue, Vue)
  .install()
}

const app = new Vue({
  router,
  ...App
})

// If we're on mobile, whine about it.
const UA = new MobileDetect(window.navigator.userAgent)
if (UA.phone() && !~window.location.href.indexOf('unsubscribe')) app.$router.push({ path: '/mobile' })

export { app, router }
