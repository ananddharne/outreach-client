let pagesStore = {

  store: {
    pages: []
  },

  init (initArray) {
    this.store.pages = initArray
  },

  get () {
    return this.store
  },

  // Pass an array of pages
  addPages (newPages) {
    // Only add new pages if there is length, i.e. the store has been initiated
    if (this.store.pages.length) {
      this.store.pages = this.store.pages.concat(newPages)
    }
  }

}

export default pagesStore
