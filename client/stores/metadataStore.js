// This store is meant for sharing internal state and should never be reported back to the server

let metadataStore = {

  store: {
    hasUnsavedChanges: false,
    saveChangesFunction: null,
    debugMode: false,
    dayOptions: [
      { label: 'Sunday', value: 0 },
      { label: 'Monday', value: 1 },
      { label: 'Tuesday', value: 2 },
      { label: 'Wednesday', value: 3 },
      { label: 'Thursday', value: 4 },
      { label: 'Friday', value: 5 },
      { label: 'Saturday', value: 6 }
    ],
    timeOptions: [
      { label: '12:00AM', value: 0 },
      { label: '1:00AM', value: 1 },
      { label: '2:00AM', value: 2 },
      { label: '3:00AM', value: 3 },
      { label: '4:00AM', value: 4 },
      { label: '5:00AM', value: 5 },
      { label: '6:00AM', value: 6 },
      { label: '7:00AM', value: 7 },
      { label: '8:00AM', value: 8 },
      { label: '9:00AM', value: 9 },
      { label: '10:00AM', value: 10 },
      { label: '11:00AM', value: 11 },
      { label: '12:00PM', value: 12 },
      { label: '1:00PM', value: 13 },
      { label: '2:00PM', value: 14 },
      { label: '3:00PM', value: 15 },
      { label: '4:00PM', value: 16 },
      { label: '5:00PM', value: 17 },
      { label: '6:00PM', value: 18 },
      { label: '7:00PM', value: 19 },
      { label: '8:00PM', value: 20 },
      { label: '9:00PM', value: 21 },
      { label: '10:00PM', value: 22 },
      { label: '11:00PM', value: 23 }
    ],
    filterSources: [
      { value: '', label: 'Filter by Source' },
      { value: 'happie', label: 'Happie Database' },
      { value: 'import', label: 'Imported from a file' },
      { value: 'linkedin', label: 'LinkedIn' },
      { value: 'career_builder', label: 'CareerBuilder' },
      { value: 'facebook', label: 'Facebook' },
      { value: 'user_added', label: 'Added by a user' },
      { value: 'applied_on_page', label: 'Applied on a page' },
      { value: 'purchased_happie', label: 'Added from Happie' }
    ],
    importSources: [
      { value: '', label: 'Select a Source...' },
      { value: 'happie', label: 'Happie Database' },
      { value: 'import', label: 'Imported from a file' },
      { value: 'linkedin', label: 'LinkedIn' },
      { value: 'career_builder', label: 'CareerBuilder' },
      { value: 'facebook', label: 'Facebook' }
    ],
    filterStatuses: [ // But also just regular statuses for when you're editing a candidate's status. But not when you're a transactional user. That's the other one.
      { value: '', label: 'Filter by Status' },
      { value: 'new', label: 'New' },
      { value: 'outreach', label: 'In Outreach' },
      { value: 'pending_enhance', label: 'Pending Enhance' },
      { value: 'pending', label: 'Pending' },
      { value: 'interested', label: 'Interested' },
      { value: 'applied', label: 'Applied' },
      { value: 'screening', label: 'Screening' },
      { value: 'interviewing', label: 'Interviewing' },
      { value: 'hired', label: 'Hired' },
      { value: 'keep_in_touch', label: 'Keep In Touch' },
      { value: 'inactive', label: 'Inactive' },
      { value: 'passed', label: 'Passed' },
      { value: 'filtered', label: 'Filtered' },
      { value: 'archived', label: 'Archived' }
    ],
    transactionalFilterStatuses: [
      { value: '', label: 'Filter by Status' },
      { value: 'interested', label: 'Interested' },
      // { value: 'applied', label: 'Applied' },
      { value: 'screening', label: 'Screening' },
      { value: 'interviewing', label: 'Interviewing' },
      { value: 'hired', label: 'Hired' },
      { value: 'archived', label: 'Archived' }
    ],
    sourcedReqAccountStatuses: [
      { value: null, label: 'Account Status' },
      { value: 'None', label: 'None' },
      { value: 'Tier 1', label: 'Tier 1' },
      { value: 'Red Alert', label: 'Red Alert' }
    ],
    sourcedReqStatuses: [
      { value: 'Needs more candidates', label: 'Needs more candidates' },
      { value: 'Needs SMS Campaign', label: 'Needs SMS Campaign' },
      { value: 'Needs InMail Campaign', label: 'Needs InMail Campaign' },
      { value: 'Needs Craigslist', label: 'Needs Craigslist' },
      { value: 'One Month', label: 'One Month' }
    ],
    sourcedReqStates: [
      { value: null, label: 'Sourced Req Status' },
      { value: 'New', label: 'New' },
      { value: 'Active', label: 'Active' },
      { value: 'Paused', label: 'Paused' },
      { value: 'Closed', label: 'Closed' }
    ],
    syndicationPriorityLevels: [
      { value: null, label: 'Job Board Priority Level' },
      { value: 'Low', label: 'Low' },
      { value: 'Medium', label: 'Medium' },
      { value: 'High', label: 'High' }
    ],
    jobFunctions: [
      {
        'value': 'select',
        'label': 'Select a Job Function'
      },
      {
        'value': 'Accounting',
        'label': 'Accounting'
      },
      {
        'value': 'Administrative',
        'label': 'Administrative'
      },
      {
        'value': 'Arts and Design',
        'label': 'Arts and Design'
      },
      {
        'value': 'Business Development',
        'label': 'Business Development'
      },
      {
        'value': 'Community & Social Services',
        'label': 'Community & Social Services'
      },
      {
        'value': 'Consulting',
        'label': 'Consulting'
      },
      {
        'value': 'Education',
        'label': 'Education'
      },
      {
        'value': 'Engineering',
        'label': 'Engineering'
      },
      {
        'value': 'Entrepreneurship',
        'label': 'Entrepreneurship'
      },
      {
        'value': 'Finance',
        'label': 'Finance'
      },
      {
        'value': 'Healthcare Services',
        'label': 'Healthcare Services'
      },
      {
        'value': 'Human Resources',
        'label': 'Human Resources'
      },
      {
        'value': 'Information Technology',
        'label': 'Information Technology'
      },
      {
        'value': 'Legal',
        'label': 'Legal'
      },
      {
        'value': 'Marketing',
        'label': 'Marketing'
      },
      {
        'value': 'Media & Communications',
        'label': 'Media & Communications'
      },
      {
        'value': 'Military & Protective Services',
        'label': 'Military & Protective Services'
      },
      {
        'value': 'Operations',
        'label': 'Operations'
      },
      {
        'value': 'Product Management',
        'label': 'Product Management'
      },
      {
        'value': 'Program & Product Management',
        'label': 'Program & Product Management'
      },
      {
        'value': 'Purchasing',
        'label': 'Purchasing'
      },
      {
        'value': 'Quality Assurance',
        'label': 'Quality Assurance'
      },
      {
        'value': 'Real Estate',
        'label': 'Real Estate'
      },
      {
        'value': 'Research',
        'label': 'Research'
      },
      {
        'value': 'Sales',
        'label': 'Sales'
      },
      {
        'value': 'Support',
        'label': 'Support'
      }
    ],
    environment: '',
    url_base: '',
    build_sha: '',
    buildMismatch: false
  },

  setEnvironment (loginResponse) {
    // Save to local storage so pagebuilder can use it
    localStorage.setItem('url_base', loginResponse.url_base)

    // Set internal state for use in outreach client
    this.store.url_base = loginResponse.url_base
    this.store.environment = loginResponse.environment.toLowerCase()
    this.store.build_sha = loginResponse.build_sha
  },

  get () {
    return this.store
  },

  set (key, value) {
    this.store[key] = value
  },

  getHasUnsavedChanges () {
    return this.store.hasUnsavedChanges
  },
  getSaveOptions () {
    // Helper to pass params to save functions when redirecting and saving changes
    // like onSubmit functions expecting an event that can call preventDefault()
    return {
      preventDefault () {
        return null
      },
      ignoreRedirect: true
    }
  },
  setHasUnsavedChanges (flag, saveFunction) {
    this.store.hasUnsavedChanges = flag

    // If save function is set, call it when leaving
    if (saveFunction) {
      this.setSaveChangesFunction(saveFunction)
    }
  },
  setSaveChangesFunction (saveFunction) {
    this.saveChangesFunction = () => {
      let options = this.getSaveOptions()
      saveFunction(options)
    }
  },

  runSaveChangesFunction () {
    if (this.saveChangesFunction) {
      this.saveChangesFunction()
    } else {
      console.warn('Save changes function not defined')
    }
  },

  getDebugMode () {
    return this.store.debugMode
  },
  setDebugMode (flag) {
    this.store.debugMode = flag
  }
}

export default metadataStore
