let sourcedReqStore = {
  store: {
    sourcedReqs: [],
    filters: {
      sourcer: 0,
      sales_person: 0,
      account_manager: 0,
      sourcing_statuses: null,
      account_statuses: '',
      req_statuses: '',
      engagement_date_start: null,
      engagement_date_end: null,
      active: '',
      search: '',
      offset: 0,
      limit: 10
    },
    totalCount: 0
  },
  init (initArray) {
    this.store.sourcedReqs = initArray
  },
  get () {
    return this.store
  },
  // Pass an array of sourced reqs
  add (newSourcedReqs) {
    this.store.sourcedReqs = this.store.sourcedReqs.concat(newSourcedReqs)
  },
  addComment (comment, sourcedReq) {
    $.each(this.store.sourcedReqs, (key, value) => {
      if (value.id === sourcedReq.id) {
        value.comments.push(comment)
        value.comment_count++
        return false
      }
    })
  },
  editComment (comment, sourcedReq) {
    $.each(this.store.sourcedReqs, (key, value) => {
      if (value.id === sourcedReq.id) {
        $.each(value.comments, (k, v) => {
          if (v.id === comment.id) {
            v.comment = comment.comment
            return false
          }
        })
        return false
      }
    })
  },

  removeComment (comment, sourcedReq) {
    $.each(this.store.sourcedReqs, (key, value) => {
      if (value.id === sourcedReq.id) {
        $.each(value.comments, (k, v) => {
          if (v.id === comment.id) {
            value.comments.splice(k, 1)
            value.comment_count--
            return false
          }
        })
        return false
      }
    })
  },
  // Pass a sourced req
  update (updatedSourcedReq) {
    $.each(this.store.sourcedReqs, (key, value) => {
      if (updatedSourcedReq.id === value.id) {
        $.each(updatedSourcedReq, (key) => {
          value[key] = updatedSourcedReq[key]
        })
      }
    })
  },
  clearFilters () {
    this.store.filters = {
      sourcer: '',
      salesperson: '',
      accountManager: '',
      reqStatus: '',
      submitDueDate: null,
      engagementDueDate: null,
      offset: 0,
      limit: 10
    }
  },
  deleteJob (id) {
    $.each(this.store.sourcedReqs, (key, value) => {
      if (value.req === id) {
        console.log('found it')
        value.req = null
        value.active = false
      }
    })
  }
}

export default sourcedReqStore
