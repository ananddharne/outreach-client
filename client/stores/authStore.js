let authStore = {
  setToken (token) {
    localStorage.setItem('hap_token', token)
  },
  getToken () {
    return localStorage.getItem('hap_token')
  },
  setAdminToken (token) {
    return localStorage.setItem('admin_token', token)
  },
  getAdminToken () {
    return localStorage.getItem('admin_token')
  },
  removeAdminToken () {
    localStorage.removeItem('admin_token')
  },
  logout () {
    localStorage.removeItem('hap_token')
  },
  setRedirectPath (path) {
    localStorage.setItem('hap_login_redirect', path)
  },
  getRedirectPath () {
    return localStorage.getItem('hap_login_redirect')
  },
  resetRedirectPath () {
    localStorage.removeItem('hap_login_redirect')
  },
  setInviteToken (token) {
    localStorage.setItem('hap_invite_token', token)
  },
  getInviteToken () {
    return localStorage.getItem('hap_invite_token')
  },
  setAuth0Token (token) {
    localStorage.setItem('auth0_token', token)
  },
  getAuth0Token () {
    return localStorage.getItem('auth0_token')
  }
}

export default authStore
