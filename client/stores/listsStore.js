let listsStore = {

  store: {
    lists: []
  },

  init (initArray) {
    this.store.lists = initArray
  },

  get () {
    return this.store
  }

}

export default listsStore
