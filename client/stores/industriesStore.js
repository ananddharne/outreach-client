let industriesStore = {

  store: {
    industries: []
  },

  get () {
    return this.store
  },
  init (initObject) {
    initObject.unshift({ value: 0, label: 'Select Industry' })
    this.store.industries = initObject
  }

}

export default industriesStore
