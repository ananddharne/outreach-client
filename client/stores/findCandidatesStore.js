let findCandidatesStore = {

  store: {
    candidates: [],
    filters: {
      locations: [],
      location_radius: 100,
      titles: [],
      current_title: false,
      companies: [],
      current_company: false,
      role_duration: [],
      experience_duration: [],
      minimum_degree: false,
      fields_of_study: [],
      institutions: [],
      offset: 0
    },
    query: {
      q: '',
      total_count: 0
    }
  },

  get () {
    return this.store
  },

  set (key, value) {
    if (key) {
      this.store.filters[key] = value
    }
  }

}

export default findCandidatesStore
