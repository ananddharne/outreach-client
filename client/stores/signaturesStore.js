let signaturesStore = {

  store: {
    signatures: []
  },

  get () {
    return this.store
  },

  init (initObject) {
    this.store.signatures = initObject
  }

}

export default signaturesStore
