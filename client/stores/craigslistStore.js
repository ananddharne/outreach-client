let craigslistLocationStore = {
  store: {
    locations: [
      {
        country: 'Canada',
        state: 'Alberta',
        city: 'Calgary',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'clg'
      },
      {
        country: 'Canada',
        state: 'Alberta',
        city: 'Edmonton',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'edm'
      },
      {
        country: 'Canada',
        state: 'Alberta',
        city: 'Ft Mcmurray',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'fmc'
      },
      {
        country: 'Canada',
        state: 'Alberta',
        city: 'Lethbridge',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'lth'
      },
      {
        country: 'Canada',
        state: 'Alberta',
        city: 'Medicine Hat',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'hat'
      },
      {
        country: 'Canada',
        state: 'Alberta',
        city: 'Peace Country',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'pax'
      },
      {
        country: 'Canada',
        state: 'Alberta',
        city: 'Red Deer',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'red'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Cariboo',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'cbo'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Comox Valley',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'cmx'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Fraser Valley',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'abb'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Kamloops',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'kml'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Kelowna',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'kel'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Nanaimo',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'nmo'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Prince George',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'yxs'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Skeena-Bulkley',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'ske'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Sunshine Coast',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'sun'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Vancouver',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'van'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Victoria',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'vic'
      },
      {
        country: 'Canada',
        state: 'British Columbia',
        city: 'Whistler/ Squamish',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'whi'
      },
      {
        country: 'Canada',
        state: 'Manitoba',
        city: 'Winnipeg',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'win'
      },
      {
        country: 'Canada',
        state: 'New Brunswick',
        city: 'New Brunswick',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'nbw'
      },
      {
        country: 'Canada',
        state: 'Newfoundland and Labrador',
        city: "St John'S",
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'nfl'
      },
      {
        country: 'Canada',
        state: 'Northwest Territories',
        city: 'Territories',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'toc'
      },
      {
        country: 'Canada',
        state: 'Northwest Territories',
        city: 'Yellowknife',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'ykf'
      },
      {
        country: 'Canada',
        state: 'Nova Scotia',
        city: 'Halifax',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'hfx'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Barrie',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'brr'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Belleville',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'bel'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Brantford',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'bfd'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Chatham-Kent',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'chk'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Cornwall',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'ycc'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Guelph',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'gph'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Hamilton',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'hml'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Kingston',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'kng'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Kitchener',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'kch'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Kootenays',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'koo'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'London',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'lon'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Niagara Region',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'nsc'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Ottawa',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'ott'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Owen Sound',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'ows'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Peterborough',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'ypq'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Sarnia',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'srn'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Sault Ste Marie',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'soo'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Sudbury',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'sud'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Thunder Bay',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'tby'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Toronto',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'tor'
      },
      {
        country: 'Canada',
        state: 'Ontario',
        city: 'Windsor',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'wsr'
      },
      {
        country: 'Canada',
        state: 'Prince Edward Island',
        city: 'Pei',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'pei'
      },
      {
        country: 'Canada',
        state: 'Quebec',
        city: 'Montreal',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mon'
      },
      {
        country: 'Canada',
        state: 'Quebec',
        city: 'Quebec',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'qbc'
      },
      {
        country: 'Canada',
        state: 'Quebec',
        city: 'Saguenay',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'sgy'
      },
      {
        country: 'Canada',
        state: 'Quebec',
        city: 'Sherbrooke',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'shb'
      },
      {
        country: 'Canada',
        state: 'Quebec',
        city: 'Trois-Rivieres',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'trs'
      },
      {
        country: 'Canada',
        state: 'Saskatchewan',
        city: 'Regina',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'reg'
      },
      {
        country: 'Canada',
        state: 'Saskatchewan',
        city: 'Saskatoon',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'skt'
      },
      {
        country: 'Canada',
        state: 'Yukon',
        city: 'Whitehorse',
        subMarket: '',
        craigslistMediaFee: '$0',
        value: 'whh'
      },
      {
        country: 'USA',
        state: 'Alabama',
        city: 'Auburn',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'aub'
      },
      {
        country: 'USA',
        state: 'Alabama',
        city: 'Birmingham',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'bhm'
      },
      {
        country: 'USA',
        state: 'Alabama',
        city: 'Dothan',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'dhn'
      },
      {
        country: 'USA',
        state: 'Alabama',
        city: 'Gadsden',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'anb'
      },
      {
        country: 'USA',
        state: 'Alabama',
        city: 'Huntsville',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'hsv'
      },
      {
        country: 'USA',
        state: 'Alabama',
        city: 'Mobile',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'mob'
      },
      {
        country: 'USA',
        state: 'Alabama',
        city: 'Montgomery',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mgm'
      },
      {
        country: 'USA',
        state: 'Alabama',
        city: 'The Shoals',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'msl'
      },
      {
        country: 'USA',
        state: 'Alabama',
        city: 'Tuscaloosa',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'tsc'
      },
      {
        country: 'USA',
        state: 'Alaska',
        city: 'Anchorage',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'anc'
      },
      {
        country: 'USA',
        state: 'Alaska',
        city: 'Fairbanks',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'fai'
      },
      {
        country: 'USA',
        state: 'Alaska',
        city: 'Kenai',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ena'
      },
      {
        country: 'USA',
        state: 'Alaska',
        city: 'Southeast Ak',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'jnu'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Flagstaff',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'flg'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Mohave Co',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mhv'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Prescott',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'prc'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Show Low',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'sow'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Sierra Vista',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'fhu'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Tucson',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'tus'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Yuma',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'yum'
      },
      {
        country: 'USA',
        state: 'Arkansas',
        city: 'Fayetteville',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'fyv'
      },
      {
        country: 'USA',
        state: 'Arkansas',
        city: 'Fort Smith',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'fsm'
      },
      {
        country: 'USA',
        state: 'Arkansas',
        city: 'Jonesboro',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'jbr'
      },
      {
        country: 'USA',
        state: 'Arkansas',
        city: 'Little Rock',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'lit'
      },
      {
        country: 'USA',
        state: 'Arkansas',
        city: 'Texarkana',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'txk'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Bakersfield',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'bak'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Chico',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'chc'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Fresno',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'fre'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Gold Country',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'gld'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Hanford',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'hnf'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Humboldt',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'hmb'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Imperial Co',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'imp'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Inland Empire',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'inl'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Mendocino Co',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mdo'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Merced',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'mer'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Modesto',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'mod'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Monterey',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'mtb'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Palm Springs',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'psp'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Redding',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'rdd'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'San Luis Obispo',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'slo'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Santa Barbara',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'sba'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Santa Maria',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'smx'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Siskiyou Co',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ssk'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Stockton',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'stk'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Susanville',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ssn'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Ventura',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'oxr'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Visalia-Tulare',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'vis'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Yuba-Sutter',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'ybs'
      },
      {
        country: 'USA',
        state: 'Colorado',
        city: 'Boulder',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'bou'
      },
      {
        country: 'USA',
        state: 'Colorado',
        city: 'Colo Springs',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'cos'
      },
      {
        country: 'USA',
        state: 'Colorado',
        city: 'Eastern Co',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'eco'
      },
      {
        country: 'USA',
        state: 'Colorado',
        city: 'Fort Collins',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'ftc'
      },
      {
        country: 'USA',
        state: 'Colorado',
        city: 'High Rockies',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'rck'
      },
      {
        country: 'USA',
        state: 'Colorado',
        city: 'Pueblo',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'pub'
      },
      {
        country: 'USA',
        state: 'Colorado',
        city: 'Western Slope',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'gjt'
      },
      {
        country: 'USA',
        state: 'Connecticut',
        city: 'Eastern Ct',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'nlo'
      },
      {
        country: 'USA',
        state: 'Connecticut',
        city: 'Hartford',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'htf'
      },
      {
        country: 'USA',
        state: 'Connecticut',
        city: 'New Haven',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'hvn'
      },
      {
        country: 'USA',
        state: 'Connecticut',
        city: 'Northwest Ct',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'nct'
      },
      {
        country: 'USA',
        state: 'Delaware',
        city: 'Delaware',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'dlw'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Daytona Beach',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'dab'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Florida Keys',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'key'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Gainesville',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'gnv'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Heartland Fl',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'cfl'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Jacksonville',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'jax'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Lake City',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'lcq'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Ocala',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'oca'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Okaloosa',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'vps'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Orlando',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'orl'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Panama City',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'pfn'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Pensacola',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'pns'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Sarasota',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'srq'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Space Coast',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'mlb'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'St Augustine',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'ust'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Tallahassee',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'tal'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Tampa Bay',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'tpa'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Tampa Bay',
        subMarket: 'Hernando CO',
        craigslistMediaFee: '$35',
        value: 'hdo'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Tampa Bay',
        subMarket: 'Hillsborough CO',
        craigslistMediaFee: '$35',
        value: 'hil'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Tampa Bay',
        subMarket: 'Pasco CO',
        craigslistMediaFee: '$35',
        value: 'psc'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Tampa Bay',
        subMarket: 'Pinellas CO',
        craigslistMediaFee: '$35',
        value: 'pnl'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Treasure Coast',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'psl'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Albany',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'aby'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Athens',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'ahn'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Augusta',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'aug'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Brunswick',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'bwk'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Columbus',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'csg'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Macon',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'mcn'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Northwest Ga',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'nwg'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Savannah',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'sav'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Statesboro',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'tbr'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Valdosta',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'vld'
      },
      {
        country: 'USA',
        state: 'Hawaii',
        city: 'Hawaii',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'hnl'
      },
      {
        country: 'USA',
        state: 'Hawaii',
        city: 'Hawaii',
        subMarket: 'Big Island',
        craigslistMediaFee: '$25',
        value: 'big'
      },
      {
        country: 'USA',
        state: 'Hawaii',
        city: 'Hawaii',
        subMarket: 'Kauai',
        craigslistMediaFee: '$25',
        value: 'hnl'
      },
      {
        country: 'USA',
        state: 'Hawaii',
        city: 'Hawaii',
        subMarket: 'Maui',
        craigslistMediaFee: '$25',
        value: 'mau'
      },
      {
        country: 'USA',
        state: 'Hawaii',
        city: 'Hawaii',
        subMarket: 'Moloka',
        craigslistMediaFee: '$25',
        value: 'mol'
      },
      {
        country: 'USA',
        state: 'Hawaii',
        city: 'Hawaii',
        subMarket: 'Oahu',
        craigslistMediaFee: '$25',
        value: 'oah'
      },
      {
        country: 'USA',
        state: 'Idaho',
        city: 'Boise',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'boi'
      },
      {
        country: 'USA',
        state: 'Idaho',
        city: 'East Idaho',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'eid'
      },
      {
        country: 'USA',
        state: 'Idaho',
        city: 'Twin Falls',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'twf'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Bloomington',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'bln'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Chambana',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'chm'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Decatur',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'dil'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'La Salle Co',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'lsl'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Mattoon',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mto'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Peoria',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'pia'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Rockford',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'rfd'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Southern Il',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'cbd'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Springfield',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'spi'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Western Il',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'qcy'
      },
      {
        country: 'USA',
        state: 'Indiana',
        city: 'Bloomington',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'bmg'
      },
      {
        country: 'USA',
        state: 'Indiana',
        city: 'Evansville',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'evv'
      },
      {
        country: 'USA',
        state: 'Indiana',
        city: 'Fort Wayne',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'fwa'
      },
      {
        country: 'USA',
        state: 'Indiana',
        city: 'Indianapolis',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'ind'
      },
      {
        country: 'USA',
        state: 'Indiana',
        city: 'Kokomo',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'okk'
      },
      {
        country: 'USA',
        state: 'Indiana',
        city: 'Muncie',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'mun'
      },
      {
        country: 'USA',
        state: 'Indiana',
        city: 'Richmond',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'rin'
      },
      {
        country: 'USA',
        state: 'Indiana',
        city: 'South Bend',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'sbn'
      },
      {
        country: 'USA',
        state: 'Indiana',
        city: 'Terre Haute',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'tha'
      },
      {
        country: 'USA',
        state: 'Indiana',
        city: 'Tippecanoe',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'laf'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Ames',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ame'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Cedar Rapids',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'ced'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Des Moines',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'dsm'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Dubuque',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'dbq'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Fort Dodge',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ftd'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Iowa City',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'iac'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Mason City',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'msc'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Quad Cities',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'mli'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Sioux City',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'sux'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Southeast Ia',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'otu'
      },
      {
        country: 'USA',
        state: 'Iowa',
        city: 'Waterloo',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wlo'
      },
      {
        country: 'USA',
        state: 'Kansas',
        city: 'Lawrence',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'lwr'
      },
      {
        country: 'USA',
        state: 'Kansas',
        city: 'Manhattan',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mhk'
      },
      {
        country: 'USA',
        state: 'Kansas',
        city: 'Northwest Ks',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'nwk'
      },
      {
        country: 'USA',
        state: 'Kansas',
        city: 'Salina',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'sns'
      },
      {
        country: 'USA',
        state: 'Kansas',
        city: 'Southeast Ks',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'sek'
      },
      {
        country: 'USA',
        state: 'Kansas',
        city: 'Southwest Ks',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'swk'
      },
      {
        country: 'USA',
        state: 'Kansas',
        city: 'Topeka',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'tpk'
      },
      {
        country: 'USA',
        state: 'Kansas',
        city: 'Wichita',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'wic'
      },
      {
        country: 'USA',
        state: 'Kentucky',
        city: 'Bowling Green',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'blg'
      },
      {
        country: 'USA',
        state: 'Louisiana',
        city: 'Lafayette',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'lft'
      },
      {
        country: 'USA',
        state: 'Nevada',
        city: 'Reno',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'rno'
      },
      {
        country: 'USA',
        state: 'New Mexico',
        city: 'Roswell',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'row'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Boone',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'bnc'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Fayetteville',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'fay'
      },
      {
        country: 'USA',
        state: 'South Dakota',
        city: 'Sioux Falls',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'fsd'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Pullman-Moscow',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'plm'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Spokane',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'spk'
      },
      {
        country: 'USA',
        state: 'Kentucky',
        city: 'Eastern Ky',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'eky'
      },
      {
        country: 'USA',
        state: 'Kentucky',
        city: 'Lexington',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'lex'
      },
      {
        country: 'USA',
        state: 'Kentucky',
        city: 'Louisville',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'lou'
      },
      {
        country: 'USA',
        state: 'Kentucky',
        city: 'Owensboro',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'owb'
      },
      {
        country: 'USA',
        state: 'Kentucky',
        city: 'Western Ky',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wky'
      },
      {
        country: 'USA',
        state: 'Louisiana',
        city: 'Central La',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'aex'
      },
      {
        country: 'USA',
        state: 'Louisiana',
        city: 'Baton Rouge',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'btr'
      },
      {
        country: 'USA',
        state: 'Louisiana',
        city: 'Houma',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'hum'
      },
      {
        country: 'USA',
        state: 'Louisiana',
        city: 'Lake Charles',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'lkc'
      },
      {
        country: 'USA',
        state: 'Louisiana',
        city: 'Monroe',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mlu'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Monroe',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mnr'
      },
      {
        country: 'USA',
        state: 'Louisiana',
        city: 'New Orleans',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'nor'
      },
      {
        country: 'USA',
        state: 'Louisiana',
        city: 'Shreveport',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'shv'
      },
      {
        country: 'USA',
        state: 'Massachusetts',
        city: 'Cape Cod',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'cap'
      },
      {
        country: 'USA',
        state: 'Massachusetts',
        city: 'South Coast',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'sma'
      },
      {
        country: 'USA',
        state: 'Massachusetts',
        city: 'Western Mass',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'wma'
      },
      {
        country: 'USA',
        state: 'Massachusetts',
        city: 'Worcester',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'wor'
      },
      {
        country: 'USA',
        state: 'Maryland',
        city: 'Annapolis',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'anp'
      },
      {
        country: 'USA',
        state: 'Maryland',
        city: 'Baltimore',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'bal'
      },
      {
        country: 'USA',
        state: 'Maryland',
        city: 'Eastern Shore',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'esh'
      },
      {
        country: 'USA',
        state: 'Maryland',
        city: 'Frederick',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'fdk'
      },
      {
        country: 'USA',
        state: 'Maryland',
        city: 'Southern Md',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'smd'
      },
      {
        country: 'USA',
        state: 'Maryland',
        city: 'Western Md',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wmd'
      },
      {
        country: 'USA',
        state: 'Maine',
        city: 'Maine',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'mne'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Ann Arbor',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'aaa'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Battle Creek',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'btc'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Central Mi',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'cmu'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Flint',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'fnt'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Grand Rapids',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'grr'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Holland',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'hld'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Jackson',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'jxn'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Kalamazoo',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'kzo'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Lansing',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'lan'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Saginaw',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'mbs'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Muskegon',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'mkg'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Northern Mi',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'nmi'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Port Huron',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'phn'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Southwest Mi',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'swm'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'The Thumb',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'thb'
      },
      {
        country: 'USA',
        state: 'West Virginia',
        city: 'Northern Wv',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'whl'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Yoopers',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'yup'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Bemidji',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'bji'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Brainerd',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'brd'
      },
      {
        country: 'USA',
        state: 'North Dakota',
        city: 'Fargo',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'far'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Lakeland',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'lal'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Mankato',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mkt'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Southwest Mn',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mml'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Rochester',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'rmn'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'St Cloud',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'stc'
      },
      {
        country: 'USA',
        state: 'Missouri',
        city: 'Columbia',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'cou'
      },
      {
        country: 'USA',
        state: 'Missouri',
        city: 'Joplin',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'jln'
      },
      {
        country: 'USA',
        state: 'Missouri',
        city: 'Kirksville',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'krk'
      },
      {
        country: 'USA',
        state: 'Missouri',
        city: 'Kansas City',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'ksc'
      },
      {
        country: 'USA',
        state: 'Missouri',
        city: 'Lake Of Ozarks',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'loz'
      },
      {
        country: 'USA',
        state: 'Missouri',
        city: 'Springfield',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'sgf'
      },
      {
        country: 'USA',
        state: 'Missouri',
        city: 'Southeast Mo',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'smo'
      },
      {
        country: 'USA',
        state: 'Missouri',
        city: 'St Joseph',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'stj'
      },
      {
        country: 'USA',
        state: 'Missouri',
        city: 'St Louis',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'stl'
      },
      {
        country: 'USA',
        state: 'Mississipi',
        city: 'Gulfport',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'gpt'
      },
      {
        country: 'USA',
        state: 'Mississipi',
        city: 'Southwest Ms',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'hez'
      },
      {
        country: 'USA',
        state: 'Mississipi',
        city: 'Jackson',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'jan'
      },
      {
        country: 'USA',
        state: 'Mississipi',
        city: 'Meridian',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mei'
      },
      {
        country: 'USA',
        state: 'Mississipi',
        city: 'North Ms',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'nms'
      },
      {
        country: 'USA',
        state: 'Mississipi',
        city: 'Hattiesburg',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'usm'
      },
      {
        country: 'USA',
        state: 'Montana',
        city: 'Billings',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'bil'
      },
      {
        country: 'USA',
        state: 'Montana',
        city: 'Butte',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'btm'
      },
      {
        country: 'USA',
        state: 'Montana',
        city: 'Bozeman',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'bzn'
      },
      {
        country: 'USA',
        state: 'Montana',
        city: 'Kalispell',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'fca'
      },
      {
        country: 'USA',
        state: 'Montana',
        city: 'Great Falls',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'gtf'
      },
      {
        country: 'USA',
        state: 'Montana',
        city: 'Helena',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'hln'
      },
      {
        country: 'USA',
        state: 'Montana',
        city: 'Eastern Montana',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mnt'
      },
      {
        country: 'USA',
        state: 'Montana',
        city: 'Missoula',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'mso'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Asheville',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'ash'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Charlotte',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'cha'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Eastern Nc',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'enc'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Greensboro',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'gbo'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Hickory',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'hky'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Jacksonville',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'oaj'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Outer Banks',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'obx'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Raleigh',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'ral'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Wilmington',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'wnc'
      },
      {
        country: 'USA',
        state: 'North Carolina',
        city: 'Winston-Salem',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'wsl'
      },
      {
        country: 'USA',
        state: 'North Dakota',
        city: 'Bismarck',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'bis'
      },
      {
        country: 'USA',
        state: 'North Dakota',
        city: 'Grand Forks',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'gfk'
      },
      {
        country: 'USA',
        state: 'North Dakota',
        city: 'North Dakota',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ndk'
      },
      {
        country: 'USA',
        state: 'Nebraska',
        city: 'Omaha',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'oma'
      },
      {
        country: 'USA',
        state: 'Nebraska',
        city: 'Scottsbluff',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'bff'
      },
      {
        country: 'USA',
        state: 'Nebraska',
        city: 'Grand Island',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'gil'
      },
      {
        country: 'USA',
        state: 'Nebraska',
        city: 'North Platte',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'lbf'
      },
      {
        country: 'USA',
        state: 'Nebraska',
        city: 'Lincoln',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'lnk'
      },
      {
        country: 'USA',
        state: 'New Hampshire',
        city: 'New Hampshire',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'nhm'
      },
      {
        country: 'USA',
        state: 'New Jersey',
        city: 'Central Nj',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'cnj'
      },
      {
        country: 'USA',
        state: 'New Jersey',
        city: 'Jersey Shore',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'jys'
      },
      {
        country: 'USA',
        state: 'New Jersey',
        city: 'North Jersey',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'njy'
      },
      {
        country: 'USA',
        state: 'New Jersey',
        city: 'South Jersey',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'snj'
      },
      {
        country: 'USA',
        state: 'New Mexico',
        city: 'Albuquerque',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'abq'
      },
      {
        country: 'USA',
        state: 'New Mexico',
        city: 'Clovis-Portales',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'cvn'
      },
      {
        country: 'USA',
        state: 'New Mexico',
        city: 'Farmington',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'fnm'
      },
      {
        country: 'USA',
        state: 'New Mexico',
        city: 'Las Cruces',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'lcr'
      },
      {
        country: 'USA',
        state: 'New Mexico',
        city: 'Santa Fe',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'saf'
      },
      {
        country: 'USA',
        state: 'Nevada',
        city: 'Elko',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'elk'
      },
      {
        country: 'USA',
        state: 'Nevada',
        city: 'Las Vegas',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'lvg'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Albany',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'alb'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Binghamton',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'bgm'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Buffalo',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'buf'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Catskills',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'cat'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Chautauqua',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'chq'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Elmira',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'elm'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Finger Lakes',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'fgl'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Glens Falls',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'gfl'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Hudson Valley',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'hud'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Long Island',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'isp'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Ithaca',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ith'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Oneonta',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'onh'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Plattsburgh',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'plb'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Rochester',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'rcs'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Syracuse',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'syr'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Utica',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'uti'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Watertown',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wtn'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Twin Tiers',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'tts'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Akron-Canton',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'cak'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Chillicothe',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'chl'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Cincinnati',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'cin'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Cleveland',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'cle'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Columbus',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'col'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Dayton',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'day'
      },
      {
        country: 'USA',
        state: 'West Virginia',
        city: 'Huntington',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'hts'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Ashtabula',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'jfn'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Lima-Findlay',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'lma'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Mansfield',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'mfd'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Tuscarawas Co',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'nph'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Athens',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ohu'
      },
      {
        country: 'USA',
        state: 'West Virginia',
        city: 'Parkersburg',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'pkb'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'Potsdam-Massena',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ptd'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Sandusky',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'sky'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Toledo',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'tol'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Youngstown',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'yng'
      },
      {
        country: 'USA',
        state: 'Ohio',
        city: 'Zanesville',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'zvl'
      },
      {
        country: 'USA',
        state: 'Oklahoma',
        city: 'Northwest Ok',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'end'
      },
      {
        country: 'USA',
        state: 'Oklahoma',
        city: 'Lawton',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'law'
      },
      {
        country: 'USA',
        state: 'Oklahoma',
        city: 'Oklahoma City',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'okc'
      },
      {
        country: 'USA',
        state: 'Oklahoma',
        city: 'Stillwater',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'osu'
      },
      {
        country: 'USA',
        state: 'Oklahoma',
        city: 'Tulsa',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'tul'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Bend',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'bnd'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Oregon Coast',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'cor'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Corvallis',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'crv'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'East Oregon',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'eor'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Eugene',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'eug'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Klamath Falls',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'klf'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Medford',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'mfr'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Roseburg',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'rbg'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Salem',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'sle'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Allentown',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'alt'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Altoona',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'aoo'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Scranton',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'avp'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Cumberland Val',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'cbg'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Erie',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'eri'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Harrisburg',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'hrs'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Lancaster',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'lns'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Meadville',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mdv'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Pittsburgh',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'pit'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Poconos',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'poc'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'State College',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'psu'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Reading',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'rea'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Williamsport',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wpt'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'York',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'yrk'
      },
      {
        country: 'USA',
        state: 'Rhode Island',
        city: 'Rhode Island',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'prv'
      },
      {
        country: 'USA',
        state: 'South Dakota',
        city: 'Rapid City',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'rap'
      },
      {
        country: 'USA',
        state: 'South Carolina',
        city: 'Columbia',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'cae'
      },
      {
        country: 'USA',
        state: 'South Carolina',
        city: 'Charleston',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'chs'
      },
      {
        country: 'USA',
        state: 'South Carolina',
        city: 'Florence',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'flo'
      },
      {
        country: 'USA',
        state: 'South Carolina',
        city: 'Greenville',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'gsp'
      },
      {
        country: 'USA',
        state: 'South Carolina',
        city: 'Hilton Head',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'hhi'
      },
      {
        country: 'USA',
        state: 'South Carolina',
        city: 'Myrtle Beach',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'myr'
      },
      {
        country: 'USA',
        state: 'South Dakota',
        city: 'Northeast Sd',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'abr'
      },
      {
        country: 'USA',
        state: 'South Dakota',
        city: 'Central Sd',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'csd'
      },
      {
        country: 'USA',
        state: 'South Dakota',
        city: 'South Dakota',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'sdk'
      },
      {
        country: 'USA',
        state: 'Tennessee',
        city: 'Chattanooga',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'cht'
      },
      {
        country: 'USA',
        state: 'Tennessee',
        city: 'Clarksville',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'ckv'
      },
      {
        country: 'USA',
        state: 'Tennessee',
        city: 'Cookeville',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'coo'
      },
      {
        country: 'USA',
        state: 'Tennessee',
        city: 'Jackson',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'jxt'
      },
      {
        country: 'USA',
        state: 'Tennessee',
        city: 'Memphis',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'mem'
      },
      {
        country: 'USA',
        state: 'Tennessee',
        city: 'Nashville',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'nsh'
      },
      {
        country: 'USA',
        state: 'Tennessee',
        city: 'Tri-Cities',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'tri'
      },
      {
        country: 'USA',
        state: 'Tennessee',
        city: 'Knoxville',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'knx'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Abilene',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'abi'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Amarillo',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'ama'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Beaumont',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'bpt'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Brownsville',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'bro'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Corpus Christi',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'crp'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'College Station',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'cst'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Del Rio',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'drt'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'El Paso',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'elp'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'East Tx',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'etx'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Galveston',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'gls'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Killeen-Temple',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'grk'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Lubbock',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'lbb'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Laredo',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'lrd'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Mcallen',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'mca'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Deep East Tx',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'och'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Odessa',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'odm'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'San Antonio',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'sat'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'San Angelo',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'sjt'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'San Marcos',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'tsu'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Victoria',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'vtx'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Waco',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'wco'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Wichita Falls',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wtf'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Southwest Tx',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wtx'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Texoma',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'txm'
      },
      {
        country: 'USA',
        state: 'Utah',
        city: 'Logan',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'lgu'
      },
      {
        country: 'USA',
        state: 'Utah',
        city: 'Ogden',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ogd'
      },
      {
        country: 'USA',
        state: 'Utah',
        city: 'Provo',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'pvu'
      },
      {
        country: 'USA',
        state: 'Utah',
        city: 'Salt Lake',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'slc'
      },
      {
        country: 'USA',
        state: 'Utah',
        city: 'St George',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'stg'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Charlottesville',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'uva'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Danville',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'dnv'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Fredericksburg',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'ezf'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Norfolk',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'nfk'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Harrisonburg',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'shd'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Lynchburg',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'lyn'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Blacksburg',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'vpi'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Richmond',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'ric'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Roanoke',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'roa'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Southwest Va',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'vaw'
      },
      {
        country: 'USA',
        state: 'Virginia',
        city: 'Winchester',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'okv'
      },
      {
        country: 'USA',
        state: 'Vermont',
        city: 'Vermont',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'brl'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Bellingham',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'bli'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Tri-Cities',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'kpr'
      },
      {
        country: 'USA',
        state: 'Idaho',
        city: 'Lewiston',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'lws'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Moses Lake',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'mlk'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Skagit',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'mvw'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Olympic Pen',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'olp'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Wenatchee',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wen'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Yakima',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'yak'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'Appleton',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'app'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Duluth',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'dlh'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'Eau Claire',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'eau'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'Green Bay',
        subMarket: '',
        craigslistMediaFee: '$15',
        value: 'grb'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'Janesville',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'jvl'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'La Crosse',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'lse'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'Madison',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'mad'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'Milwaukee',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'mil'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'Northern Wi',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'nwi'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'Kenosha-Racine',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'rac'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'Sheboygan',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'sbm'
      },
      {
        country: 'USA',
        state: 'Wisconsin',
        city: 'Wausau',
        subMarket: '',
        craigslistMediaFee: '$10',
        value: 'wau'
      },
      {
        country: 'USA',
        state: 'West Virginia',
        city: 'Charleston',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'crw'
      },
      {
        country: 'USA',
        state: 'West Virginia',
        city: 'Eastern Wv',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'ewv'
      },
      {
        country: 'USA',
        state: 'West Virginia',
        city: 'Southern Wv',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'swv'
      },
      {
        country: 'USA',
        state: 'West Virginia',
        city: 'West Virginia',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wva'
      },
      {
        country: 'USA',
        state: 'West Virginia',
        city: 'Morgantown',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wvu'
      },
      {
        country: 'USA',
        state: 'Wyoming',
        city: 'Wyoming',
        subMarket: '',
        craigslistMediaFee: '$7',
        value: 'wyo'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Sf Bay Area',
        subMarket: '',
        craigslistMediaFee: '$75',
        value: 'sfo'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Sf Bay Area',
        subMarket: 'North Bay / Marin',
        craigslistMediaFee: '$75',
        value: 'nby'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Sf Bay Area',
        subMarket: 'East Bay Area',
        craigslistMediaFee: '$75',
        value: 'eby'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Sf Bay Area',
        subMarket: 'Peninsula',
        craigslistMediaFee: '$75',
        value: 'pen'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Sf Bay Area',
        subMarket: 'South Bay Area',
        craigslistMediaFee: '$75',
        value: 'sby'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Sf Bay Area',
        subMarket: 'Santa Cruz CO',
        craigslistMediaFee: '$75',
        value: 'scz'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Sf Bay Area',
        subMarket: 'City of San Francisco',
        craigslistMediaFee: '$75',
        value: 'sfc'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Seattle',
        subMarket: '',
        craigslistMediaFee: '$45',
        value: 'sea'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Seattle',
        subMarket: 'Eastside',
        craigslistMediaFee: '$45',
        value: 'est'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Seattle',
        subMarket: 'Kitsap / West Puget',
        craigslistMediaFee: '$45',
        value: 'kit'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Seattle',
        subMarket: 'Olympia / Thurston',
        craigslistMediaFee: '$45',
        value: 'oly'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Seattle',
        subMarket: 'Seattle',
        craigslistMediaFee: '$45',
        value: 'see'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Seattle',
        subMarket: 'South King CO',
        craigslistMediaFee: '$45',
        value: 'skc'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Seattle',
        subMarket: 'Snohomish County',
        craigslistMediaFee: '$45',
        value: 'sno'
      },
      {
        country: 'USA',
        state: 'Washington',
        city: 'Seattle',
        subMarket: 'Tacoma / Pierce',
        craigslistMediaFee: '$45',
        value: 'tac'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'New York City',
        subMarket: '',
        craigslistMediaFee: '$45',
        value: 'nyc'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'New York',
        subMarket: 'Brooklyn',
        craigslistMediaFee: '$45',
        value: 'brk'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'New York',
        subMarket: 'Westchester',
        craigslistMediaFee: '$45',
        value: 'wch'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'New York',
        subMarket: 'Bronx',
        craigslistMediaFee: '$45',
        value: 'brx'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'New York',
        subMarket: 'New Jersey',
        craigslistMediaFee: '$45',
        value: 'jsy'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'New York',
        subMarket: 'Long Island',
        craigslistMediaFee: '$45',
        value: 'lgi'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'New York',
        subMarket: 'Manhattan',
        craigslistMediaFee: '$45',
        value: 'mnh'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'New York',
        subMarket: 'Fairfield CO',
        craigslistMediaFee: '$45',
        value: 'nyc'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'New York',
        subMarket: 'Queens',
        craigslistMediaFee: '$45',
        value: 'que'
      },
      {
        country: 'USA',
        state: 'New York',
        city: 'New York',
        subMarket: 'Staten Island',
        craigslistMediaFee: '$45',
        value: 'stn'
      },
      {
        country: 'USA',
        state: 'Massachusetts',
        city: 'Boston',
        subMarket: '',
        craigslistMediaFee: '$45',
        value: 'bos'
      },
      {
        country: 'USA',
        state: 'Massachusetts',
        city: 'Boston',
        subMarket: 'Boston/Cambridge/Brookline',
        craigslistMediaFee: '$45',
        value: 'gbs'
      },
      {
        country: 'USA',
        state: 'Massachusetts',
        city: 'Boston',
        subMarket: 'North Shore',
        craigslistMediaFee: '$45',
        value: 'nos'
      },
      {
        country: 'USA',
        state: 'Massachusetts',
        city: 'Boston',
        subMarket: 'Metro West',
        craigslistMediaFee: '$45',
        value: 'nmw'
      },
      {
        country: 'USA',
        state: 'Massachusetts',
        city: 'Boston',
        subMarket: 'Northwest/Merrimack',
        craigslistMediaFee: '$45',
        value: 'nwb'
      },
      {
        country: 'USA',
        state: 'Massachusetts',
        city: 'Boston',
        subMarket: '',
        craigslistMediaFee: '$45',
        value: 'sob'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Los Angeles',
        subMarket: '',
        craigslistMediaFee: '$45',
        value: 'lax'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Los Angeles',
        subMarket: 'Antelope Valley',
        craigslistMediaFee: '$45',
        value: 'ant'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Los Angeles',
        subMarket: 'Central LA 213/323',
        craigslistMediaFee: '$45',
        value: 'lac'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Los Angeles',
        subMarket: 'Long Beach / 562',
        craigslistMediaFee: '$45',
        value: 'lgb'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Los Angeles',
        subMarket: 'San Fernando Valley',
        craigslistMediaFee: '$45',
        value: 'sfv'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Los Angeles',
        subMarket: 'San Gabriel Valley',
        craigslistMediaFee: '$45',
        value: 'sgv'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Los Angeles',
        subMarket: 'Westside-Southbay-310',
        craigslistMediaFee: '$45',
        value: 'wst'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'San Diego',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'sdo'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'San Diego',
        subMarket: 'City of San Diego',
        craigslistMediaFee: '$35',
        value: 'csd'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'San Diego',
        subMarket: 'East San Diego County',
        craigslistMediaFee: '$35',
        value: 'esd'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'San Diego',
        subMarket: 'North San Diego County',
        craigslistMediaFee: '$35',
        value: 'nsd'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'San Diego',
        subMarket: 'South San Diego County',
        craigslistMediaFee: '$35',
        value: 'ssd'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Portland',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'pdx'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Portland',
        subMarket: 'Clackamas County',
        craigslistMediaFee: '$35',
        value: 'clc'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Portland',
        subMarket: 'Clark/Cowlitz WA',
        craigslistMediaFee: '$35',
        value: 'clk'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Portland',
        subMarket: 'Multnomah County',
        craigslistMediaFee: '$35',
        value: 'mlt'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Portland',
        subMarket: 'Columbia Gorge',
        craigslistMediaFee: '$35',
        value: 'grg'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Portland',
        subMarket: 'North Coast',
        craigslistMediaFee: '$35',
        value: 'nco'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Portland',
        subMarket: 'Washington County',
        craigslistMediaFee: '$35',
        value: 'wsc'
      },
      {
        country: 'USA',
        state: 'Oregon',
        city: 'Portland',
        subMarket: 'Yamhill CO',
        craigslistMediaFee: '$35',
        value: 'yam'
      },
      {
        country: 'USA',
        state: 'DC',
        city: 'Washington',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'wdc'
      },
      {
        country: 'USA',
        state: 'DC',
        city: 'Washington',
        subMarket: 'District of Columbia',
        craigslistMediaFee: '$35',
        value: 'doc'
      },
      {
        country: 'USA',
        state: 'DC',
        city: 'Washington',
        subMarket: 'Maryland',
        craigslistMediaFee: '$35',
        value: 'mld'
      },
      {
        country: 'USA',
        state: 'DC',
        city: 'Washington',
        subMarket: 'Northern Virginia',
        craigslistMediaFee: '$35',
        value: 'nva'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Chicago',
        subMarket: '',
        craigslistMediaFee: '$45',
        value: 'chi'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Chicago',
        subMarket: 'City of Chicago',
        craigslistMediaFee: '$45',
        value: 'chc'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Chicago',
        subMarket: 'North Chicagoland',
        craigslistMediaFee: '$45',
        value: 'nch'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Chicago',
        subMarket: 'Northwest Suburbs',
        craigslistMediaFee: '$45',
        value: 'nwc'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Chicago',
        subMarket: 'Northwest Indiana',
        craigslistMediaFee: '$45',
        value: 'nwi'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Chicago',
        subMarket: 'South Chicagoland',
        craigslistMediaFee: '$45',
        value: 'sox'
      },
      {
        country: 'USA',
        state: 'Illinois',
        city: 'Chicago',
        subMarket: 'West Chicagoland',
        craigslistMediaFee: '$45',
        value: 'wcl'
      },
      {
        country: 'USA',
        state: 'Colorado',
        city: 'Denver',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'den'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Atlanta',
        subMarket: 'City of Atlanta',
        craigslistMediaFee: '$35',
        value: 'atl'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Atlanta',
        subMarket: 'OTP East',
        craigslistMediaFee: '$35',
        value: 'eat'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Atlanta',
        subMarket: 'OTP North',
        craigslistMediaFee: '$35',
        value: 'nat'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Atlanta',
        subMarket: 'OTP South',
        craigslistMediaFee: '$35',
        value: 'sat'
      },
      {
        country: 'USA',
        state: 'Georgia',
        city: 'Atlanta',
        subMarket: 'OTP West',
        craigslistMediaFee: '$35',
        value: 'wat'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Austin',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'aus'
      },
      {
        country: 'USA',
        state: 'Pennsylvania',
        city: 'Philadelphia',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'phi'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Phoenix',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'phx'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Phoenix',
        subMarket: 'Central/South PHX',
        craigslistMediaFee: '$35',
        value: 'cph'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Phoenix',
        subMarket: 'East Valley',
        craigslistMediaFee: '$35',
        value: 'evl'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Phoenix',
        subMarket: 'PHX North',
        craigslistMediaFee: '$35',
        value: 'nph'
      },
      {
        country: 'USA',
        state: 'Arizona',
        city: 'Phoenix',
        subMarket: 'West Valley',
        craigslistMediaFee: '$35',
        value: 'wvl'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Minneapolis',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'min'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Minneapolis',
        subMarket: 'Anoka/Chisago/Isanti',
        craigslistMediaFee: '$35',
        value: 'ank'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Minneapolis',
        subMarket: 'Carver/Sherburne/Wright',
        craigslistMediaFee: '$35',
        value: 'csw'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Minneapolis',
        subMarket: 'Dakota / Scott',
        craigslistMediaFee: '$35',
        value: 'dak'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Minneapolis',
        subMarket: 'Hennepin County',
        craigslistMediaFee: '$35',
        value: 'hnp'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Minneapolis',
        subMarket: 'Ramsey County',
        craigslistMediaFee: '$35',
        value: 'ram'
      },
      {
        country: 'USA',
        state: 'Minnesota',
        city: 'Minneapolis',
        subMarket: 'Washington CO / WI',
        craigslistMediaFee: '$35',
        value: 'wsh'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'South Florida',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'mia'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'South Florida',
        subMarket: 'Broward County',
        craigslistMediaFee: '$35',
        value: 'brw'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'South Florida',
        subMarket: 'Miami / Dade County',
        craigslistMediaFee: '$35',
        value: 'mdc'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'South Florida',
        subMarket: 'Palm Beach County',
        craigslistMediaFee: '$35',
        value: 'pbc'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Dallas',
        subMarket: 'Dallas',
        craigslistMediaFee: '$35',
        value: 'dal'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Dallas',
        subMarket: 'Fortworth',
        craigslistMediaFee: '$35',
        value: 'ftw'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Dallas',
        subMarket: 'Mid Cities',
        craigslistMediaFee: '$35',
        value: 'mdf'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Dallas',
        subMarket: 'North DFW',
        craigslistMediaFee: '$35',
        value: 'ndf'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Dallas',
        subMarket: 'South DFW',
        craigslistMediaFee: '$35',
        value: 'sdf'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Detroit Metro',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'det'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Detroit Metro',
        subMarket: 'Macomb County',
        craigslistMediaFee: '$35',
        value: 'mcb'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Detroit Metro',
        subMarket: 'Oakland County',
        craigslistMediaFee: '$35',
        value: 'okl'
      },
      {
        country: 'USA',
        state: 'Michigan',
        city: 'Detroit Metro',
        subMarket: 'Wayne County',
        craigslistMediaFee: '$35',
        value: 'wyn'
      },
      {
        country: 'USA',
        state: 'Texas',
        city: 'Houston',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'hou'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Orange Co',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'orc'
      },
      {
        country: 'USA',
        state: 'California',
        city: 'Sacramento',
        subMarket: '',
        craigslistMediaFee: '$35',
        value: 'sac'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Fort Myers',
        subMarket: '',
        craigslistMediaFee: '$25',
        value: 'fmy'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Fort Myers',
        subMarket: 'Charlotte County',
        craigslistMediaFee: '$25',
        value: 'fl_chl'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Fort Myers',
        subMarket: 'Collier County',
        craigslistMediaFee: '$25',
        value: 'fl_col'
      },
      {
        country: 'USA',
        state: 'Florida',
        city: 'Fort Myers',
        subMarket: 'Lee County',
        craigslistMediaFee: '$25',
        value: 'fl_lee'
      }
    ],
    categories: [
      {
        label: 'Accounting/Finance',
        value: 23
      },
      {
        label: 'Admin/Office',
        value: 24
      },
      {
        label: 'Architect/Engineer/Cad',
        value: 48
      },
      {
        label: 'Art/Media/Design',
        value: 25
      },
      {
        label: 'Business/Mgmt',
        value: 12
      },
      {
        label: 'Customer Service',
        value: 100
      },
      {
        label: 'Education/Teaching',
        value: 57
      },
      {
        label: 'Et Cetera',
        value: 15
      },
      {
        label: 'Food/Beverage/Hospitality',
        value: 129
      },
      {
        label: 'General Labor',
        value: 130
      },
      {
        label: 'Government',
        value: 61
      },
      {
        label: 'Healthcare',
        value: 26
      },
      {
        label: 'Human Resource',
        value: 54
      },
      {
        label: 'Internet Engineering',
        value: 14
      },
      {
        label: 'Legal/Paralegal',
        value: 47
      },
      {
        label: 'Manufacturing',
        value: 128
      },
      {
        label: 'Marketing/Advertising/Pr',
        value: 13
      },
      {
        label: 'Nonprofit',
        value: 28
      },
      {
        label: 'Real Estate',
        value: 127
      },
      {
        label: 'Retail/Wholesale',
        value: 27
      },
      {
        label: 'Sales',
        value: 49
      },
      {
        label: 'Salon/Spa/Fitness',
        value: 126
      },
      {
        label: 'Science/Biotech',
        value: 75
      },
      {
        label: 'Security',
        value: 131
      },
      {
        label: 'Skilled Trades/Artisan',
        value: 59
      },
      {
        label: 'Software/Qa/Dba/Etc',
        value: 21
      },
      {
        label: 'Systems/Networking',
        value: 50
      },
      {
        label: 'Technical Support',
        value: 55
      },
      {
        label: 'Transportation',
        value: 125
      },
      {
        label: 'Tv/Film/Video/Radio',
        value: 52
      },
      {
        label: 'Web/Html/Info Design',
        value: 11
      },
      {
        label: 'Writing/Editing',
        value: 16
      }
    ]
  },
  get () {
    return this.store
  }
}

export default craigslistLocationStore
