let userPreferencesStore = {

  store: {
  },

  defaults: {
    candidateGridType: 'table',
    candidatesPerPage: 25,
    pagesReportsGridType: 'graph',
    hiddenManagerColumns: [],
    hiddenSourcedReqColumns: []
  },

  get () {
    return this.store
  },

  set (key, value) {
    this.store[key] = value
    localStorage.setItem('hap_preferences', JSON.stringify(this.store))
  },

  init () {
    // Get preferences from local storage
    let preferences
    if (localStorage.getItem('hap_preferences') && localStorage.getItem('hap_preferences') !== 'undefined') {
      preferences = JSON.parse(localStorage.getItem('hap_preferences'))
    }

    // Define default values if we don't have anything saved yet
    if (!preferences) {
      preferences = $.extend(true, {}, this.defaults)
    } else {
      // Make sure all the preferences are set
      let setPrefs = Object.keys(preferences)
      $.each(this.defaults, (key, value) => {
        if (setPrefs.indexOf(key) === -1) {
          preferences[key] = value
        }
      })
    }

    this.store = preferences
    localStorage.setItem('hap_preferences', JSON.stringify(this.store))
  },

  clear () {
    localStorage.removeItem('hap_preferences')
    this.init()
  }

}

export default userPreferencesStore
