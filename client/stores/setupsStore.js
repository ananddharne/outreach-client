const setupsStore = {

  store: {
    setups: []
  },

  init (data) {
    this.store.setups = data
  },

  get () {
    return this.store
  },

  delete (id) {
    const index = this.store.setups.findIndex(setup => setup.id == id)
    if (~index) this.store.setups.splice(index, 1)
  }

}

export default setupsStore
