let mergeTagsStore = {

  store: {
    mergeTags: [],
    mergeTagObject: {
      'company': {
        'address': {
          'city': null,
          'formatted': null,
          'postal_code': null,
          'state': null,
          'street_address_1': null,
          'street_address_2': null
        },
        'extended_attributes': {},
        'industry': null,
        'latitude': 0.0,
        'linkedin_url': null,
        'logo_url': null,
        'longitude': 0.0,
        'name': 'Acme Widgets',
        'primary_contact_email': null,
        'primary_contact_first_name': null,
        'primary_contact_last_name': null,
        'primary_contact_phone': null,
        'website_url': null
      },
      'html': null,
      'organization': {
        'address': {
          'city': null,
          'formatted': null,
          'postal_code': null,
          'state': null,
          'street_address_1': null,
          'street_address_2': null
        },
        'extended_attributes': {},
        'industry': null,
        'latitude': 0.0,
        'linkedin_url': null,
        'logo_url': null,
        'longitude': 0.0,
        'name': 'Dev1',
        'primary_contact_email': null,
        'primary_contact_first_name': null,
        'primary_contact_last_name': null,
        'primary_contact_phone': null,
        'website_url': null
      },
      'subject': 'This is a test {{text}}',
      'text': 'My Text',
      'user': {
        'address': {
          'city': null,
          'formatted': null,
          'postal_code': null,
          'state': null,
          'street_address_1': null,
          'street_address_2': null
        },
        'email': 'gwhite@kupulau.com',
        'email_addresses': [
          {
            'address': 'a@a.com'
          },
          {
            'address': 'gwhite@kupulau.com',
            'type': null
          }
        ],
        'extended_attributes': {},
        'first_name': 'First',
        'industry': {
          'code': 4,
          'name': 'Computer Software'
        },
        'last_name': 'Last',
        'linkedin_url': null,
        'mailing_addresses': [
          {
            'city': 'Concord',
            'postal_code': '01742',
            'state': 'MA',
            'street_address_1': '66 Crowell Field Dr',
            'street_address_2': 'Apt. 123',
            'type': null
          }
        ],
        'name': 'First Last',
        'phone_number': '(978) 371-8092',
        'phone_numbers': [
          {
            'number': '+19783718092',
            'type': null
          }
        ],
        'primary_email': 'a@a.com',
        'profile_photo_url': null,
        'title': null,
        'short_title': null,
        'long_title': null,
        'website_url': null
      },
      'candidate': {
        'address': {
          'city': null,
          'formatted': null,
          'postal_code': null,
          'state': null,
          'street_address_1': null,
          'street_address_2': null
        },
        'email': 'gwhite@kupulau.com',
        'email_addresses': [
          {
            'address': 'a@a.com'
          },
          {
            'address': 'gwhite@kupulau.com',
            'type': null
          }
        ],
        'extended_attributes': {},
        'first_name': 'First',
        'industry': {
          'code': 4,
          'name': 'Computer Software'
        },
        'last_name': 'Last',
        'linkedin_url': null,
        'mailing_addresses': [
          {
            'city': 'Concord',
            'postal_code': '01742',
            'state': 'MA',
            'street_address_1': '66 Crowell Field Dr',
            'street_address_2': 'Apt. 123',
            'type': null
          }
        ],
        'name': 'First Last',
        'phone_number': '(978) 371-8092',
        'phone_numbers': [
          {
            'number': '+19783718092',
            'type': null
          }
        ],
        'primary_email': 'a@a.com',
        'profile_photo_url': null,
        'title': null,
        'website_url': null,
        'company_name': null,
        'short_company_name': null,
        'long_company_name': null,
        'profile_url': '#'
      },
      'unsubscribe': null
    },
    previewCandidate: {
      'id': 3,
      'date_created': '2017-03-10T02:40:29.966849Z',
      'last_updated': '2017-03-10T02:40:29.966910Z',
      'name': 'Albert Einstein',
      'first_name': 'Albert',
      'last_name': 'Einstein',
      'lead_source': 'import',
      'linkedin_url': 'https://linkedin.com/in/al-einstein-a4300726',
      'email': 'albert.einstein@gmail.com',
      'primary_email': 'albert.einstein@gmail.com',
      'industry_code': 1,
      'emails': [{
        'type': null,
        'address': 'albert.einstein@gmail.com'
      }],
      'address': {
        'city': 'Princeton',
        'type': null,
        'state': 'NJ',
        'postal_code': '08540',
        'street_address_1': '112 Mercer St.',
        'street_address_2': 'Apt. 123'
      },
      'addresses': [{
        'city': 'Princeton',
        'type': null,
        'state': 'NJ',
        'postal_code': '08540',
        'street_address_1': '112 Mercer St.',
        'street_address_2': 'Apt. 123'
      }],
      'phone': '(617) 555-5555',
      'phones': [{
        'type': '',
        'number': '+16175555555',
        'enhanced': false
      }],
      'profile_photo_url': 'https://pbs.twimg.com/profile_images/653259954988576768/RCrYrzHS.jpg',
      'company_name': 'E+C',
      'short_company_name': 'E+C',
      'long_company_name': 'Einstein & Co.',
      'title': 'CSO',
      'short_title': 'CSO',
      'long_title': 'Chief Scientist'
    }
  },

  get () {
    return this.store
  },

  // initObject default value is an empty array. When we have custom merge tags, we can pass them here
  init (initObject = []) {
    // Flatten the human readable merge tag object into an array of merge tags
    let startArray = []
    let index = 0
    $.each(this.store.mergeTagObject, (key, value) => {
      // Get all child merge tags
      let children = this.getAllChildMergeTags(key, value)
      $.each(children, (k, v) => {
        let humanReadableName = this.titleCase(v)
        startArray.push({
          id: index,
          name: humanReadableName,
          value: v
        })
        index += 1
      })
    })
    this.store.mergeTags = startArray.concat(initObject)

    // This is for printing the arry to use in our unit tests
    // console.log(JSON.stringify(this.store.mergeTags))
  },

  // After some long and hard fought hours... this recursive function successfully returns an array of all
  // the children merge tags of the passed parent tag
  getAllChildMergeTags (parent, value, childMergeTags = []) {
    if (typeof value === 'object' && !Array.isArray(value) && value != null) {
      $.each(value, (k, v) => {
        this.getAllChildMergeTags(parent + '.' + k, v, childMergeTags)
      })
      return childMergeTags
    } else {
      childMergeTags.push(parent)
      return childMergeTags
    }
  },

  // Function to make the human readable name of the merge tag
  titleCase (str) {
    str = str.toString().replace(/\./g, ' ').replace(/_/g, ' ')
    return str.replace(/\w\S*/g, function (txt) { return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase() })
  }

}

export default mergeTagsStore
