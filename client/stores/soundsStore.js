import notification from 'media/notification.mp3'

let soundsStore = {

  store: {
    sounds: {
      pop: new Audio(notification)
    }
  },

  get () {
    return this.store
  },

  playSound (sound) {
    this.store.sounds[sound].play()
  },

  pauseSound (sound) {
    this.store.sounds[sound].pause()
  }

}

export default soundsStore
