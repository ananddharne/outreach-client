let jobsStore = {

  store: {
    jobs: [],
    unsavedJob: null,
    newJobTemplate: {
      'id': 'new',
      'hiring_manager': null,
      'active': true,
      'title': '',
      'description': '<p>Tell candidates what gets you excited about working for your company. What sets your company apart? Is it your company’s mission, product, vision for the future, founding story, or core values? Why should candidates want to join your team? Are you the best at what you do, a leading innovator in your field, or a growing organization with a strong track record?</p><h2>RESPONSIBILITIES</h2><ul><li>Responsibility 1</li><li>Responsibility 2</li><li>Responsibility 3</li><li>Responsibility 4</li></ul><h2><br></h2><h2>REQUIREMENTS</h2><ol><li>Requirement 1</li><li>Requirement 2</li><li>Requirement 3</li><li>Requirement 4</li></ol>',
      'latitude': 0,
      'longitude': 0,
      'place_id': null,
      'compensation': '',
      'category': '',
      'published_url': ''
    }
  },

  init (initArray) {
    this.store.jobs = initArray
  },

  get () {
    return this.store
  },

  getJob (id) {
    let job
    $.each(this.store.jobs, (key, value) => {
      if (value.id == id) {
        job = value
      }
    })
    return job
  },

  // Make a locations array so it is easier to manipulate in the modal
  formatJobs (jobs) {
    jobs.forEach(job => {
      job.locations = [
        {
          location: job.location,
          place_id: job.place_id,
          latitude: job.latitude,
          longitude: job.longitude,
          vm: { place: { place_id: null } }
        }
      ]
      if (job.location_2) {
        job.locations.push({
          location: job.location_2,
          place_id: '',
          latitude: 0,
          longitude: 0,
          vm: { place: { place_id: null } }
        })
      }
      if (job.location_3) {
        job.locations.push({
          location: job.location_3,
          place_id: '',
          latitude: 0,
          longitude: 0,
          vm: { place: { place_id: null } }
        })
      }
    })
    return jobs
  },

  // Pass an array of jobs
  addJobs (newJobs) {
    // Only add new jobs if there is length, i.e. the store has been initiated,
    // Otherwise do the init here
    let formattedJobs = this.formatJobs(newJobs)
    if (this.store.jobs.length) {
      this.store.jobs = this.store.jobs.concat(formattedJobs)
    } else {
      this.init(formattedJobs)
    }
  },

  // Pass an array of jobs
  updateJobs (updatedJobs) {
    let formattedJobs = this.formatJobs(updatedJobs)
    $.each(formattedJobs, (newKey, newValue) => {
      $.each(this.store.jobs, (oldKey, oldValue) => {
        if (oldValue.id == newValue.id) {
          this.store.jobs[oldKey] = newValue
        }
      })
    })

    return this.store
  },

  // Pass an array of ids
  deleteJobs (jobsToDelete) {
    let newJobs = $.grep(this.store.jobs, (value, key) => {
      let match = true
      $.each(jobsToDelete, (toDeleteKey, toDeleteValue) => {
        if (value.id === toDeleteValue) {
          match = false
        }
      })
      return match
    })

    this.store.jobs = newJobs
  },

  setUnsavedJob (job) {
    this.store.unsavedJob = job
  },

  getUnsavedJob () {
    return this.store.unsavedJob
  },

  resetUnsavedJob () {
    this.store.unsavedJob = null
  },

  clear () {
    this.store.jobs = []
  }

}

export default jobsStore
