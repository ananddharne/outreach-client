let organizationStore = {

  store: {
    organization: {
      name: '',
      address: null,
      industry: {},
      website_url: '',
      linkedin_url: '',
      primary_contact_first_name: '',
      primary_contact_last_name: '',
      primary_contact_phone: '',
      primary_contact_email: '',
      logo_url: '',
      sms_enabled: false,
      sweep_enabled: false,
      sweep_delay: 0
    },
    organizations: []
  },

  get () {
    return this.store
  },

  set (key, value) {
    if (key) {
      this.store.organization[key] = value
    }
  },

  init (initObject) {
    this.store.organizations = initObject
  },

  setCurrentOrganization (organization) {
    this.store.organization = organization
    if (organization) {
      this.updateOrganizations(organization)
    }
  },

  // Pass the updated org to keep the organization array up to date
  updateOrganizations (updatedOrganization) {
    $.each(this.store.organizations, (key, value) => {
      if (updatedOrganization.id == value.id) {
        this.store.organizations[key] = updatedOrganization
        return false
      }
    })

    return this.store
  }

}

export default organizationStore
