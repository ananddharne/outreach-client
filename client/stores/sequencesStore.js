let sequencesStore = {

  store: {
    sequences: [],
    unsavedSequence: null
  },

  init (initArray) {
    this.store.sequences = initArray
  },

  get () {
    return this.store
  },

  getSequence (id) {
    let sequence
    $.each(this.store.sequences, (key, value) => {
      if (value.id == id) {
        sequence = value
      }
    })
    return sequence
  },

  // Pass an array of sequences
  addSequences (newSequences) {
    // Only add new sequences if there is length, i.e. the store has been initiated
    if (this.store.sequences.length) {
      this.store.sequences = this.store.sequences.concat(newSequences)
    } else {
      this.init(newSequences)
    }
  },

  // Pass an array of sequences
  updateSequences (updatedSequences) {
    $.each(updatedSequences, (newKey, newValue) => {
      $.each(this.store.sequences, (oldKey, oldValue) => {
        if (oldValue.id == newValue.id) {
          this.store.sequences[oldKey] = newValue
        }
      })
    })

    return this.store
  },

  // Pass an array of ids
  deleteSequences (sequencesToDelete) {
    let newSequences = $.grep(this.store.sequences, (value, key) => {
      let match = true
      $.each(sequencesToDelete, (toDeleteKey, toDeleteValue) => {
        if (value.id === toDeleteValue) {
          match = false
        }
      })
      return match
    })

    this.store.sequences = newSequences
  },

  setUnsavedSequence (sequence) {
    this.store.unsavedSequence = sequence
  },

  getUnsavedSequence () {
    return this.store.unsavedSequence
  },

  resetUnsavedSequence () {
    this.store.unsavedSequence = null
  },

  clear () {
    this.store.sequences = []
  }
}

export default sequencesStore
