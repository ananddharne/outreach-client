let conversationsStore = {

  store: {
    conversations: [],
    candidates: [],
    filters: {
      company: 0,
      organization: 0,
      req: 0,
      unread: 0,
      starred: 0,
      archived: 0,
      candidate_name: '',
      user_email: '',
      limit: 100,
      offset: 0
    }
  },

  get () {
    return this.store
  },

  init (initObject) {
    this.store.conversations = initObject
  },

  add (convos) {
    this.store.conversations = this.store.conversations.concat(convos)
  },

  update (conversation) {
    $.each(this.store.conversations, (index, convo) => {
      if (conversation.id == convo.id) {
        $.each(convo, (key, value) => {
          if (conversation[key] !== undefined) {
            convo[key] = conversation[key]
          }
        })
        return false
      }
    })
  },

  delete (conversation) {
    let index = this.store.conversations.findIndex((convo) => {
      return convo.id == conversation.id
    })
    if (index !== -1) {
      this.store.conversations.splice(index, 1)
    }
  },

  clearFilters () {
    this.store.filters = {
      company: 0,
      organization: 0,
      req: 0,
      unread: 0,
      starred: 0,
      archived: 0,
      candidate_name: '',
      user_email: '',
      limit: 100,
      offset: 0
    }
  }

}

export default conversationsStore
