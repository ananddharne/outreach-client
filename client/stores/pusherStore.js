import Pusher from 'pusher-js'
import { metadataStore } from 'stores'

const pusherStore = {
  store: {
    app_id: null,
    key: null,
    secret: null,
    pusher: null,
    channels: {
      user: {
        name: '',
        channel: null,
        promise: $.Deferred()
      },
      org: {
        name: '',
        channel: null,
        promise: $.Deferred()
      }
    }
  },

  get () {
    return this.store
  },

  init (response) {
    this.store.app_id = response.pusher_app_id
    this.store.key = response.pusher_key
    this.store.secret = response.secret
    this.store.pusher = new Pusher(this.store.key, {
      authEndpoint: `${response.url_base}pusher/auth`,
      encrypted: true,
      auth: {
        headers: {
          authorization: `Token ${response.token}`
        }
      }
    })

    // Subscribe to the user channel
    this.store.channels.user.name = `private-user-${response.user.id}`
    this.store.channels.user.channel = this.store.pusher.subscribe(this.store.channels.user.name)
    this.store.channels.user.channel.bind('pusher:subscription_succeeded', () => {
      this.store.channels.user.promise.resolve()
    })

    // Subscribe to the org channel
    // Check if they are a sourcer
    let name = response.user.groups.indexOf('Sourcers') !== -1 && response.user.company.name == 'Happie' && response.user.current_organization.name == 'Sourcing' ? 'sourcer' : response.user.current_organization.id
    this.store.channels.org.name = `private-organization-${name}`
    this.store.channels.org.channel = this.store.pusher.subscribe(this.store.channels.org.name)
    this.store.channels.org.channel.bind('pusher:subscription_succeeded', () => {
      this.store.channels.org.promise.resolve()
    })

    // Bind to new build event
    this.store.channels.org.channel.bind('build_check', (data) => {
      const storeData = metadataStore.get()
      if (data.build_sha !== storeData.build_sha) {
        metadataStore.set('buildMismatch', true)
      }
    })
  },

  unsubscribe () {
    // Unsubscribe from the channels
    if (this.store.channels.user.channel) {
      this.store.channels.user.promise
      .done(() => {
        this.store.pusher.unsubscribe(this.store.channels.user.name)
      })
    }
    if (this.store.channels.org.channel) {
      this.store.channels.org.promise
      .done(() => {
        this.store.pusher.unsubscribe(this.store.channels.org.name)
      })
    }
    // Reset the channels
    this.store.channels.user.channel = null
    this.store.channels.org.channel = null
    this.store.channels.user.promise = $.Deferred()
    this.store.channels.org.promise = $.Deferred()
  }

}

export default pusherStore
