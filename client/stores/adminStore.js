let adminStore = {

  store: {
    users: [],
    companies: [],
    organizations: [],
    is_system_admin: false,
    is_company_admin: false,
    is_sourcer_admin: false,
    is_impersonating: false,
    admin_token: null,
    hasInit: false
  },

  get () {
    return this.store
  },

  set (key, value) {
    if (key) {
      this.store[key] = value
    }
  }

}

export default adminStore
