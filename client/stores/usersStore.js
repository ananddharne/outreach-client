let usersStore = {

  store: {
    users: []
  },

  get () {
    return this.store
  },

  init (initObject) {
    this.store.users = initObject
  }
}
export default usersStore
