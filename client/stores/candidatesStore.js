let candidatesStore = {

  store: {
    candidates: [],
    totalCount: 0,
    page: 1,
    filters: {
      req_ids: '',
      statuses: '',
      date_created_start: null,
      date_created_end: null,
      list_ids: '',
      creator_ids: '',
      sources: '',
      city: '',
      state: '',
      search: '',
      sort_key: 'first_name',
      company_id: 0,
      organization_id: 0,
      offset: 0
    }
  },

  candidateModel: {
    first_name: '',
    last_name: '',
    company_name: '',
    short_company_name: '',
    linkedin_url: '',
    title: '',
    short_title: '',
    industry: -1,
    addresses: [],
    emails: [],
    phones: [],
    lead_source: '',
    status: '',
    comment_count: 0,
    comments: [],
    profile_photo_url: '',
    resume_uploaded: false,
    req_ids: [],
    list_ids: []
  },

  get () {
    return this.store
  },

  set (key, value) {
    if (key) {
      this.store[key] = value
    }
  },

  init (initObject) {
    let mappedCandidates = this.mapCandidates(initObject)
    this.store.candidates = mappedCandidates
  },

  mapCandidates (newCandidates) {
    $.each(newCandidates, (key, value) => {
      $.each(this.candidateModel, (modelKey, modelValue) => {
        if (!value[modelKey]) {
          value[modelKey] = modelValue
        }
      })
    })
    return newCandidates
  },

  addCandidates (newCandidates) {
    let mappedCandidates = this.mapCandidates(newCandidates)

    this.store.candidates = this.store.candidates.concat(mappedCandidates)
  },

  addCommentToCandidate (comment, candidate) {
    $.each(this.store.candidates, (key, value) => {
      if (value.id == candidate.id) {
        if (!value.comments) value.comments = []
        value.comments.push(comment)
        value.comment_count++
        return false
      }
    })
  },

  editCommentOnCandidate (comment, candidate) {
    $.each(this.store.candidates, (key, value) => {
      if (value.id == candidate.id) {
        $.each(value.comments, (k, v) => {
          if (v.id == comment.id) {
            v.comment = comment.comment
            return false
          }
        })
        return false
      }
    })
  },

  removeCommentFromCandidate (comment, candidate) {
    $.each(this.store.candidates, (key, value) => {
      if (value.id == candidate.id) {
        $.each(value.comments, (k, v) => {
          if (v.id == comment.id) {
            value.comments.splice(k, 1)
            value.comment_count--
            return false
          }
        })
        return false
      }
    })
  },

  deleteCandidates (candidatesToDelete) {
    let newCandidates = $.grep(this.store.candidates, (value, key) => {
      let match = true
      $.each(candidatesToDelete, (toDeleteKey, toDeleteValue) => {
        if (value.id === toDeleteValue) {
          match = false
        }
      })
      return match
    })

    this.store.candidates = newCandidates
  },

  applyQueryFilters (query) {
    const filterKeys = Object.keys(this.store.filters)
    for (let key in query) {
      if (~filterKeys.indexOf(key)) {
        this.store.filters[key] = query[key]
      }
    }
  },

  clearFilters () {
    this.store.filters = {
      req_ids: '',
      statuses: '',
      date_created_start: null,
      date_created_end: null,
      list_ids: '',
      response_types: '',
      creator_ids: '',
      sources: '',
      city: '',
      state: '',
      search: '',
      sort_key: 'first_name',
      company_id: 0,
      organization_id: 0,
      offset: 0
    }
    this.store.page = 1
  }

}

export default candidatesStore
