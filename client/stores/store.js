// TODO: Remove this file, right now just for reference for Vuex

import Vue from 'vue'
import Vuex from 'vuex'

import _ from 'lodash'

Vue.use(Vuex)

const state = {
  count: 0,
  campaigns: []
}

const mutations = {
  INCREMENT (state) {
    state.count++
  },
  DECREMENT (state) {
    state.count--
  },
  LOAD_CAMPAIGNS (state, campaigns) {
    state.campaigns = campaigns
  },
  TOGGLE_CAMPAIGN_ENABLED (state, id) {
    let campaign = _.find(state.campaigns, (o) => {
      return o.id === id
    }) || {}

    if (campaign.enabled) {
      campaign.enabled = false
    } else {
      campaign.enabled = true
    }
  }
}

const actions = {
  incrementAsync ({ commit }) {
    setTimeout(() => {
      commit('INCREMENT')
    }, 200)
  },
  loadCampaigns ({ commit }) {
    setTimeout(() => {
      commit('LOAD_CAMPAIGNS', [
        { 'name': 'Principal Embedded Software Engineer',
          steps: 4,
          active: 89,
          scheduled: 47,
          paused: 21,
          enabled: false,
          id: 1 },
        {
          'name': 'Senior Account Executive',
          steps: 6,
          active: 122,
          scheduled: 64,
          paused: 39,
          enabled: true,
          id: 3
        }
      ])
    }, 1000)
  },
  toggleCampaignEnabled ({ commit }, options) {
    commit('TOGGLE_CAMPAIGN_ENABLED', options.id)
  }
}

const store = new Vuex.Store({
  state,
  mutations,
  actions
})

export default store
