let accountStore = {

  // Wrapped in outer object so changes can be watched
  store: {
    account: {
      first_name: '',
      last_name: '',
      title: '',
      linkedin_url: '',
      email: '',
      phones: [{
        type: '',
        number: ''
      }],
      profile_photo_url: '',
      id: '',
      company: '',
      current_organization: '',
      timezone: '',
      notification_filters: [''],
      addresses: [{
        street_address_1: '',
        street_address_2: '',
        city: '',
        state: '',
        postal_code: ''
      }],
      token: ''
    }
  },

  get () {
    return this.store
  },

  set (key, value) {
    this.store.account[key] = value
  },

  init (initObject) {
    let user = initObject.user

    // Users should always be in a company, but add this check just so app doesn't blow up
    if (!user.company) {
      user.company = {}
      console.error('User does not belong to a company.')
    }

    if (!user.current_organization) {
      user.current_organization = {}
      console.error('User does not belong to a organization.')
    }

    if (user.current_organization.is_transactional) {
      user.is_transactional = true
    }

    // Cycle through groups and set flags on the user
    $.each(user.groups, (key, value) => {
      switch (value) {
        case 'Sourcers':
          user.is_sourcer = true
          user.is_transactional = false
          break
        case 'Account Managers':
          user.is_manager = true
          user.is_transactional = false
          break
      }
    })

    if (initObject.is_system_admin || initObject.is_company_admin) {
      user.is_transactional = false
    }

    // Check to load sourced reqs
    if (user.is_sourcer && user.company.name == 'Happie' && user.current_organization.name == 'Sourcing') {
      user.is_sourced_req_admin = true
    }

    // Add the token to the user object
    user.token = initObject.token

    this.store.account = user
  }

}

export default accountStore
