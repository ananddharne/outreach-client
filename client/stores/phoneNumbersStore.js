let phoneNumbersStore = {

  store: {
    phones: []
  },

  init (initArray) {
    this.store.phones = initArray
  },

  get () {
    return this.store
  },

  // Pass an array of phones
  addPhones (newPhones) {
    this.store.phones = this.store.phones.concat(newPhones)
  }

}

export default phoneNumbersStore
