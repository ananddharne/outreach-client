let companyStore = {

  store: {
    company: {
      name: '',
      address: null,
      industry: {},
      website_url: '',
      linkedin_url: '',
      primary_contact_first_name: '',
      primary_contact_last_name: '',
      primary_contact_phone: '',
      primary_contact_email: '',
      logo_url: ''
    }
  },

  get () {
    return this.store
  },

  set (key, value) {
    this.store.company[key] = value
  },

  init (initObject) {
    this.store.company = initObject
  }

}

export default companyStore
