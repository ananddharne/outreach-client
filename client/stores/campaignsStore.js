let campaignsStore = {

  store: {
    campaigns: [],
    unsavedCampaign: null
  },

  init (initArray) {
    this.store.campaigns = initArray
  },

  get () {
    return this.store
  },

  getCampaign (id) {
    let campaign
    $.each(this.store.campaigns, (key, value) => {
      if (value.id == id) {
        campaign = value
      }
    })
    return campaign
  },

  // Pass an array of campaigns
  addCampaigns (newCampaigns) {
    // Only add new campaigns if there is length, i.e. the store has been initiated
    if (this.store.campaigns.length) {
      this.store.campaigns = this.store.campaigns.concat(newCampaigns)
    } else {
      this.init(newCampaigns)
    }
  },

  // Pass an array of campaigns
  updateCampaigns (updatedCampaigns) {
    $.each(updatedCampaigns, (newKey, newValue) => {
      $.each(this.store.campaigns, (oldKey, oldValue) => {
        if (oldValue.id == newValue.id) {
          this.store.campaigns[oldKey] = newValue
        }
      })
    })

    return this.store
  },

  // Pass an array of ids
  deleteCampaigns (campaignsToDelete) {
    let newCampaigns = $.grep(this.store.campaigns, (value, key) => {
      let match = true
      $.each(campaignsToDelete, (toDeleteKey, toDeleteValue) => {
        if (value.id === toDeleteValue) {
          match = false
        }
      })
      return match
    })

    this.store.campaigns = newCampaigns
  },

  setUnsavedCampaign (campaign) {
    this.store.unsavedCampaign = campaign
  },

  getUnsavedCampaign () {
    return this.store.unsavedCampaign
  },

  resetUnsavedCampaign () {
    this.store.unsavedCampaign = null
  },

  clear () {
    this.store.campaigns = []
  }

}

export default campaignsStore
