let templatesStore = {

  store: {
    templates: [],
    unsavedTemplate: null
  },

  init (initArray) {
    this.store.templates = initArray
  },

  get () {
    return this.store
  },

  getTemplate (id) {
    let template
    $.each(this.store.templates, (key, value) => {
      if (value.id == id) {
        template = value
      }
    })
    return template
  },

  // Pass an array of templates
  addTemplates (newTemplates) {
    // Only add new templates if there is length, i.e. the store has been initiated
    if (this.store.templates.length) {
      this.store.templates = this.store.templates.concat(newTemplates)
    } else {
      this.init(newTemplates)
    }
  },

  // Pass an array of templates
  updateTemplates (updatedTemplates) {
    $.each(updatedTemplates, (newKey, newValue) => {
      $.each(this.store.templates, (oldKey, oldValue) => {
        if (oldValue.id == newValue.id) {
          this.store.templates[oldKey] = newValue
        }
      })
    })

    return this.store
  },

  // Pass an array of ids
  deleteTemplates (templatesToDelete) {
    let newTemplates = $.grep(this.store.templates, (value, key) => {
      let match = true
      $.each(templatesToDelete, (toDeleteKey, toDeleteValue) => {
        if (value.id === toDeleteValue) {
          match = false
        }
      })
      return match
    })

    this.store.templates = newTemplates
  },

  setUnsavedTemplate (template) {
    this.store.unsavedTemplate = template
  },

  getUnsavedTemplate () {
    return this.store.unsavedTemplate
  },

  resetUnsavedTemplate () {
    this.store.unsavedTemplate = null
  },

  clear () {
    this.store.templates = []
  }

}

export default templatesStore
