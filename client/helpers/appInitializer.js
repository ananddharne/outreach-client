import api from 'api'
import * as stores from 'stores'
import { modals } from 'helpers'

const appInitializer = {
  init (response) {
    stores.accountStore.init(response)
    stores.companyStore.init(response.user.company)
    stores.organizationStore.init(response.organizations)
    stores.organizationStore.setCurrentOrganization(response.user.current_organization)
    // TODO: this is also where we can pass custom merge tags POST MVP
    stores.mergeTagsStore.init()
    // Set the environment domain as a cookie
    stores.metadataStore.setEnvironment(response)

    // Initialize pusher
    stores.pusherStore.init(response)

    // Format timezones for dropdowns
    stores.timezonesStore.formatTimezones()

    // Show dialog if not in an organization
    if (!response.user.current_organization) {
      modals.show({
        component: 'Alert',
        props: {
          message: `Uh-oh, it looks like you aren't part of an organization yet. Please contact Happie for help.`,
          confirmText: 'OK',
          confirm: () => {
            modals.hide()
          }
        }
      })
    }

    // Init users here so we don't have to do it anywhere else
    api.ajax('users')
    .done((response) => {
      stores.usersStore.init(response)
    })

    // Init industries
    api.ajax('industries', null, {
      after (response) {
        // Convert data to display label and value as code
        $.each(response, (key, value) => {
          response[key].value = value.code
          response[key].label = value.name
          delete value.name
        })
      }
    })
    .done((response) => {
      stores.industriesStore.init(response)
    })

    // Init phones
    api.ajax('tracking-numbers')
    .done((response) => {
      stores.phoneNumbersStore.init(response)
    })

    // If there is an admin token, init it
    if (stores.authStore.getAdminToken()) {
      const adminToken = JSON.parse(stores.authStore.getAdminToken())
      stores.adminStore.set('is_system_admin', adminToken.isSystemAdmin)
      stores.adminStore.set('is_company_admin', adminToken.isCompanyAdmin)
      stores.adminStore.set('is_sourcer_admin', adminToken.isSourcerAdmin)
      stores.adminStore.set('admin_token', adminToken.token)
      stores.adminStore.set('is_impersonating', true)
      // stores.adminStore.set('hasInit', true);
    }

    // Set admin data if the user is an admin and we haven't already init
    let isSourcerAdmin = response.user.groups.indexOf('Sourcers') !== -1
    if ((response.is_system_admin || response.is_company_admin || isSourcerAdmin) && !stores.adminStore.get().hasInit) {
      stores.adminStore.set('is_system_admin', response.is_system_admin)
      stores.adminStore.set('is_company_admin', response.is_company_admin)
      stores.adminStore.set('admin_token', response.token)
      stores.adminStore.set('is_sourcer_admin', isSourcerAdmin)
      stores.adminStore.set('hasInit', true)

      // Get all the admin data here so we don't have to worry about it anywhere else
      // Get all users
      api.ajax('minimal-users')
      .done((response) => {
        console.log('response from getting admin users', response)

        if (response.length) {
          stores.adminStore.set('users', response)
        }
      })
      .fail((e) => {
        console.error('error getting admin users', e)
      })

      // Get all orgs and companies
      api.ajax('organizations', {
        type: 'GET',
        data: {
          all: 1
        }
      })
      .done((response) => {
        console.log('response from getting admin orgs', response)

        if (response.length) {
          // Get the companies out of the organizations
          let companies = []
          $.each(response, (key, value) => {
            let inCompanies = false
            $.each(companies, (k, v) => {
              if (v.id === value.company.id) {
                inCompanies = true
                return false
              }
            })

            if (!inCompanies) {
              companies.push(value.company)
            }
          })

          stores.adminStore.set('companies', companies)
          stores.adminStore.set('organizations', response)
        }
      })
      .fail((e) => {
        console.error('error getting admin orgs', e)
      })
    }
  },

  clearStores (storesToClear = []) {
    $.each(storesToClear, (index, store) => {
      stores[store].init([])
    })
  },

  clearAllStores () {
    this.clearStores([
      'jobsStore',
      'usersStore',
      'candidatesStore',
      'signaturesStore',
      'listsStore',
      'sequencesStore',
      'campaignsStore',
      'templatesStore',
      'pagesStore',
      'conversationsStore'
    ])

    // Reset the pusher store
    stores.pusherStore.unsubscribe()
  }
}

export default appInitializer
