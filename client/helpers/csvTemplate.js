export default {

  // TODO: Get this data from the file in the build folder called import-template.csv
  template: `First Name,Last Name,Phone,Email,LinkedIn URL,Photo URL,Status,Response Type,Website,Title,Company,Street Address 1,Street Address 2,City,State,Postal Code
  Albert,Einstein,617-555-5555,albert.einstein@gmail.com,https://linkedin.com/in/al-einstein-a4300726,https://pbs.twimg.com/profile_images/653259954988576768/RCrYrzHS.jpg,Engaged,Yes,http://gethappie.me,Chief Scientist,Einstein & Co. ,112 Mercer St., ,Princeton,NJ,8540
`,

  getHref () {
    let csv = 'data:text/csv;charset=utf-8,' + this.template
    let encodedUri = encodeURI(csv)

    return encodedUri
  }
}
