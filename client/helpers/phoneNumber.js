import { PhoneNumberFormat, PhoneNumberUtil } from 'google-libphonenumber'

const phoneNumber = {
  phoneUtil: PhoneNumberUtil.getInstance(),
  PNF: PhoneNumberFormat,
  makeReadable (number) {
    try {
      return this.phoneUtil.format(this.phoneUtil.parse(number, 'US'), this.PNF.US)
    } catch (e) {
      console.warn(e)
      return number
    }
  }
}

export default phoneNumber
