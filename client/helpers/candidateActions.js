import api from 'api'
import { modals, alerts } from 'helpers'
import { candidatesStore, accountStore } from 'stores'

const candidateActions = {

  sendMessage (recipients = [], singleRecipient = false, type = 'both') {
    // TODO: This needs to be refactored in the sendmessage modal because of sms's
    // NOTE: turning this off for now because this needs some thought to handle sms

    // const noEmail = $.grep(this.candidatesStore.candidates, (candidate) => {
    //   return this.checkedCandidates.indexOf(candidate.id) !== -1 && !candidate.emails.length
    // }).length
    // if (this.checkedCandidates.length === 0) {
    //   alerts.show({
    //     status: 'error',
    //     message: 'No candidates selected.'
    //   })
    //   return
    // } else if (this.checkedCandidates.length === 1 && noEmail) {
    //   alerts.show({
    //     status: 'error',
    //     message: 'This candidate has no email address associated with their profile.'
    //   })
    //   return
    // } else if (this.checkedCandidates.length === noEmail) {
    //   alerts.show({
    //     status: 'error',
    //     message: 'None of the selected candidates have email addresses associated with their profiles.'
    //   })
    //   return
    // } else if (this.checkedCandidates.length > 1 && noEmail === 1) {
    //   alerts.show({
    //     status: 'warning',
    //     message: 'One of the selected candidates does not have an email adddress associated with their profile.'
    //   })
    // } else if (this.checkedCandidates.length > 1 && noEmail) {
    //   alerts.show({
    //     status: 'warning',
    //     message: `${noEmail} of the selected candidates do not have email addresses associated with their profiles.`
    //   })
    // }

    modals.show({
      component: 'SendMessageModal',
      props: {
        // noEmail: noEmail,
        candidates: this.checkedCandidates,
        recipients,
        type,
        enableEmail: singleRecipient ? this.hasEmail : true,
        enableSMS: singleRecipient ? this.hasPhone : true,
        containerStyles: {
          width: '700px'
        }
      }
    })
  },

  // Single candidate actions

  submitCandidate () {
    // TODO

    return $.Deferred().resolve()
  },

  approveCandidate () {
    let promise = api.ajax(`candidates/bulk/status`, {
      type: 'POST',
      data: {
        ids: this.checkedCandidates,
        status: 'interested'
      }
    })
    .done((response) => {
      console.log('approved candidates ', response)
      alerts.show({
        status: 'success',
        message: `Good stuff — you successfully approved ${this.candidate.first_name}!`
      })
    })
    .fail((e) => {
      console.error('failed to approve candidates', e)
      alerts.show({
        status: 'error',
        message: 'Uh oh! For some reason, there was an error approving your candidate. Please refresh your page and try again!'
      })
    })

    return promise
  },

  archiveCandidate () {
    let promise = api.ajax(`candidates/bulk/status`, {
      type: 'POST',
      data: {
        ids: this.checkedCandidates,
        status: 'archived'
      }
    })
    .done((response) => {
      console.log('archived candidates ', response)
      alerts.show({
        status: 'success',
        message: `Good stuff — you successfully archived ${this.candidate.first_name}!`
      })
    })
    .fail((e) => {
      console.error('failed to archive candidates', e)
      alerts.show({
        status: 'error',
        message: 'Uh oh! For some reason, there was an error archiving your candidate. Please refresh your page and try again!'
      })
    })

    return promise
  },

  filterCandidate () {
    let promise = api.ajax(`candidates/bulk/status`, {
      type: 'POST',
      data: {
        ids: this.checkedCandidates,
        status: 'filtered'
      }
    })
    .done((response) => {
      console.log('filtered candidates ', response)
      alerts.show({
        status: 'success',
        message: `Good stuff — you successfully filtered ${this.candidate.first_name}!`
      })
    })
    .fail((e) => {
      console.error('failed to filter candidates', e)
      alerts.show({
        status: 'error',
        message: 'Uh oh! For some reason, there was an error filtering your candidate. Please refresh your page and try again!'
      })
    })

    return promise
  },

  addToList (onComplete = () => {}) {
    // Same as bulk add, but 'checkedCandidates' is a computed function
    modals.show({
      component: 'AddToModal',
      props: {
        apiUrl: 'candidate-lists',
        customCallback: (list) => {
          // Add candidates to this list
          api.ajax(`candidate-lists/${list.id}/members`, {
            type: 'POST',
            data: this.checkedCandidates
          })
          .done((response) => {
            alerts.show({
              status: 'success',
              message: `Awesome — you successfully added ${this.candidate.first_name} ${this.candidate.last_name} to ${list.name}! They’ll automatically receive messages in sequences to which your list is added.`
            })
            onComplete()
          })
          .fail(() => {
            alerts.show({
              status: 'error',
              message: 'Uh oh! For some reason, there was an error adding to your list. Please try again, and sorry for the inconvenience!'
            })
          })
        }

      }
    })
  },
  addToSequence () {
    // Same as bulk add, but 'checkedCandidates' is a computed function
    modals.show({
      component: 'AddToModal',
      props: {
        apiUrl: 'sequences',
        customCallback: (sequence) => {
          // Add candidates to this sequence
          api.ajax(`sequences/${sequence.id}/members`, {
            type: 'POST',
            data: this.checkedCandidates
          })
          .done((response) => {
            alerts.show({
              status: 'success',
              message: `Awesome — you successfully added ${this.candidate.first_name} ${this.candidate.last_name} to ${sequence.name}!`
            })
          })
          .fail(() => {
            alerts.show({
              status: 'error',
              message: 'Uh oh! For some reason, there was an error adding to your sequence. Please try again, and sorry for the inconvenience!'
            })
          })
        }

      }
    })
  },
  pauseMessages () {
    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure you want to pause all scheduled messages for ${this.candidate.first_name} ${this.candidate.last_name}? They won’t receive messages in sequences that they are scheduled to receive unless you choose to resume them.`,
        confirm: () => {
          api.ajax('candidates/sequences/pause', {
            type: 'POST',
            data: this.checkedCandidates
          })
          .done((response) => {
            alerts.show({
              message: `Good stuff - you successfully paused all scheduled messages for ${this.candidate.first_name} ${this.candidate.last_name}!`,
              status: 'success'
            })
          })
          .fail(() => {
            alerts.show({
              message: 'Oh no! There was an error. Please try again. Sorry for the inconvenience!',
              status: 'error'
            })
          })
        },
        confirmText: 'Yes, pause them!',
        cancelText: 'Cancel'
      }
    })
  },
  resumeMessages () {
    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure that you want to resume all scheduled messages for ${this.candidate.first_name} ${this.candidate.last_name}? You will unpause all remaining messages in sequences previously paused for them.`,
        confirm: () => {
          api.ajax('candidates/sequences/unpause', {
            type: 'POST',
            data: this.checkedCandidates
          })
          .done((response) => {
            alerts.show({
              message: `Woohoo - you successfully resumed all scheduled messages for ${this.candidate.first_name} ${this.candidate.last_name}!`,
              status: 'success'
            })
          })
          .fail(() => {
            alerts.show({
              message: 'Oh no! There was an error. Please try again. Sorry for the inconvenience!',
              status: 'error'
            })
          })
        },
        confirmText: 'Yes, resume now!',
        cancelText: 'Cancel'
      }
    })
  },
  clearMessages () {
    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure that you want to clear ${this.candidate.first_name} ${this.candidate.last_name} from all sequences? You will not be able to unpause them unless you add them back into sequences.`,
        confirm: () => {
          api.ajax('candidates/sequences/clear', {
            type: 'POST',
            data: this.checkedCandidates
          })
          .done((response) => {
            alerts.show({
              message: `Woohoo - you successfully cleared ${this.candidate.first_name} ${this.candidate.last_name} from all sequences!`,
              status: 'success'
            })
          })
          .fail(() => {
            alerts.show({
              message: 'Oh no! There was an error. Please try again. Sorry for the inconvenience!',
              status: 'error'
            })
          })
        },
        confirmText: 'Yes, clear now!',
        cancelText: 'Cancel'
      }
    })
  },
  deleteCandidate () {
    // Don't allow transactional customers to delete candidates, but archive them instead
    let url = 'candidates'
    let requestObject = {
      type: 'DELETE',
      data: this.checkedCandidates
    }
    const storeData = accountStore.get()
    if (storeData.account.is_transactional) {
      url = 'candidates/bulk/status'
      requestObject = {
        type: 'POST',
        data: {
          ids: this.checkedCandidates,
          status: 'archived'
        }
      }
    }

    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure that you want to delete ${this.candidate.first_name} ${this.candidate.last_name}? You will remove them and all records of them from your database. You cannot undo this, so please be careful!`,
        confirmText: 'Yes, delete them!',
        cancelText: 'Cancel',
        confirm: () => {
          api.ajax(url, requestObject)
          .done(() => {
            // Remove candidates from our local data
            candidatesStore.deleteCandidates(this.checkedCandidates)

            alerts.show({
              message: `You successfully deleted ${this.candidate.first_name} ${this.candidate.last_name}! We wish them well.`,
              status: 'success'
            })

            if (this.isModal) {
              this.hideModal()
            }
          })
          .fail(() => {
            alerts.show({
              message: 'Oh no! There was an error deleting the selecting candidates. Please try again. Sorry for the inconvenience!',
              status: 'error'
            })
          })
        }
      }
    })
  },
  optOut (type) {
    return api.ajax(`candidates/${this.candidate.id}/${type}/opt-out`, {
      type: 'PATCH'
    })
    .done((response) => {
      switch (type) {
        case 'sms':
          this.$set(this.candidate, 'do_not_sms', true)
          break
        case 'email':
          this.$set(this.candidate, 'do_not_email', true)
          break
        case 'all':
          this.$set(this.candidate, 'do_not_sms', true)
          this.$set(this.candidate, 'do_not_email', true)
          break
        default:
          break
      }
      alerts.show({
        message: `You have successfully opted out ${this.candidate.first_name} ${this.candidate.last_name}.`,
        status: 'success'
      })
    })
  },

  // Bulk actions

  bulkStatusChange (onSubmit) {
    modals.show({
      component: 'StatusChangeModal',
      props: { onSubmit }
    })
  },

  bulkSubmit () {
    let deferred = $.Deferred()

    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure that you want to send these candidates to Happie customers? They won’t appear in this account anymore, and customers will be able to see them in their accounts!`,
        confirm: () => {
          // TODO

          deferred.resolve()
        },
        confirmText: 'Yes, send them!',
        cancelText: 'Cancel'
      }
    })

    return deferred
  },

  bulkApprove () {
    let deferred = $.Deferred()

    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure you want to approve ${this.checkedCandidates.length} candidates? They’ll automatically enter active sequences associated with jobs or lists on their profiles, and start receiving scheduled messages!`,
        confirm: () => {
          api.ajax(`candidates/bulk/status`, {
            type: 'POST',
            data: {
              ids: this.checkedCandidates,
              status: 'interested'
            }
          })
          .done((response) => {
            console.log('approved candidates ', response)
            alerts.show({
              status: 'success',
              message: `Good stuff — you successfully approved ${this.checkedCandidates.length} candidates!`
            })

            deferred.resolve()
          })
          .fail((e) => {
            console.error('failed to approve candidates', e)
            alerts.show({
              status: 'error',
              message: 'Uh oh! For some reason, there was an error approving your candidates. Please refresh your page and try again!'
            })
          })
        },
        confirmText: 'Yes, approve them!',
        cancelText: 'Cancel'
      }
    })

    return deferred
  },

  bulkArchive () {
    let deferred = $.Deferred()

    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure you want to archive ${this.checkedCandidates.length} candidates? They’ll be out of sight, and won’t enter any of your sequences. You can always view and edit them later in the general candidates section of Happie.`,
        confirm: () => {
          api.ajax(`candidates/bulk/status`, {
            type: 'POST',
            data: {
              ids: this.checkedCandidates,
              status: 'archived'
            }
          })
          .done((response) => {
            console.log('archived candidates ', response)
            alerts.show({
              status: 'success',
              message: `Good stuff — you successfully archived ${this.checkedCandidates.length} candidates!`
            })

            deferred.resolve()
          })
          .fail((e) => {
            console.error('failed to approve candidates', e)
            alerts.show({
              status: 'error',
              message: 'Uh oh! For some reason, there was an error archiving your candidates. Please refresh your page and try again!'
            })
          })
        },
        confirmText: 'Yes, archive them!',
        cancelText: 'Cancel'
      }
    })

    return deferred
  },

  bulkFilter () {
    let deferred = $.Deferred()

    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure you want to filter ${this.checkedCandidates.length} candidates? They’ll be out of sight, and won’t enter any of your sequences. You can always view and edit them later in the general candidates section of Happie.`,
        confirm: () => {
          api.ajax(`candidates/bulk/status`, {
            type: 'POST',
            data: {
              ids: this.checkedCandidates,
              status: 'filtered'
            }
          })
          .done((response) => {
            console.log('filtered candidates ', response)
            alerts.show({
              status: 'success',
              message: `Good stuff — you successfully filtered ${this.checkedCandidates.length} candidates!`
            })

            deferred.resolve()
          })
          .fail((e) => {
            console.error('failed to filter candidates', e)
            alerts.show({
              status: 'error',
              message: 'Uh oh! For some reason, there was an error filtering your candidates. Please refresh your page and try again!'
            })
          })
        },
        confirmText: 'Yes, filter them!',
        cancelText: 'Cancel'
      }
    })

    return deferred
  },

  bulkAddToSequence () {
    // Same as bulk add, but 'checkedCandidates' is a computed function
    modals.show({
      component: 'AddToModal',
      props: {
        apiUrl: 'sequences',
        customCallback: (sequence) => {
          // Add candidates to this sequence
          api.ajax(`sequences/${sequence.id}/members`, {
            type: 'POST',
            data: this.checkedCandidates
          })
          .done((response) => {
            alerts.show({
              status: 'success',
              message: `Awesome — you successfully added ${this.checkedCandidates.length} candidates to ${sequence.name}!`
            })
          })
          .fail(() => {
            alerts.show({
              status: 'error',
              message: 'Uh oh! For some reason, there was an error adding to your sequence. Please try again, and sorry for the inconvenience!'
            })
          })
        }

      }
    })
  },
  bulkPauseMessages () {
    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure you want to pause all scheduled messages for these ${this.checkedCandidates.length} candidates? They won’t receive messages in sequences that they are scheduled to receive unless you choose to resume them.`,
        confirm: () => {
          api.ajax('candidates/sequences/pause', {
            type: 'POST',
            data: this.checkedCandidates
          })
          .done((response) => {
            alerts.show({
              message: `Good stuff - you successfully paused all scheduled messages for these ${this.checkedCandidates.length} candidates!`,
              status: 'success'
            })
          })
          .fail(() => {
            alerts.show({
              message: 'Oh no! There was an error. Please try again. Sorry for the inconvenience!',
              status: 'error'
            })
          })
        },
        confirmText: 'Yes, pause them!',
        cancelText: 'Cancel'
      }
    })
  },
  bulkResumeMessages () {
    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure that you want to resume all scheduled messages for these ${this.checkedCandidates.length} candidates? You will unpause all remaining messages in sequences previously paused for them.`,
        confirm: () => {
          api.ajax('candidates/sequences/unpause', {
            type: 'POST',
            data: this.checkedCandidates
          })
          .done((response) => {
            alerts.show({
              message: `Woohoo - you successfully resumed all scheduled messages for these ${this.checkedCandidates.length} candidates!`,
              status: 'success'
            })
          })
          .fail(() => {
            alerts.show({
              message: 'Oh no! There was an error. Please try again. Sorry for the inconvenience!',
              status: 'error'
            })
          })
        },
        confirmText: 'Yes, resume now!',
        cancelText: 'Cancel'
      }
    })
  },
  bulkClearMessages () {
    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure that you want to clear these ${this.checkedCandidates.length} candidates from all sequences? You will not be able to unpause them unless you add them back into sequences.`,
        confirm: () => {
          api.ajax('candidates/sequences/clear', {
            type: 'POST',
            data: this.checkedCandidates
          })
          .done((response) => {
            alerts.show({
              message: `Woohoo - you successfully cleared these ${this.checkedCandidates.length} candidates from all sequences!`,
              status: 'success'
            })
          })
          .fail(() => {
            alerts.show({
              message: 'Oh no! There was an error. Please try again. Sorry for the inconvenience!',
              status: 'error'
            })
          })
        },
        confirmText: 'Yes, clear now!',
        cancelText: 'Cancel'
      }
    })
  },

  bulkOptOut (type) {
    let deferred = $.Deferred()

    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure you want to opt ${this.checkedCandidates.length} candidates out of ${type} contact? Once you do, they'll no longer be able to receive messages in steps of your sequences. Be careful!`,
        confirm: () => {
          const requestObject = {
            type: 'POST',
            data: {
              ids: this.checkedCandidates
            }
          }
          switch (type) {
            case 'email':
              requestObject.data.do_not_email = true
              break
            case 'sms':
              requestObject.data.do_not_sms = true
              break
            default: // 'all'
              requestObject.data.do_not_email = requestObject.data.do_not_sms = true
              break
          }
          api.ajax(`candidates/bulk/opt-out/`, requestObject)
          .done((response) => {
            console.log('opted out candidates ', response)
            alerts.show({
              status: 'success',
              message: `Good stuff — you successfully opted ${this.checkedCandidates.length} candidates out of ${type} contact!`
            })

            deferred.resolve()
          })
          .fail((e) => {
            console.error('failed to opt out candidates', e)
            alerts.show({
              status: 'error',
              message: 'Uh oh! For some reason, there was an error opting out your candidates. Please refresh your page and try again!'
            })
          })
        },
        confirmText: 'Yes, opt them out!',
        cancelText: 'Cancel'
      }
    })

    return deferred
  },

  export () {
    api.ajax('candidates/export', {
      data: this.checkedCandidates,
      type: 'POST'
    })
    .done((response) => {
      // Create invisible link with CSV data and trigger click on it
      let csv = 'data:text/csv;charset=utf-8,' + response
      let encodedUri = encodeURI(csv)
      let link = document.createElement('a')
      link.setAttribute('href', encodedUri)
      link.setAttribute('download', 'Happie-Candidate-Export.csv')
      document.body.appendChild(link) // Required for FF

      link.click()

      document.body.removeChild(link)
    })
    .fail((e) => {
      console.error('Error exporting candidates ', e)
    })
  },
  bulkAddToList () {
    modals.show({
      component: 'AddToModal',
      props: {
        apiUrl: 'candidate-lists',
        customCallback: (list) => {
          // Add candidates to this list
          api.ajax(`candidate-lists/${list.id}/members`, {
            type: 'POST',
            data: this.checkedCandidates
          })
          .done((response) => {
            alerts.show({
              status: 'success',
              message: `Awesome — you successfully added ${this.checkedCandidates.length} candidates to ${list.name}! They’ll automatically receive messages in sequences to which your list is added.`
            })
          })
          .fail(() => {
            alerts.show({
              status: 'error',
              message: 'Uh oh! For some reason, there was an error adding to your list. Please try again, and sorry for the inconvenience!'
            })
          })
        }

      }
    })
  },
  deleteCandidates () {
    let deferred = $.Deferred()

    // Don't allow transactional customers to delete candidates, but archive them instead
    let url = 'candidates'
    let requestObject = {
      type: 'DELETE',
      data: this.checkedCandidates
    }
    const storeData = accountStore.get()
    if (storeData.account.is_transactional) {
      url = 'candidates/bulk/status'
      requestObject = {
        type: 'POST',
        data: {
          ids: this.checkedCandidates,
          status: 'archived'
        }
      }
    }

    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure that you want to delete these ${this.checkedCandidates.length} candidates? You will remove these candidates and all records of them from your database. You cannot undo this, so please be careful!`,
        confirmText: 'Yes, delete them!',
        cancelText: 'Cancel',
        cancel: () => {
          deferred.reject()
        },
        confirm: () => {
          api.ajax(url, requestObject)
          .done(() => {
            // Remove candidates from our local data
            candidatesStore.deleteCandidates(this.checkedCandidates)
            // Uncheck all
            $.each(this.candidates, (key, value) => {
              value.isChecked = false
            })
            alerts.show({
              message: 'You successfully deleted these candidates! We wish them well.',
              status: 'success'
            })

            deferred.resolve()
          })
          .fail(() => {
            alerts.show({
              message: 'Oh no! There was an error deleting the selected candidates. Please try again. Sorry for the inconvenience!',
              status: 'error'
            })
          })
        }
      }
    })

    return deferred
  },
  findPersonalInfo () {
    api.ajax('candidates/enhance/', {
      type: 'POST',
      data: this.checkedCandidates
    })
    .done((response) => {
      alerts.show({
        message: 'Enhancement process has begun!',
        status: 'success'
      })
    })
    .fail(() => {
      alerts.show({
        message: 'Oh no! There was an error. Please try again. Sorry for the inconvenience!',
        status: 'error'
      })
    })
  }

}

export default candidateActions
