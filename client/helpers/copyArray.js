export default function clone (base) {
  let newArray = []
  for (let i = 0; i < base.length; i++) {
    newArray[i] = base[i]
  }
  return newArray
};
