import api from 'api'
import { alerts, modals } from 'helpers'
import { jobsStore } from 'stores'

const jobHelper = {
  currentPath: '',
  editJob (id) {
    const promise = $.Deferred()
    this.currentPath = this.$route.path

    // Black magic to make sure the stored path is always valid
    if (!isNaN(this.currentPath.substr(this.currentPath.length - 1))) {
      this.currentPath = this.currentPath.slice(0, this.currentPath.lastIndexOf('/'))
    }

    api.ajax(`reqs/${id}`)
    .done((response) => {
      console.log(response)
      // add id to the route without pushing a new state
      this.$router.replace({ path: `${this.currentPath}/${id}` })
      this.openJobModal(response)
      // since we pulled from the api, update the jobs store in case there is new data.
      jobsStore.updateJobs([response])
      promise.resolve()
    })
    .fail((e) => {
      console.error('Error editing job ', e)
    })
    return promise
  },
  openJobModal (job) {
    modals.show({
      component: 'JobModal',
      props: {
        containerStyles: {
          width: '576px',
          'max-height': '80vh'
        },
        jobModalTools: true,
        job: job,
        // TODO: include deleteJob in this helper, make available as a Boolean pass-in
        deleteJob: this.deleteJob || false,
        saveJob: this.saveJob,
        beforeClose: this.beforeModalClose,
        shareJob: this.shareJob,
        updateJobCallback: (response) => {
          job.id = response.id
          job.published_url = response.published_url
        }
      }
    })
  },
  shareJob (job) {
    modals.show({
      component: 'ShareJobModal',
      props: {
        job: job
      }
    })
  },
  saveJob (id, updateJobCallback) {
    const promise = $.Deferred()
    // grab the old job and the unsaved job from store
    const oldJob = $.extend(true, {}, jobsStore.getJob(id))
    const newJob = $.extend(true, {}, jobsStore.getUnsavedJob())
    // If there were unsaved changes, save the changes, but still save the old job even if there were no changes
    // NOTE: don't ask
    let jobToSave = Object.keys(newJob).length ? newJob : oldJob

    // Format the category
    if (jobToSave.category === 'select') {
      jobToSave.category = ''
    }

    // Format the locations
    jobToSave.locations.forEach((location, index) => {
      // Only save the place_id, lat, lng if it is the first location
      if (!index) {
        jobToSave.location = location.location
        jobToSave.place_id = location.place_id
        jobToSave.latitude = location.latitude
        jobToSave.longitude = location.longitude
      } else { // Otherwise save the string
        jobToSave['location_' + (index + 1)] = location.location
      }
    })

    delete jobToSave.locations

    let url
    let requestType

    if (id === 'new') {
      url = 'reqs'
      delete jobToSave.id
    } else {
      url = `reqs/${id}`
      requestType = 'PUT'
    }

    api.ajax(url, {
      type: requestType,
      data: jobToSave
    })
    .done((response) => {
      console.log('response from save job: ', response)

      if (id === 'new') {
        jobsStore.addJobs([response])
      }

      // If we have an update callback to update the ID for a newly created job, call it
      if (updateJobCallback) {
        updateJobCallback(response)
      }

      // Update the job in our jobs store
      jobsStore.updateJobs([response])
      // Reset the unsaved job in the store
      jobsStore.resetUnsavedJob()

      alerts.show({
        message: "You're off to the races! Your successfully saved your job! You can now add the job to any of your pages in Happie",
        status: 'success'
      })

      // NOTE: turning off the upsell modal for now
      // modals.show({
      //   component: 'JobUpsellModal',
      //   props: {
      //     containerStyles: {
      //       width: '575px'
      //     }
      //   }
      // })
      promise.resolve()
    })
    .fail((e) => {
      console.error('Error updating job ', e)

      alerts.show({
        message: 'Uh oh! There was an error saving your job. Please try saving it again. Sorry for the inconvenience!',
        status: 'error'
      })
    })
    return promise
  },
  beforeModalClose (index) {
    // TODO: maybe check using deep equal to see if the unsaved job differs from the stored job
    // If there is an unsaved job, don't close the modal until they confirm
    if (jobsStore.getUnsavedJob()) {
      modals.show({
        component: 'Confirm',
        props: {
          message: `You currently have unsaved changes. Do you want to save your changes before you leave?`,
          confirmText: 'Yes',
          cancelText: 'No',
          confirm: () => {
            // Make sure they haven't navigated
            if (this.$route.path.indexOf(this.currentPath) !== -1) {
              // Remove the id from the route without pushing a new state
              this.$router.replace({ path: this.currentPath })
            }
            // Save the job
            const id = jobsStore.getUnsavedJob().id
            this.saveJob(id)
            // reset unsaved changes and hide the modal
            jobsStore.resetUnsavedJob()
            modals.hide({
              index: index,
              confirmed: true
            })
          },
          cancel: () => {
            // reset unsaved changes and hide the modal
            jobsStore.resetUnsavedJob()
            modals.hide({
              index: index,
              confirmed: true
            })
            // Make sure they haven't navigated
            if (this.$route.path.indexOf(this.currentPath) !== -1) {
              // Remove the id from the route without pushing a new state
              this.$router.replace({ path: this.currentPath })
            }
          }
        }
      })
    } else {
      // Make sure they haven't navigated
      if (this.$route.path.indexOf(this.currentPath) !== -1) {
        // Remove the id from the route without pushing a new state
        this.$router.replace({ path: this.currentPath })
      }
      modals.hide({
        index: index,
        confirmed: true
      })
    }
  }
}

export default jobHelper
