const messageParser = {
  // Extracts all email matches and returns an array
  emails (str) {
    return str.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi) || []
  },

  // TODO: going to need to check if this is working
  phones (str) {
    const items = []
    const minimum = 9
    let i = 0
    let n = ''
    let min = minimum

    while (i < str.length) {
      switch (str[i]) {
        case '+':                                   // start of international number
          if (n.length >= min) items.push(n)
          n = str[i]
          min = minimum + 2                      // at least 2 more chars in number
          break
        // case '-': case '.': case '(': case ')':     // ignore punctuation
          // break
        case ' ':
          if (n.length >= min) {              // space after consuming enough digits is end of number
            items.push(n)
            n = ''
          } else if (n.length) {
            n += str[i]
          }
          break
        default:
          if (str[i].match(/[0-9]|-|\(|\)|\./)) {            // add digit to and punctuation to number
            n += str[i]
            // if (n.length == 1 && n != '0') {
            //   min = 3                        // local number (extension possibly)
            // }
          } else {
            if (n.length >= min) {
              items.push(n)                  // else end of number
            }
            n = ''
          }
          break
      }
      i++
    }

    if (n.length >= min) {              // EOF
      items.push(n)
    }

    return items
  },

  escapeRegex (value) {
    return value.replace(/([.?*+^$[\]\\(){}|-])/ig, '\\$1')
  }
}

export default messageParser
