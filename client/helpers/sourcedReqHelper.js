import api from 'api'
import moment from 'moment'
import json2csv from 'json2csv'
import { modals, alerts, copyObj } from 'helpers'
import { sourcedReqStore, jobsStore, adminStore, usersStore } from 'stores'

const sourcedReqHelper = {
  openComments () {
    api.ajax(`sourced-reqs/${this.sourcedReq.id}/comments`)
    .done((response) => {
      this.sourcedReq.comments = response
      this.sourcedReq.comment_count = response.length

      modals.show({
        component: 'CandidateComments',
        props: {
          sourcedReq: this.sourcedReq,
          commentsLoaded: true,
          isModal: true,
          containerStyles: {
            width: '512px'
          }
        }
      })
    })
    .fail(() => {
      alerts.show({
        status: 'error',
        message: 'Whoops! There was a problem getting comments. Refresh the page and try again.'
      })
    })
  },
  createNew () {
    let data = {
      sourcer: this.selectedSourcer,
      sales_person: this.selectedSalesperson,
      account_manager: this.selectedAccountManager,
      req: this.selectedJob,
      active: this.isActive,
      name: this.sourcedReqName,
      req_status: this.selectedState,
      sourcing_status: this.selectedStatus,
      state: this.selectedState,
      total_goal: this.sourcingGoal,
      weekly_goal: this.submitGoal,
      submit_due_date: this.submitDueDate,
      engagement_date: this.engagementEndDate,
      syndicated: this.selectedSyndication,
      priority_level: this.selectedPriorityLevel,
      craigslist_status: this.selectedCraigslistSyndication,
      craigslist_date: this.selectedCraigslistDate,
      craigslist_locations: this.selectedCraigslistLocations,
      craigslist_category_id: this.selectedCraigslistCategory,
      craigslist_employment_type: this.selectedCraigslistEmploymentType
    }

    data = sourcedReqHelper.requiredCheck(data)

    api.ajax('sourced-reqs/', {
      type: 'POST',
      data: data
    })
    .done((response) => {
      response.isChecked = false
      response.hovered = false
      sourcedReqStore.add(response)
      console.log('Successfully added sourced req')
      modals.hide()
      alerts.show({
        status: 'success',
        message: 'Good stuff — you successfully created a new sourced req!'
      })
    })
    .fail(() => {
      console.log('Error udating sourced req')
      alerts.show({
        status: 'error',
        message: 'Uh-oh, there was an error creating this sourced req. Are you sure you filled in all fields?'
      })
    })
  },
  update () {
    let data = {
      sourcer: this.selectedSourcer,
      sales_person: this.selectedSalesperson,
      account_manager: this.selectedAccountManager,
      state: this.selectedState,
      req_status: this.selectedState,
      name: this.sourcedReqName,
      total_goal: this.sourcingGoal,
      weekly_goal: this.submitGoal,
      submit_due_date: this.submitDueDate,
      engagement_date: this.engagementEndDate,
      sourcing_status: this.selectedStatus,
      syndicated: this.selectedSyndication,
      priority_level: this.selectedPriorityLevel,
      craigslist_status: this.selectedCraigslistSyndication,
      craigslist_date: this.selectedCraigslistDate,
      craigslist_locations: this.selectedCraigslistLocations,
      craigslist_category_id: this.selectedCraigslistCategory,
      craigslist_employment_type: this.selectedCraigslistEmploymentType
    }

    data = sourcedReqHelper.requiredCheck(data)

    api.ajax(`sourced-reqs/${this.sourcedReq.id}`, {
      type: 'PUT',
      data: data
    })
    .done((response) => {
      response.isChecked = false
      response.hovered = false
      sourcedReqStore.update(response)
      console.log('Successfully updated sourced req')
      modals.hide()
      alerts.show({
        status: 'success',
        message: 'Good stuff — you successfully updated your sourced req!'
      })
    })
    .fail(() => {
      console.log('Error udating sourced req')
      alerts.show({
        status: 'error',
        message: 'Uh-oh, there was an error creating this sourced req. Please refresh the page and try again.'
      })
    })
  },
  refresh () {
    api.ajax(`sourced-reqs/${this.sourcedReq.id}`)
      .done((response) => {
        sourcedReqStore.update(response)
        console.log('Successfully refreshed sourced req')
      })
      .fail(() => {
        alerts.show({
          status: 'error',
          message: `There was an error updating ${this.sourcedReq.name}, please refresh the page.`
        })
      })
  },
  refreshAll () {
    sourcedReqStore.store.sourcedReqs.forEach((sourcedReq) => {
      api.ajax(`sourced-reqs/${sourcedReq.id}`)
        .done((response) => {
          sourcedReqStore.update(response)
          console.log('Successfully refreshed sourced req')
        })
        .fail(() => {
          alerts.show({
            status: 'error',
            message: `There was an error updating ${sourcedReq.name}, please refresh the page.`
          })
        })
    })
  },
  bulkStatusChange () {
    modals.show({
      component: 'sourcedReqStatusModal',
      props: {
        containerStyles: {
          width: '512px'
        }
      }
    })
  },
  requiredCheck (data) {
    // Remove fields that are null and not required
    if (!data.req) delete data.req
    if (!data.status) delete data.status
    if (!data.total_goal) delete data.total_goal
    if (!data.weekly_goal) delete data.weekly_goal
    if (!data.submit_due_date) delete data.submit_due_date
    if (!data.engagement_date) delete data.engagement_date
    if (!data.sales_person) delete data.sales_person
    if (!data.account_manager) delete data.account_manager
    if (!data.sourcing_status) delete data.sourcing_status
    if (!data.syndicated) {
      delete data.syndicated
      delete data.priority_level
    }
    if (!data.craigslist_status) {
      delete data.craigslist_status
      delete data.craigslist_employment_type
      delete data.craigslist_category_id
      delete data.craigslist_locations
      delete data.craigslist_date
    }
    return data
  },
  exportSourcedReqs () {
    let requestObject = {
      type: 'GET',
      data: copyObj(this.searchParams)
    }
    delete requestObject.data.offset
    delete requestObject.data.limit

    api.ajax('sourced-reqs/', requestObject)
    .done((response) => {
      const fields = [
        'name',
        'id',
        'company',
        'account_status',
        'sourcing_status',
        'days_live',
        'emails_sent',
        'smses_sent',
        'enhancements',
        'total_candidates',
        'yes_count',
        'conversion_rate',
        'net_interested_candidates',
        'candidates_last_upload',
        'days_since_last_upload',
        'account_manager',
        'sourcer',
        'sales_person',
        'req_status',
        'date_created',
        'engagement_date',
        'linked_job',
        'organization',
        'job_type',
        'syndicated',
        'priority_level',
        'craigslist_status',
        'craigslist_locations',
        'craigslist_date',
        'craigslist_employment_type',
        'craigslist_category_id'
      ]
      let sourcedReqs = response.sourced_reqs
      sourcedReqs.forEach((sourcedReq) => {
        sourcedReq.name = sourcedReq.name || 'Untitled'

        // Make sourcing status human-readable
        sourcedReq.sourcing_status = sourcedReq.sourcing_status ? sourcedReq.sourcing_status.join('; ') : ''

        // Get the job (req) associated with the sourcedReq
        const req = jobsStore.store.jobs.find((job) => {
          return sourcedReq.req == job.id
        })
        if (typeof (req) === 'undefined') {
          // Cut the mustard. If the req doesn't exist, neither will any of these fields.
          sourcedReq.company = sourcedReq.linked_job = sourcedReq.organization = sourcedReq.job_type = ''
        } else {
          sourcedReq.linked_job = req.title
          sourcedReq.job_type = req.category || ''

          // Get the company name, if it exists.
          const company = adminStore.store.organizations.find((org) => {
            if (org.id == req.organization) return org
          })
          company ? sourcedReq.company = company.company.name : '' // HACK: Just let it happen

          // Get the organization name, if it exists.
          const organization = adminStore.store.organizations.find((org) => {
            return org.id == req.organization
          })
          organization ? sourcedReq.organization = organization.name : ''
        }

        // Get the number of days since the sourced req was created.
        sourcedReq.days_live = moment().diff(moment(sourcedReq.date_created), 'days')

        // Initialize total candidate amount, conversion rate,  and last uploaded amount as 0
        sourcedReq.total_candidates = sourcedReq.candidates_last_upload = 0

        // Initialize days since last upload.
        sourcedReq.days_since_last_upload = ''

        // If there are submissions, get total candidates and last candidates uploaded.
        if (sourcedReq.submissions.length) {
          sourcedReq.candidates_last_upload = sourcedReq.submissions[0].candidates_submitted
          sourcedReq.days_since_last_upload = moment().diff(moment(sourcedReq.submissions[0].last_updated), 'days')
          sourcedReq.submissions.forEach((submission) => {
            sourcedReq.total_candidates += submission.candidates_submitted
          })
        }

        // Get the conversion  rate
        sourcedReq.conversion_rate = getPercent(sourcedReq.yes_count, sourcedReq.total_candidates)
        function getPercent (value, total) {
          let percent = (value / total * 100).toFixed(1)
          return !isNaN(percent) ? `${percent}%` : `0%`
        }

        // Calculate net_interested_candidates
        sourcedReq.net_interested_candidates = sourcedReq.yes_count - sourcedReq.archived_count

        // Get account manager, sourcer, and salesperson.
        if (sourcedReq.account_manager === null) {
          sourcedReq.account_manager = ''
        } else {
          const accountManager = usersStore.store.users.find((user) => {
            return user.id == sourcedReq.account_manager
          })
          accountManager ? sourcedReq.account_manager = `${accountManager.first_name} ${accountManager.last_name}` : ''
        }

        if (sourcedReq.sourcer === null) {
          sourcedReq.sourcer = ''
        } else {
          const sourcer = usersStore.store.users.find((user) => {
            return user.id == sourcedReq.sourcer
          })
          sourcer ? sourcedReq.sourcer = `${sourcer.first_name} ${sourcer.last_name}` : ''
        }

        if (sourcedReq.sales_person === null) {
          sourcedReq.sales_person = ''
        } else {
          const salesPerson = usersStore.store.users.find((user) => {
            return user.id == sourcedReq.sales_person
          })
          salesPerson ? sourcedReq.sales_person = `${salesPerson.first_name} ${salesPerson.last_name}` : ''
        }

        // Designate sourced req as inactive or inactive.
        // TODO: Add more states once they exist in the API.
        sourcedReq.req_status = sourcedReq.active ? 'Active' : 'Inactive'

        sourcedReq.date_created = moment(sourcedReq.date_created).format('MM/DD/YYYY')
        sourcedReq.engagement_date = moment(sourcedReq.engagement_date).format('MM/DD/YYYY')

        // Make syndicated prop human-readable
        sourcedReq.syndicated = sourcedReq.syndicated ? 'Yes' : 'No'
      })

      // Compute values for total columns.
      // TODO: Add more totals once the associated fields exest in the API.
      let totalCandidates = 0
      let totalEmails = 0
      let totalTexts = 0
      let totalEnhancements = 0
      let totalYesCount = 0
      let totalNetInterestedCandidates = 0
      sourcedReqs.forEach((sourcedReq) => {
        totalCandidates += sourcedReq.total_candidates
        totalEmails += sourcedReq.emails_sent
        totalTexts += sourcedReq.smses_sent
        totalEnhancements += sourcedReq.enhancements
        totalYesCount += sourcedReq.yes_count
        totalNetInterestedCandidates += sourcedReq.yes_count - sourcedReq.archived_count
      })
      sourcedReqs.push({
        name: 'TOTALS',
        total_candidates: totalCandidates,
        emails_sent: totalEmails,
        smses_sent: totalTexts,
        enhancements: totalEnhancements,
        yes_count: totalYesCount,
        net_interested_candidates: totalNetInterestedCandidates
      })

      console.log('exporting', sourcedReqs)

      // Squirt it all out in a .csv
      const csv = 'data:text/csv;charset=utf-8,' + json2csv({ data: sourcedReqs, fields })
      const encodeUri = encodeURI(csv)
      const link = document.createElement('a')
      link.setAttribute('href', encodeUri)
      link.setAttribute('download', 'Happie-Sourced-Req-Export.csv')
      document.body.appendChild(link) // Required for FF
      link.click()
      document.body.removeChild(link)
    })
  }
}

export default sourcedReqHelper
