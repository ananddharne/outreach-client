
export default function (parent, amountToCheck) {
  let docViewTop = $(parent).scrollTop()
  let height = $(parent).outerHeight()
  let scrollHeight = $(parent).prop('scrollHeight')

  let heightCheck = scrollHeight - docViewTop - height

  if (heightCheck - amountToCheck <= 0) {
    return true
  } else {
    return false
  }
}
