const modals = {

  // The `event bus` is the mechanism by which we communicate between components and ModalContainer.
  // it should only ever be registered once on app load

  bus: {
    show () {},
    hide () {}
  },

  registerBus (bus) {
    this.bus = bus
  },

  // Show and hide are wrapped in timeouts to ensure they render after "normal" components,
  // to avoid race conditions if a component renders a modal with similar data on component load
  show (options) {
    setTimeout(() => {
      this.bus.show(options)
    })
  },

  hide (options) {
    setTimeout(() => {
      this.bus.hide(options)
    })
  },

  focusSelector (focusSelector) {
    let element = $(focusSelector)
    let top = element.offset().top + element.outerHeight()

    // Get container the element belongs to
    let container = element.parents('.hp-modal-container')

    container.scrollTop(top)

    element.focus()
  }
}

export default modals
