import api from 'api'
import { modals, alerts } from 'helpers'
import { metadataStore, candidatesStore } from 'stores'

export default {
  getCityState (candidate) {
    if (candidate.addresses && candidate.addresses.length > 0) {
      if (candidate.addresses[0].city != '' && candidate.addresses[0].state != '') {
        return `${candidate.addresses[0].city}, ${candidate.addresses[0].state}`
      } else {
        // If one of them is empty, just return both
        return `${candidate.addresses[0].city}${candidate.addresses[0].state}`
      }
    } else {
      return ''
    }
  },

  downloadResume (candidate) {
    api.ajax(`candidates/${candidate.id}/resume`, {
      disableOverlay: false // TODO: Figure out if this should be added or not
    })
    .done((response) => {
      // Create invisible link, click it, then remove it
      // TODO: Ensure works as expected on IE
      let hiddenLink = $(`<a href='${response.url}' target='_blank' download='resume'></a>`)

      hiddenLink.hide()
      .appendTo('body')[0]
      .click()

      hiddenLink.remove()
    })
    .fail((e) => {
      console.error('Failed to get resume ', e)
    })
  },

  deleteResume (candidate) {
    modals.show({
      component: 'Confirm',
      props: {
        message: `Are you sure that you want to delete this candidate's Resume? You will remove this candidate's record your database. You cannot undo this, so please be careful!`,
        confirmText: 'Yes, delete it!',
        cancelText: 'Cancel',
        confirm: () => {
          api.ajax(`candidates/${candidate.id}/resume/`, {
            type: 'DELETE',
            data: this.candidate
          })
          .done(() => {
            // Remove candidates resume from our local data
            let candidate = candidatesStore.store.candidates.find(c => c.id == this.candidate.id)
            candidate.resume_uploaded = false
            alerts.show({
              message: 'You successfully deleted this candidates resume! We wish them well.',
              status: 'success'
            })
            this.$set(this.candidate, 'resume_uploaded', false)
            this.$refs.dropzone.removeAllFiles()
          })
          .fail(() => {
            alerts.show({
              message: 'Oh no! There was an error deleting the selected candidates resume. Please try again. Sorry for the inconvenience!',
              status: 'error'
            })
          })
        }
      }
    })
  },

  openCandidateModal (id, reloadFunction = () => {}) {
    let newQuery = $.extend(true, {}, this.$route.query)
    newQuery.candidate = id
    this.$router.replace({ query: newQuery })
    modals.show({
      component: 'CandidatesProfilePanel',
      transitionType: 'slide-from-right',
      props: {
        candidateIdProp: id,
        positionerStyles: {
          height: '100%'
        },
        containerStyles: {
          'min-height': '550px',
          width: 'auto',
          'background': 'rgba(0,0,0,0)',
          'float': 'right'
        },
        hideXout: true,
        onSave: () => {
          this.reloadRequired = true
        },
        beforeClose: (index) => {
          if (metadataStore.getHasUnsavedChanges()) {
            modals.show({
              component: 'Confirm',
              props: {
                message: `You currently have unsaved changes. Do you want to save your changes before you leave?`,
                confirmText: 'Yes',
                cancelText: 'No',
                confirm: () => {
                  // Save the candidate
                  metadataStore.runSaveChangesFunction()
                  metadataStore.setHasUnsavedChanges(false)

                  if (this.reloadRequired) {
                    reloadFunction()
                    this.reloadRequired = false
                  }
                  modals.hide({
                    index: index,
                    confirmed: true
                  })
                },
                cancel: () => {
                  // reset unsaved changes and hide the modal
                  metadataStore.setHasUnsavedChanges(false)
                  if (this.reloadRequired) {
                    reloadFunction()
                    this.reloadRequired = false
                  }
                  modals.hide({
                    index: index,
                    confirmed: true
                  })
                }
              }
            })
          } else {
            if (this.reloadRequired) {
              reloadFunction()
              this.reloadRequired = false
            }
            modals.hide({
              index: index,
              confirmed: true
            })
          }
        }
      }
    })
  },

  openCandidateComments (candidate) {
    api.ajax(`candidates/${candidate.id}/comments`)
    .done((response) => {
      this.candidate.comments = response
      this.candidate.comment_count = response.length

      modals.show({
        component: 'CandidateComments',
        props: {
          candidate: candidate,
          commentsLoaded: true,
          containerStyles: {
            width: '512px'
          }
        }
      })
    })
    .fail((e) => {
      console.log('failed to get comments for candidate ', e)
    })
  },

  openExperienceModal () {
    // If we are opening this modal, all the experiences should already be loaded
    modals.show({
      component: 'CandidateExperience',
      props: {
        experiences: this.candidate.candidate_profile.experience,
        allExp: true,
        containerStyles: {
          width: '250px'
        }
      }
    })
  },

  minimalMap (response) {
    let candidates = []
    $.each(response, (index, value) => {
      let candidate = {
        id: value[0],
        first_name: value[1],
        last_name: value[2],
        title: value[3],
        company_name: value[4],
        profile_photo_url: value[5],
        status: value[6],
        response_type: value[7],
        resume_uploaded: value[8],
        has_linkedin: value[9],
        has_facebook: value[10],
        isChecked: false
      }
      candidates.push(candidate)
    })
    return candidates
  }
}
