const addProtocol = (url) => {
  if (!url) {
    return ''
  }
  const prefix = 'https://'
  if (url.indexOf('http://') !== 0 && url.indexOf('https://') !== 0 && url.length) {
    return prefix + url
  }

  return url
}

export default addProtocol
