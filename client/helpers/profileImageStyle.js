import placeholder from 'img/account-placeholder.svg'

export default function (url, placeholderArg) {
  let placeholderImage = placeholderArg || placeholder
  if (url) {
    return {
      'background-image': `url('${url}'), url(${placeholderImage})`
    }
  } else {
    return {
      'background-image': `url(${placeholderImage})`
    }
  }
}
