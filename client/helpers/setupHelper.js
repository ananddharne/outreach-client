import api from 'api'
import { alerts, modals } from 'helpers'
import { setupsStore, jobsStore } from 'stores'

const setupHelper = {
  getSetups () {
    api.ajax('transactional-setup/pending')
    .done(response => {
      response.forEach(setup => {
        setup.job_object.job = jobsStore.formatJobs([Object.assign({}, jobsStore.store.newJobTemplate)])[0]
      })
      setupsStore.init(response)
    })
  },
  saveSetup (setup) {
    setup.status = 'reviewed'
    api.ajax(`transactional-setup/${setup.id}`, { type: 'PUT', data: setup })
    .done(response => {
      console.log('saved setup', response)
      alerts.show({
        status: 'success',
        message: 'Save Successful'
      })
    })
  },
  patch (id, data) {
    const dataObject = {
      type: 'PUT',
      data
    }
    console.log(dataObject)
    api.ajax(`transactional-setup/${id}`, dataObject)
    .done(response => {
      console.log('patched', data)
      alerts.show({
        status: 'success',
        message: 'Save Sucecssful'
      })
    })
  },
  markStep (step, label) {
    // Mark in the store
    const key = `step_${step}_done`
    this.setup[key] = !this.setup[key]
    // Send to the api
    const data = {}
    data[key] = this.setup[key]
    data.status = 'reviewed'
    api.ajax(`transactional-setup/${this.setup.id}`, {
      type: 'PUT',
      disableOverlay: true,
      data
    })
    .done(response => {
      console.log(response)
      alerts.show({
        message: this.setup[key]
          ? `Nice work — you successfully marked the “${label}” section as complete!`
          : `Okie dokie — you unmarked the “${label}” section as complete.`,
        status: 'success'
      })
    })
  },
  activateSetup () {
    const setup = this.setup
    const stepsComplete = setup.step_1_done && setup.step_2_done && setup.step_3_done && setup.step_4_done && setup.step_5_done && setup.step_6_done && setup.step_7_done
    modals.show({
      component: 'Confirm',
      props: {
        confirmText: 'Yes',
        cancelText: 'No',
        message: `${!stepsComplete ? 'It looks like not all steps have been marked as complete. ' : ''}Are you sure you want to activate this account? It will go live and you will no longer be able to edit it in setup.`,
        confirm () {
          api.ajax(`transactional-setup/${setup.id}/install`)
          .done(response => {
            console.log(response)
            alerts.show({
              message: 'You successfully activated this transactional account. The job is active, and the customer is being notified!',
              status: 'success'
            })
            // Remove the setup from the local store
            setupsStore.delete(setup.id)
          })
          .fail(e => {
            console.error(e)
            alerts.show({
              message: 'Oh no! There was an error activating the transactional account. Check your work, and if nothing is obvious, please let <a href="mailto:bugs@gethappie.me">bugs@gethappie.me</a> know!',
              status: 'error'
            })
          })
        }
      }
    })
  },
  deleteSetup () {
    const setup = this.setup
    modals.show({
      component: 'Confirm',
      props: {
        confirmText: 'Yes',
        cancelText: 'No',
        message: 'Are you sure you want to delete this pending transactional account setup? You’ll no longer be able to edit it from this panel and the associated sourced req will be deleted.',
        confirm () {
          api.ajax(`transactional-setup/${setup.id}`, {
            type: 'DELETE'
          })
          .done(response => {
            console.log(response)
            alerts.show({
              message: 'You successfully deleted this transactional account!',
              status: 'success'
            })
            // Remove the setup from the local store
            setupsStore.delete(setup.id)
          })
          .fail(e => {
            console.error(e)
            alerts.show({
              message: 'Oh no! There was an error deleting the transactional account. Check your work, and if nothing is obvious, please let <a href="mailto:bugs@gethappie.me">bugs@gethappie.me</a> know!',
              status: 'error'
            })
          })
        }
      }
    })
  }
}

export default setupHelper
