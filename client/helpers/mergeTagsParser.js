import { usersStore } from 'stores'
import utf8 from 'utf8'
import URI from 'urijs'

// TODO: Write some tests with these guys, seem pretty fragile at the moment
export default {

  parseHTML (html) {
    html = this.fixAnchors(html)
    html = this.marginZero(html)
    html = this.parseSpans(html)
    return html
  },

  parseSpans (html) {
    let str = html
    try {
      str = utf8.encode(str)
      str = utf8.decode(str).replace(/`/g, '')
    } catch (err) {
      console.error('utf8 decoder failed ', err)
      str = html
    }
    str = str.replace(/<\/?span[^>]*>/g, '')
    return str
  },

  // Add margin: 0 to paragraphs so emails display the same across all clients.
  marginZero (str) {
    const $str = $($.parseHTML('<span>' + str + '</span>'))
    $($str).find('p').css('margin', 0)
    return $str[0].outerHTML
  },

  // Fix anchor tags because the froala editor adds random characters after merge tags in the href attribute
  // Seems to only happen when a copy/paste occurs
  fixAnchors (html) {
    const $html = $(`<span>${html}</span>`)
    $html.find('a').each((index, elem) => {
      let link = $(elem).attr('href')
      let mergeIndex = link.indexOf('}}') + 1
      let lastIndex = link.length - 1
      // If the link is a merge tag, remove any characters at the end of it
      if (mergeIndex && mergeIndex !== lastIndex) {
        $(elem).attr('href', link.slice(0, mergeIndex - lastIndex))
      }
    })
    return $html[0].outerHTML
  },

  toTags (html) {
    // Replace tags with what we need
    let text = $(html)

    let tags = text.find('.atwho-inserted')

    $.each(tags, (key, value) => {
      let userTag = $(value).find('.user-id').html()

      if (userTag) {
        $(value).replaceWith(`{{user_id ${userTag}}}`)
      }
    })

    // Replace all anchor tag text with their href's
    $.each(text.find('a'), (key, value) => {
      $(value).replaceWith($(value).attr('href'))
    })

    // Convert the html to plain text
    let result = text.text()

    // Convert links back to anchor tags
    result = URI.withinString(result, (url) => {
      return `<a href="${url}" target="_blank">${url}</a>`
    })

    return result
  },
  fromTags (html) {
    // Convert from merge tag syntax to At.js sytax
    let text = html

    text = text.replace(/\{{(.*?)\}}/g, (val) => {
      let userId
      let mergeTag
      let trimmedVal = val.replace(/{{/g, '').replace(/}}/g, '').replace(/ /g, '')

      if (trimmedVal.indexOf('user_id') > -1) {
        userId = trimmedVal.replace(/user_id/g, '')
        userId = parseInt(userId, 10)
      } else {
        // Merge tags get screwy because of the `data-atwho-at-query` attribute with {{,
        // so we need this extra level of parsing which is kinda gross
        let splitValue = val.split('{{')
        // After splitting, get last array item and prepend it with {{
        mergeTag = `{{${splitValue[splitValue.length - 1]}`
      }

      if (userId) {
        // Get name of user
        let users = usersStore.get().users

        let userName
        $.each(users, (key, value) => {
          if (value.id === userId) {
            userName = value.first_name || value.last_name
              ? `${value.first_name}${value.last_name}`
              : `${value.email}`
            return false
          }
        })

        return `<span class="atwho-inserted" data-atwho-at-query="@">@${userName}<span class="hidden user-id">${userId}</span></span>`
      } else {
        return `<span class="atwho-inserted" data-atwho-at-query="{{">${mergeTag}<span class="hidden merge-tag-id">${mergeTag}</span></span>`
      }
    })

    return text
  }

}
