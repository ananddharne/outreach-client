const hoverNumber = {
  init () {
    setTimeout(() => {
      // Unbind the previous hover function so we're not just appending a bunch of hover functions
      $('.hover-number').unbind('hover').hover((e) => {
        $('.hover-number .percent').css({ display: 'none' })
        $('.hover-number .number').css({ display: 'inline' })
      }, (e) => {
        $('.hover-number .percent').css({ display: 'inline' })
        $('.hover-number .number').css({ display: 'none' })
      })
    })
  }
}

export default hoverNumber
