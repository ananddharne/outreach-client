export default function (html) {
  return html ? $(`<span>${html}</span>`).text() : ''
}
