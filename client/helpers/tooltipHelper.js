export default {
  init (element) {
    $(element).tooltipster({
      theme: 'tooltipster-noir',
      maxWidth: 300,
      contentAsHTML: true,
      interactive: true,
      trigger: 'custom',
      triggerOpen: {
        mouseenter: true,
        touchstart: true
      },
      triggerClose: {
        scroll: true,
        mouseleave: true,
        tap: true
      }
    })
  },

  setContent (element, content) {
    $(element).tooltipster('content', content)
  },

  enable (element) {
    $(element).tooltipster('enable')
  },

  disable (element) {
    $(element).tooltipster('disable')
  }
}
