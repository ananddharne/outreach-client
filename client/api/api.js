import router from '../router/router'
import { authStore } from 'stores'

let basePath
let baseVersion
let fullBasePath
let isDebug = false
let showLoaderCallback = function () {}

let init = function () {
  // API Token initialization happens on login to set a token for authStore
  basePath = `/api`
  baseVersion = `v1`

  fullBasePath = `${basePath}/${baseVersion}`

  if (window.happie_page_data && window.happie_page_data.api_base) {
    fullBasePath = window.happie_page_data.api_base
  }
}

init()

let api = {

  activeRequests: {},

  setDebug (debugValue) {
    isDebug = debugValue

    // If setting debug to true, expose api object to global scope
    if (debugValue) {
      window.api = api
    }
  },

  /*
  Main entry point for ajax requests in the app
  Url and Options should match normal jQuery ajax options
  Handlers object has these optional params
  {
    before: // function to run on `options.data` to prepare to send to server
    after: // function to run on response data to parse for front end
    disableOverlay: // If set to true, does NOT show the loading overlay on top of all screen content
    debugData: // data object to return in lieu of making actual API request
    debugTimeout: // amount of ms to wait for debug data to return
  }
  */
  ajax (url, options, handlers) {
    options = options || {}
    handlers = handlers || {}

    let urlToSend = `${fullBasePath}/${url}`
    // Add trailing slash if not present
    urlToSend = urlToSend.replace(/\/?$/, '/')

    // Set some defaults if not included

    // Format `type` if data comes along
    if (options.data) {
      if (options.type === undefined) {
        options.type = 'POST'
      }

      // Check for undefined because if contentType is `false` we're sending image form data
      if (options.type !== 'GET' && options.contentType === undefined) {
        options.contentType = 'application/json'
        // Stringify data
        options.data = JSON.stringify(options.data)
      }
    }

    // Default to GET if nothing sent in
    if (!options.type) {
      options.type = 'GET'
    }

    // Add token header if we're logged in
    let token = authStore.getToken()
    if (token && url !== 'users/login') {
      options.headers = {
        'Authorization': `Token ${token}`
      }
    }

    if (handlers.before) {
      handlers.before(options.data)
    }

    if (!options.disableOverlay) {
      this.beginLoading(url)
    }

    console.log('calling url ', url)

    let promise
    if (isDebug && handlers.debugData) {
      promise = $.Deferred()
      let timeoutDelay = handlers.debugTimeout || 0
      setTimeout(() => {
        promise.resolve(handlers.debugData)
      }, timeoutDelay)
    } else {
      promise = $.ajax(urlToSend, options)
    }

    // Error handler
    promise.fail(this.onError)

    // If an "after" handler is specified, call back to it after request made
    if (handlers.after) {
      promise.done((response) => {
        handlers.after(response)
      })
    }

    if (!options.disableOverlay) {
      promise.always(() => {
        this.endLoading(url)
      })
    }

    return promise
  },

  // activeRequests objects keeps a count of the currently active API requests, mapped to their URL
  // current implementation assumes same URL route will never have more than 1 concurrent request
  beginLoading (url) {
    this.activeRequests[url] = true

    showLoaderCallback(true)
  },

  getActiveRequests () {
    return this.activeRequests
  },

  endLoading (url) {
    delete this.activeRequests[url]

    // If we don't have any more active requests, hide loader
    if (Object.keys(this.activeRequests).length < 1) {
      showLoaderCallback(false)
    }
  },

  registerShowLoaderCallback (callback) {
    showLoaderCallback = callback
  },

  onError (error) {
    console.log('error from api call is: ', error)
    // Redirect to login if not authorized

    let route = router.match(location)

    if (error.status === 401) {
      // Use hash as current path, but remove the actual `#` when saving it
      authStore.setRedirectPath(route.hash.replace('#', ''))
      authStore.logout()
      router.push({ path: '/login' })
    }
  }

}

export default api
