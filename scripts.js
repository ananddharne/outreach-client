#!/usr/bin/env node

const fs = require('fs')
const { exec } = require('child_process')
require('colors')

// If there is no _settings file, create one with atom as the default editor because we have a ton of atom fanbois
let _ = {}
try {
  _ = require('./_settings')
} catch (e) {
  write('./_settings.json', `{
  "edit_command": "atom"
}
`)
  _.edit_command = 'atom'
}

const dir = `./client/components`
const script = process.argv[2]

switch (script) {
  case 'create':
    create(process.argv[3], process.argv[4])
    break
  case '-h':
    help()
    break
  case '--help':
    help()
    break
  case 'help':
    help()
    break
  default:
    console.log('\nInvalid or non-existant script argument. Try ./scripts.js -h\n'.red)
    break
}

function help () {
  console.log()
  console.log('Commands:')
  console.log()
  console.log('\tcreate' + ' <name>'.blue + ' [modal]'.yellow + ' - Creates a .vue component'.gray)
  console.log()
}

function create (name, type) {
  if (!name) {
    console.log('Please include a name for your new component.'.red)
    return
  }

  let file = `${dir}/${name}.vue`
  switch (type) {
    case 'modal':
      file = `${dir}/Modals/${name}.vue`
      write(file, getVueTemplate())
      append(`${dir}/Modals/modalList.js`, `export ${name} from 'components/Modals/${name}'\n`, name)
      exec(`${_.edit_command} ${file}`)
      break
    default:
      write(file, getVueTemplate())
      exec(`${_.edit_command} ${file}`)
      break
  }
}

function append (file, data, name) {
  fs.appendFile(file, data, function (err) {
    if (err) throw err
    console.log(`Appended ${name} to ${file}`.green)
  })
}

function write (file, data) {
  fs.writeFile(file, data, function (err) {
    if (err) throw err
    console.log(`Created ${file}`.green)
  })
}

function getVueTemplate () {
  return `<template>
</template>

<script>
export default {
  components: {
  },
  data () {
    return {
    }
  },
  computed: {
  },
  methods: {
  }
}
</script>

<style lang='scss' scoped>
  @import "~styles/variables";
</style>
`
}
