'use strict'
const pkg = require('../package')

module.exports = {
  port: 4000,
  title: 'Happie Outreach',
  vendor: Object.keys(pkg.dependencies),
  babel: {
    babelrc: false,
    presets: [
      ['es2015', { modules: false }],
      'stage-1'
    ],
    plugins: [
      'transform-vue-jsx'
    ]
  },
  postcss: [
    require('autoprefixer')({
      // Vue does not support ie 8 and below
      browsers: ['last 2 versions', 'ie > 8']
    }),
    require('postcss-nested')
  ],
  vue: {
    preserveWhitespace: false,
    loaders: {
      js: 'babel-loader',
      scss: 'vue-style-loader!css-loader!sass-loader', // <style lang="scss">
      sass: 'vue-style-loader!css-loader!sass-loader' // <style lang="sass">
    }
  },
  cssModules: false,
}
