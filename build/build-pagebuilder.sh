chmod 600 ./credentials/pagebuilder_deploy
echo 'Host pagebuilder.github.com\n  HostName github.com\n  User git\n  IdentityFile ~/outreach-client/credentials/pagebuilder_deploy\n  IdentitiesOnly yes' >> ~/.ssh/config
rm -rf ~/.node-gyp
if [ ! -d pagebuilder/.git ] || [ ! -d client/pagebuilder ]; then
  echo "Cached files not found, cloning pagebuilder"
  rm -rf ./pagebuilder
  git clone git@pagebuilder.github.com:GetHappie/pagebuilder.git
  cd pagebuilder
  echo "Installing dependencies for pagebuilder"
  yarn install
  npm rebuild node-sass
  echo "Building pagebuilder release"
  npm run build
  echo "Copying pagebuilder build to outreach source folder"
  cd ..
  cp -r ./pagebuilder/build/* ./client/pagebuilder/
else
  echo "Checking if pagebuilder is up to date"
  cd pagebuilder
  git fetch origin
  reslog=$(git log HEAD..origin/master --oneline)
  if [ "$reslog" = "" ]; then
    echo "Pagebuilder is up to date"
  else
    echo "Creating new pagebuilder build"
    cd ..
    mv pagebuilder pagebuilder_cached
    echo "Cloning pagebuilder"
    rm -rf ./pagebuilder
    git clone git@pagebuilder.github.com:GetHappie/pagebuilder.git
    mv pagebuilder_cached/node_modules pagebuilder
    cd pagebuilder
    echo "Installing dependencies for pagebuilder"
    yarn install
    npm rebuild node-sass
    echo "Building pagebuilder release"
    npm run build
    echo "Copying pagebuilder build to outreach source folder"
    cd ..
    cp -r ./pagebuilder/build/* ./client/pagebuilder/
  fi
fi
