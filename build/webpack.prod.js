'use strict'
const exec = require('child_process').execSync
const webpack = require('webpack')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const ProgressBarPlugin = require('progress-bar-webpack-plugin')
const base = require('./webpack.base')
const _ = require('./utils')
const config = require('./config')

const date = new Date()
const banner = `Copyright © Happie ${date.getFullYear()}. Built at ${date.getMonth() + 1}/${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`

if (config.electron) {
  // remove dist folder in electron mode
  exec('rm -rf app/assets/')
} else {
  // remove dist folder in web app mode
  exec('rm -rf dist/')
  // use source-map in web app mode
  base.devtool = 'source-map'
}

// a white list to add dependencies to vendor chunk
base.entry.vendor = config.vendor
// use hash filename to support long-term caching
base.output.filename = 'js/[name].[chunkhash:8].js'
// add webpack plugins
base.plugins.push(
  new ProgressBarPlugin(),
  new ExtractTextPlugin('static/styles.[contenthash:8].css'),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: '"production"'
    }
  }),
  new webpack.LoaderOptionsPlugin({
    minimize: true
  }),
  new webpack.optimize.UglifyJsPlugin({
    sourceMap: true,
    compress: {
      // Disably uglifyJS compilation warnings
      warnings: false,
      // Remove console.log|info from compiled JS. Keep warns and errors because I think Sentry might use them.
      // If I'm wrong, replace the line with 'drop_console: true'.
      pure_funcs: ['console.log', 'console.info']
    },
    output: {
      comments: false
    }
  }),
  // extract vendor chunks
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    filename: 'js/vendor.[chunkhash:8].js'
  }),
  new webpack.BannerPlugin(banner)
)

// extract css in standalone .css files
// TODO: do we need this fallback loader? - fallbackLoader: 'style-loader'
base.module.loaders.push({
  test: /\.css$/,
  loader: ExtractTextPlugin.extract({
    loader: [_.cssLoader, 'postcss-loader'],
  })
})

// extract css in single-file components
// TODO; do we need this fallback loader? - fallbackLoader: 'vue-style-loader'
config.vue.loaders.css = ExtractTextPlugin.extract({
  loader: 'css-loader?-autoprefixer'
})

// Put post css into vue
config.vue.postcss = config.postcss

// push the plugin config into the base
base.plugins.push(new webpack.LoaderOptionsPlugin({
  debug  : true,
  options: {
    context  : __dirname,
    babel    : config.babel,
    postcss  : config.postcss,
    vue      : config.vue
  }
}))

// Also extract sass files
// base.vue.loaders.sass = ExtractTextPlugin.extract("css!sass")

module.exports = base
