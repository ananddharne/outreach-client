
// This is the webpack config used for unit tests.
var baseConfig = require('./webpack.dev')

// no need for app entry during tests
delete baseConfig.entry

module.exports = baseConfig
