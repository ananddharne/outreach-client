#!/bin/bash
# Script for building pagebuilder locally and moving the build contents into happie-outreach and outreach-client
cd ./pagebuilder
npm run build
cd ..
echo "Removing old build"
rm -rf ./client/pagebuilder/*
echo "Copying new build into outreach-client"
cp -a ./pagebuilder/build/. ./client/pagebuilder/
if [ -d ../happie-outreach/staticfiles/outreach/pagebuilder_static ]; then
	echo "Copying new build into happie-outreach"
	rm -rf ../happie-outreach/staticfiles/outreach/pagebuilder_static/*
	cp -a ./pagebuilder/build/. ../happie-outreach/staticfiles/outreach/pagebuilder_static/
else
  echo "Couldn't find directory ../happie-outreach/staticfiles/outreach/pagebuilder_static"
  echo "Make sure your happie-outreach folder is adjacent to your outreach-client folder"
fi
