#!/usr/bin/env python3
# coding=utf-8
"""
Upload to gcloud
"""
import os
import subprocess

import sys


def run_command(cmd: str):
    """
    Run the specified command (using the shell for globbing/path search.)

    Args:
        cmd (str): Command to execute

    Returns:
        Tuple[int, str]: exit status, command stdout/stderr
    """
    print("--> %s" % cmd)
    try:
        s = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
        return 0, s.decode('utf-8')
    except subprocess.CalledProcessError as ce:
        return ce.returncode, str(ce)
    except Exception as e:
        return -1, str(e)


def upload(src_dir, dest_address):
    """
    Upload directory to target address (should be gs://bucket/directory
    Args:
        src_dir ():
        dest_address ():

    Returns:

    """
    src_dir = os.path.abspath(src_dir)
    for subdir, dirs, files in os.walk(src_dir):
        source = os.path.abspath(subdir)
        dest = dest_address + subdir.replace(src_dir, '')
        print("gsutil cp -Z %s/* %s/" % (source, dest))
        sts, output = run_command("gsutil cp -Z %s/* %s/" % (source, dest))
        print(output)


if len(sys.argv) < 3:
    print("Usage %s <src_dir> gs://bucket-name/directory" % sys.argv[0])
else:
    upload(sys.argv[1], sys.argv[2])
