'use strict'
const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const config = require('./config')
const _ = require('./utils')

module.exports = {
  entry: {
    client: './client/index.js'
  },
  output: {
    path: _.outputPath,
    filename: 'js/[name].js',
    publicPath: ''
  },
  externals: {
    // require("jquery") is external and available
    //  on the global var jQuery
    'jquery': 'jQuery'
  },
  resolve: {
    extensions: ['.js', '.vue', '.css', '.json', '.scss'],
    alias: {
      root: path.join(__dirname, '../client'),
      components: path.join(__dirname, '../client/components'),
      styles: path.resolve(__dirname, '../client/styles'),
      lib: path.resolve(__dirname, '../client/lib'),
      helpers: path.resolve(__dirname, '../client/helpers/helpers'),
      img: path.resolve(__dirname, '../client/img'),
      media: path.resolve(__dirname, '../client/media'),
      stores: path.resolve(__dirname, '../client/stores/stores'),
      api: path.resolve(__dirname, '../client/api/api'),
      vue: path.resolve(__dirname, '../node_modules/vue/dist/vue.js')
    }
  },
  module: {
    loaders: [
      {
        test: /\.vue$/,
        loaders: ['vue-loader']
      },
      {
        test: /\.(js|vue)$/,
        loader: 'eslint-loader',
        enforce: 'pre',
        include: [path.resolve(__dirname, '../client')],
        options: {
          formatter: require('eslint-friendly-formatter')
        }
      },
      {
        test: /\.js$/,
        loaders: ['babel-loader'],
        exclude: [/node_modules/, /client\/pagebuilder/]
      },
      {
        test: /\.scss$/,
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      },
      {
        test: /\.es6$/,
        loaders: ['babel-loader']
      },
      {
        test: /\.(jpg|jpeg|png|gif|eot|otf|webp|ttf|woff|woff2|mp3)(\?.*)?$/,
        loader: 'file-loader',
        query: {
          name: 'media/[name].[hash:8].[ext]'
        }
      },
      {
        test: /\.svg$/,
        loader: 'svg-url-loader'
      },
      {
        test: /\.ico$/,
        loader: 'file-loader',
        query: {
          name: './[name].[ext]'
        }
      },
      {
        test: /\.html$/,
        loaders: ['html-loader'],
        include: [/pagebuilder/]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: config.title,
      template: path.resolve(__dirname, './index.html'),
      filename: _.outputIndexPath,
      favicon: path.resolve(__dirname, './favicon.ico')
    }),
    new HtmlWebpackPlugin({
      template: path.resolve(__dirname, './404-page.html'),
      filename: _.output404Path
    }),
    new webpack.ProvidePlugin({
      $: 'jquery',
      jQuery: 'jquery'
    }),
    new CopyWebpackPlugin([{
      from: path.join(__dirname, '../client/pagebuilder'),
      to: 'pagebuilder_static'
    }])
  ],
  target: _.target
}
