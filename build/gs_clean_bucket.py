#!/usr/bin/env python2
# coding=utf-8
"""
Upload to gcloud
"""
import datetime
import subprocess
import sys
from optparse import OptionParser


def run_command(cmd):
    """
    Run the specified command (using the shell for globbing/path search.)

    Args:
        cmd (str): Command to execute

    Returns:
        Tuple[int, str]: exit status, command stdout/stderr
    """
    print("--> %s" % cmd)
    try:
        s = subprocess.check_output(cmd, stderr=subprocess.STDOUT, shell=True)
        return 0, s.decode('utf-8')
    except subprocess.CalledProcessError as ce:
        return ce.returncode, str(ce)
    except Exception as e:
        return -1, str(e)


def clean(bucket_name, max_age_in_days):
    """
    Cleans the target bucket
    Args:
        bucket_name ():
        max_age_in_days ():

    Returns:

    """
    if not bucket_name.startswith("gs://"):
        bucket_name = "gs://%s" % bucket_name
    sts, s = run_command("gsutil ls -lr %s" % bucket_name)
    now = datetime.datetime.utcnow()
    to_delete = set()
    for line in s.split('\n'):
        try:
            size, s_date, path = line.split()
            date = datetime.datetime.strptime(s_date, "%Y-%m-%dT%H:%M:%SZ")
            if (now - date).days > max_age_in_days:
                to_delete.add(path)
        except:
            pass

    if to_delete:
        cmd = "gsutil -m rm %s" % ' '.join(to_delete)
        run_command(cmd)
    else:
        print("Bucket is clean")


parser = OptionParser()
parser.add_option("-d", "--days", dest="days", action="store", type="int",
                  help="Maximum age of file (in days)")

parser.add_option("-b", "--bucket", dest="bucket", action="store", type="string",
                  help="bucket name")

(options, args) = parser.parse_args()
if not options.bucket or not options.days:
    print("Usage %s -d <max age in days> -b <bucket name>" % sys.argv[0])
    sys.exit(-1)
clean(options.bucket, options.days)
