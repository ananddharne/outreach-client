#!/bin/bash
# Script for building outreach locally and moving the build contents into happie-outreach
npm run build-outreach
if [ -d ../happie-outreach/staticfiles/outreach ]; then
	echo "Removing old build"
	rm -rf ../happie-outreach/staticfiles/outreach/*
	echo "Copying new build into happie-outreach"
	cp -a ./dist/. ../happie-outreach/staticfiles/outreach/
else
	echo "Couldn't find directory ../happie-outreach/staticfiles/outreach"
	echo "Make sure your happie-outreach folder is adjacent to your outreach-client folder"
fi