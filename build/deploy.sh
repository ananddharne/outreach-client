echo "Uploading to GCloud"

if [ -z "$1" ]
  then
    echo "Uploading to Default Path: gs://happie-frontend-staging.pagebuilder.me"
    gsutil -m rsync -r ./dist gs://happie-frontend-staging.pagebuilder.me
  else
    echo "Uploading to path:"
    echo $1
    gsutil -m rsync -r ./dist $1
fi
