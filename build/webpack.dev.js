'use strict'
const webpack = require('webpack')
const base = require('./webpack.base')
const _ = require('./utils')
const config = require('./config')

base.devtool = 'eval-source-map'
base.plugins.push(
  new webpack.DefinePlugin({
    'process.env.NODE_ENV': JSON.stringify('development')
  }),
  new webpack.HotModuleReplacementPlugin(),
  new webpack.NoEmitOnErrorsPlugin()
)

// push loader for .css file
base.module.loaders.push(
  {
    test: /\.css$/,
    loader: `style-loader!${_.cssLoader}!postcss-loader`
  }
)

// Put post css into vue
config.vue.postcss = config.postcss

if (process.env.NODE_ENV === 'testing') {
  config.babel.plugins.push('istanbul')
}

// Push the plugin config into the base
base.plugins.push(new webpack.LoaderOptionsPlugin({
  debug  : true,
  options: {
    context  : __dirname,
    babel    : config.babel,
    postcss  : config.postcss,
    vue      : config.vue
  }
}))

module.exports = base
