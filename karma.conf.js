// Karma configuration
// Generated on Tue Jun 13 2017 13:55:21 GMT-0400 (EDT)

const base = require('./build/webpack.dev')
module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      'https://code.jquery.com/jquery-3.1.1.min.js',
      '__tests__/test_index.js'
    ],
    // list of files to exclude
    exclude: [

    ],
    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      '__tests__/test_index.js': ['webpack', 'sourcemap']
    },
    webpack: base,

    webpackMiddleware: {
      // webpack-dev-middleware configuration
      // i. e.
      stats: 'errors-only'
    },
    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['progress', 'coverage'],
    coverageReporter: {
      type: 'text',
      dir: 'coverage/'
    },
    // web server port
    port: 9876,

    // Proxy to staging
    proxies: {
      '/api': {
        'target': 'https://talent-staging.gethappie.me/api',
        'changeOrigin': true
      }
    },

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_DEBUG,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['ChromeWithoutWebSecurity'],

    // Custom flags because I don't fucking know why
    customLaunchers: {
      ChromeWithoutWebSecurity: {
        base: 'ChromeHeadless',
        flags: ['--disable-web-security']
      }
    },

    plugins: [
      'karma-chrome-launcher',
      'karma-webpack',
      'karma-jasmine',
      'karma-coverage',
      'babel-plugin-istanbul',
      'karma-sourcemap-loader'
    ],
    // End I don't fucking know why

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false,

    // Concurrency level
    // how many browser should be started simultaneous
    concurrency: Infinity
  })
}
