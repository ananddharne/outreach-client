# Outreach Client (Front-End)

Built with [vue](https://vuejs.org/) bootstrapped with [vuepack](https://github.com/egoist/vuepack).

## Environment:

- These instructions assume a Linux/MacOS environment. If you're on windows, you'll need to run a Linux environment following the instructions [here](https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/)
- You will also need [node](https://nodejs.org/en/download/package-manager/) and [yarn](https://yarnpkg.com/en/docs/install)

## To Install:

* Clone the repo, cd into the project and run:

```
$ npm run build
```

* You'll also need to pull down the [happie-outreach](https://github.com/GetHappie/happie-outreach) project and follow the instructions in that Readme to get it up and running. Make sure when you clone this project, the `happie-outreach` folder is **adjacent** to to your `outreach-client` folder.

* In order to develop with pagebuilder, you will need to clone [pagebuilder](https://github.com/GetHappie/pagebuilder) **into** your `outreach-client` folder and then run `npm run build-local-pagebuilder`. You can also start the development server for pagebuilder with `npm run pagebuilder` and that will run on localhost:3000.

## To Develop:

To run the front end client on localhost:4000
```
$ npm start
```

For [Auth0](http://auth0.com) to work, you'll need to add the environment variable `AUTH0_TOKEN=xyz`.  You can get the actual token value from Greg or Adam.

By default the `happie-outreach` project will run on localhost:8000, and the `outreach-client` will proxy any `/api` request to that server. If this changes, the proxy setting can be changed in `server.js`.  

## Build Scripts:

Some of the script below assume you have already cloned [pagebuilder](https://github.com/GetHappie/pagebuilder) into your `outreach-client` folder and you have [happie-outreach](https://github.com/GetHappie/happie-outreach) adjacent to your `outreach-client` folder.

To install modules and build outreach-client:

```
$ npm run build
```

To just build outreach-client:

```
$ npm run build-outreach
```

To build a local version of outreach-client that will copy build files into your `happie-outreach` folder:

```
$ npm run build-local-outreach
```

To build a local version of pagebuilder that will copy build files into your `happie-outreach` and `outreach-client` folders:

```
$ npm run build-local-pagebuilder
```

## Testing

To run unit tests you will need to start the testing server:

```
$ npm run testing
```

then in a new terminal window, run:

```
$ npm run unit
```

To run e2e tests, make sure you follow these instructions for your machine [nightwatch](http://nightwatchjs.org/gettingstarted#installation) and then run:

```
$ npm run e2e
```

To run both:

```
$ npm test
```

## Linting

```
$ npm run lint
```

To autofix common linter errors:

```
$ npm run lint-fix
```

## Deployments

After committing to master, a build should be triggered on CircleCI to push assets to http://talent-staging.gethappie.me. 

To push to production (http://talent.gethappie.me), merge master into the `production` branch, which should kick off a CircleCI build.

## Documentation

Other documentation for the front-end app can be found here: [docs](https://github.com/GetHappie/docs)

