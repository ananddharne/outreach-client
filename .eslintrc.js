module.exports = {
  'root': true,
  'parser': 'babel-eslint',
  'extends': 'standard',
  // required to lint *.vue files
  'plugins': [
    'html'
  ],
  'settings': {
    // Consider .vue files as html
    'html/html-extensions': ['.html', '.vue'],
    // Code should start at the beginning of the line after a script tag
    'html/indent': '0'
  },
  'env': {
    'browser': true,
    'jquery': true,
    'es6': true
  },
  // Custom rules
  'rules': {
    // allow paren-less arrow functions
    'arrow-parens': 0,
    // allow async-await
    'generator-star-spacing': 0,
    // allow == or ===
    'eqeqeq': 0,
    // Force spaces in one-liner objects. (Mostly for component props so I don't lose my mind)
    'object-curly-spacing': ['error', 'always'],
    // allow snake_case in dot notation (used for API calls)
    'dot-notation': ["error", { "allowPattern": "^[a-z]+(_[a-z]+)+$" }],
    // ignore some library variables so eslint doesn't freak out
    'no-unused-vars': ['error', { "vars": "all", "args": "none", "ignoreRestSiblings": true, 'varsIgnorePattern': '(tooltipster|select2|momentTimezone)'}],
    // Because Adam insists on using sublime text
    // Even though it happened on Anand's comp (and he's using atom, gg)
    "no-multiple-empty-lines": ["error", { "max": 1, "maxEOF": 0, "maxBOF": 2 }]
  }
}
