exports.command = function (targetUrl, action, callback) {
  this.timeoutsAsyncScript(3500)
  action()
  this.executeAsync(function (targetUrl, done) {
    $(document).ajaxComplete(function (e, res, req) {
      if (req.url.indexOf(targetUrl) === 0) {
        done(true)
      }
    })
  }, [targetUrl], function (status) {
    callback()
  })
}
