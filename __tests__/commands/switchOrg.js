exports.command = function (org) {
  this.pause(250)
  .waitForElementVisible('.organization-dropdown', 1000)
  .click('.organization-dropdown')
  .pause(250)
  .executeAsync(function (data, done) {
    $(`.organization-dropdown li:contains(${data})`).click()
    this.waitForAjax('/api/v1/users/verify-login/', function () {
    }, function () {
    })
  }, [org])
  .pause(1000)
}
