// require all modules ending in "_test" from the
// current directory and all subdirectories
import Vue from 'vue'
Vue.config.productionTip = false

var testsContext = require.context('./', true, /_test$/)
testsContext.keys().forEach(testsContext)

var componentsContext = require.context('../client', true, /\.(js|vue)$/)
componentsContext.keys().filter((file) => {
  // console.log(file)
  return file.indexOf('../lib/') === 0
}).forEach(componentsContext)
