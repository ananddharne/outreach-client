import { appInitializer } from 'helpers'
describe('appInitializer', () => {
  it('asserting api call', () => {
    expect(typeof appInitializer.init).toBe('function')
    expect(typeof appInitializer.clearStores).toBe('function')
    expect(typeof appInitializer.clearAllStores).toBe('function')
  })
})
