import { tooltipHelper } from 'helpers'

describe('tooltipHelper', () => {
  it('asserting  functions', () => {
    expect(typeof tooltipHelper.init).toBe('function')
    expect(typeof tooltipHelper.setContent).toBe('function')
    expect(typeof tooltipHelper.enable).toBe('function')
    expect(typeof tooltipHelper.disable).toBe('function')
  })
})
