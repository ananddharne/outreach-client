import { sourcedReqHelper } from 'helpers'

describe('sourcedReqHelper', () => {
  it('pulls comments from the API and displays them in a modal', () => {
    expect(typeof sourcedReqHelper.openComments).toBe('function')
    expect(typeof sourcedReqHelper.createNew).toBe('function')
    expect(typeof sourcedReqHelper.update).toBe('function')
    expect(typeof sourcedReqHelper.refresh).toBe('function')
    expect(typeof sourcedReqHelper.refreshAll).toBe('function')
    expect(typeof sourcedReqHelper.bulkStatusChange).toBe('function')
    expect(typeof sourcedReqHelper.requiredCheck).toBe('function')
    expect(typeof sourcedReqHelper.exportSourcedReqs).toBe('function')
  })
})
