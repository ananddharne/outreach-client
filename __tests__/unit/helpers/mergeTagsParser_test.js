import { mergeTagsParser } from 'helpers'

describe('mergeTagsParser', () => {
  it('asserting  functions', () => {
    expect(typeof mergeTagsParser.parseHTML).toBe('function')
    expect(typeof mergeTagsParser.parseSpans).toBe('function')
    expect(typeof mergeTagsParser.marginZero).toBe('function')
    expect(typeof mergeTagsParser.toTags).toBe('function')
    expect(typeof mergeTagsParser.fromTags).toBe('function')
  })
})
