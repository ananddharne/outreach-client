import { jobHelper } from 'helpers'

describe('jobHelper', () => {
  it('asserting functions and path storage', () => {
    expect(typeof jobHelper.currentPath).toBe('string')
    expect(typeof jobHelper.editJob).toBe('function')
    expect(typeof jobHelper.openJobModal).toBe('function')
    expect(typeof jobHelper.shareJob).toBe('function')
    expect(typeof jobHelper.saveJob).toBe('function')
    expect(typeof jobHelper.beforeModalClose).toBe('function')
  })
})
