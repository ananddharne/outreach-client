import { candidateHelper } from 'helpers'

describe('candidateHelper', () => {
  it('asserting  functions', () => {
    expect(typeof candidateHelper.getCityState).toBe('function')
    expect(typeof candidateHelper.downloadResume).toBe('function')
    expect(typeof candidateHelper.openCandidateModal).toBe('function')
    expect(typeof candidateHelper.minimalMap).toBe('function')
  })
})
