import { modals } from 'helpers'

describe('modals', () => {
  it('asserting  functions', () => {
    expect(typeof modals.bus.show).toBe('function')
    expect(typeof modals.bus.hide).toBe('function')
    expect(typeof modals.registerBus).toBe('function')
  })
})
