import { candidateActions } from 'helpers'

describe('candidateActions', () => {
  it('asserting  functions', () => {
    expect(typeof candidateActions.sendMessage).toBe('function')
    expect(typeof candidateActions.addToList).toBe('function')
    expect(typeof candidateActions.addToSequence).toBe('function')
    expect(typeof candidateActions.pauseMessages).toBe('function')
    expect(typeof candidateActions.resumeMessages).toBe('function')
    expect(typeof candidateActions.clearMessages).toBe('function')
    expect(typeof candidateActions.optOut).toBe('function')
    expect(typeof candidateActions.deleteCandidate).toBe('function')
    expect(typeof candidateActions.bulkAddToSequence).toBe('function')
    expect(typeof candidateActions.bulkPauseMessages).toBe('function')
    expect(typeof candidateActions.bulkResumeMessages).toBe('function')
    expect(typeof candidateActions.bulkOptOut).toBe('function')
    expect(typeof candidateActions.bulkClearMessages).toBe('function')
    expect(typeof candidateActions.export).toBe('function')
    expect(typeof candidateActions.bulkAddToList).toBe('function')
    expect(typeof candidateActions.deleteCandidate).toBe('function')
    expect(typeof candidateActions.findPersonalInfo).toBe('function')
  })
})
