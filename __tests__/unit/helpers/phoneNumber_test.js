import { phoneNumber } from 'helpers'

describe('phoneNumber', () => {
  it('localizes libphonenumber objects and operates on given phone numbers', () => {
    expect(typeof phoneNumber.phoneUtil).toBe('object')
    expect(typeof phoneNumber.PNF).toBe('object')
    expect(typeof phoneNumber.makeReadable).toBe('function')
  })
})
