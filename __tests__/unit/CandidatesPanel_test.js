
// import Vue from 'vue'
// import CandidatesPanel from 'components/Sections/Candidates/CandidatesPanel'
// import VueRouter from 'vue-router'
//
// describe('CandidatesPanel', () => {
//   // Check data and methods on the un-mounted component
//   it('asserts mounted components and its methods', () => {
//     Vue.use(VueRouter)
//     const router = new VueRouter({
//
//       routes: [
//         { path: '/candidatespanel', name: 'logintest', component: { render: h => h('div') } },
//         { path: '/candidatep', name: 'candidatec', component: CandidatesPanel }
//       ]
//     })
//
//     const vms = new Vue({
//       el: document.createElement('div'),
//       router: router,
//       render: h => h('router-view')
//     })
//
//     router.push({ name: 'candidatec' })
//
//     Vue.nextTick(() => {
//       expect(typeof vms.methods.init).toBe('function')
//       expect(typeof vms.pusherStore.channel.unbind).toBe('function')
//       expect(typeof vms.pusherStore.channel.bind).toBe('function')
//     })
//   })
// })
