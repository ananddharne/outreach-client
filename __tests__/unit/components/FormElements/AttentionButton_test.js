import Vue from 'vue'
import AttentionButton from 'components/FormElements/AttentionButton'
import VueRouter from 'vue-router'

describe('AttentionButton', () => {
  // Check data and methods on the un-mounted component
  it('asserts mounted components and its methods', () => {
    Vue.use(VueRouter)
    const router = new VueRouter({

      routes: [
        { path: '/attentionbutton', name: 'logintest', component: { render: h => h('div') } },
        { path: '/attentionb', name: 'attentionbutton', component: AttentionButton }
      ]
    })
    const vms = new Vue({
      el: document.createElement('div'),
      router: router,
      render: h => h('router-view')
    })
    router.push({ name: 'attentionbutton' })
    Vue.nextTick(() => {
      expect(typeof vms.methods.clickFunction).toBe('function')
    })
  })
})
