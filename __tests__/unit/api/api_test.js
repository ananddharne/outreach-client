import api from 'api'

describe('api', () => {
  it('assertions', () => {
    expect(typeof api.setDebug).toBe('function')
    expect(typeof api.ajax).toBe('function')
    expect(typeof api.beginLoading).toBe('function')
    expect(typeof api.getActiveRequests).toBe('function')
    expect(typeof api.endLoading).toBe('function')
    expect(typeof api.registerShowLoaderCallback).toBe('function')
    expect(typeof api.onError).toBe('function')
  })
})
