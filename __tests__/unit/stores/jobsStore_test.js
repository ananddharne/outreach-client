import { jobsStore } from 'stores'

describe('jobsStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof jobsStore.get).toBe('function')
    expect(typeof jobsStore.getJob).toBe('function')
    expect(typeof jobsStore.init).toBe('function')
    expect(typeof jobsStore.addJobs).toBe('function')
    expect(typeof jobsStore.updateJobs).toBe('function')
    expect(typeof jobsStore.deleteJobs).toBe('function')
    expect(typeof jobsStore.setUnsavedJob).toBe('function')
    expect(typeof jobsStore.getUnsavedJob).toBe('function')
    expect(typeof jobsStore.resetUnsavedJob).toBe('function')
    expect(typeof jobsStore.clear).toBe('function')
    expect(jobsStore.store.jobs).toEqual([])
    expect(jobsStore.store.unsavedJob).toBeNull()
  })
  it('init function', () => {
    jobsStore.init(['anand', 'swjob', 'python developer'])
    const storeData = jobsStore.get()
    expect(storeData.jobs).toEqual(['anand', 'swjob', 'python developer'])
  })

  it('getJob function', () => {
    let job = {
      'id': 0,
      'hiring_manager': null,
      'active': true,
      'title': '',
      'latitude': 0,
      'longitude': 0,
      'published_url': ''
    }
    jobsStore.init([ job, 'swjob', 'python developer' ])
    const thejob = jobsStore.getJob(0)
    expect(thejob).toEqual({ 'id': 0,
      'hiring_manager': null,
      'active': true,
      'title': '',
      'latitude': 0,
      'longitude': 0,
      'published_url': '' })
  })

  it('addJobs function', () => {
    jobsStore.init([ 'swjob', 'python developer' ])
    let job1 = {
      'id': 1,
      'hiring_manager': null,
      'active': true,
      'title': '',
      'latitude': 0,
      'longitude': 0,
      'published_url': ''
    }

    jobsStore.addJobs([job1])
    job1 = jobsStore.formatJobs([job1])[0]
    expect(jobsStore.store.jobs).toEqual(['swjob', 'python developer', job1])
    jobsStore.clear()
    jobsStore.addJobs([job1])
    expect(jobsStore.store.jobs).toEqual([job1])
  })

  it('updateJobs function', () => {
    let job1 = {
      'id': 1,
      'hiring_manager': null,
      'active': true,
      'title': '',
      'latitude': 0,
      'longitude': 0,
      'published_url': ''
    }
    let job1update = {
      id: 1,
      title: 'uit'
    }
    job1update = jobsStore.formatJobs([job1update])[0]
    jobsStore.init([ job1, 'python developer' ])
    jobsStore.updateJobs([job1update])
    expect(jobsStore.store.jobs).toEqual([job1update, 'python developer'])
  })
  it('updateJobs function', () => {
    let job1 = {
      'id': 1,
      'hiring_manager': null,
      'active': true,
      'title': '',
      'latitude': 0,
      'longitude': 0,
      'published_url': ''
    }
    jobsStore.init([ job1, 'python developer' ])
    jobsStore.deleteJobs([1])
    expect(jobsStore.store.jobs).toEqual(['python developer'])
  })

  it('setUnsavedJob, getUnsavedJob, resetUnsavedJob function', () => {
    let job1 = {
      'id': 1,
      'hiring_manager': null,
      'active': true,
      'title': '',
      'latitude': 0,
      'longitude': 0,
      'published_url': ''
    }
    jobsStore.init([ 'python developer' ])
    jobsStore.setUnsavedJob(job1)
    expect(jobsStore.store.unsavedJob).toEqual(job1)
    jobsStore.getUnsavedJob()
    jobsStore.resetUnsavedJob()
    expect(jobsStore.store.unsavedJob).toEqual(null)
  })
})
