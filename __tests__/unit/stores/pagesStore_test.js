import { pagesStore } from 'stores'

describe('pagesStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof pagesStore.get).toBe('function')
    expect(typeof pagesStore.init).toBe('function')
    expect(typeof pagesStore.addPages).toBe('function')
    expect(pagesStore.store.pages).toEqual([])
  })

  it('init function', () => {
    pagesStore.init(['anand', 'swpage', 'python developer'])
    const storeData = pagesStore.get()
    expect(storeData.pages).toEqual(['anand', 'swpage', 'python developer'])
  })

  it('addPages function', () => {
    pagesStore.init(['anand', 'swpage', 'python developer'])
    let pages1 = {
      'id': 1,
      'title': 'This is a pages test'
    }
    pagesStore.addPages([pages1])
    const storeData = pagesStore.get()
    console.log(storeData)
    expect(storeData.pages).toEqual(['anand', 'swpage', 'python developer', { id: 1, title: 'This is a pages test' }])
  })
})
