import { companyStore } from 'stores'

describe('companyStore', () => {
  it('get function and assert inital store data', () => {
    expect(typeof companyStore.get).toBe('function')
    const storeData = companyStore.get()

    expect(storeData.company.name).toEqual('')
    expect(storeData.company.address).toBeNull()
    expect(storeData.company.industry).toEqual({})
    expect(storeData.company.website_url).toEqual('')
    expect(storeData.company.linkedin_url).toEqual('')
    expect(storeData.company.primary_contact_first_name).toEqual('')
    expect(storeData.company.primary_contact_last_name).toEqual('')
    expect(storeData.company.primary_contact_last_name).toEqual('')
    expect(storeData.company.primary_contact_phone).toEqual('')
    expect(storeData.company.primary_contact_email).toEqual('')
    expect(storeData.company.logo_url).toEqual('')
  })

  it('init function', () => {
    expect(typeof companyStore.set).toBe('function')
    companyStore.init({ name: 'Happie' })
    const storeData = companyStore.get()
    expect(storeData.company.name).toEqual('Happie')
  })

  it('set and get function', () => {
    expect(typeof companyStore.set).toBe('function')
    companyStore.set('name', 'Happie')
    const storeData = companyStore.get()
    expect(storeData.company.name).toEqual('Happie')
  })
})
