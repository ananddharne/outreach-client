import { campaignsStore } from 'stores'

describe('campaignsStore', () => {
  it('get function and assert inital store data', () => {
    expect(typeof campaignsStore.get).toBe('function')
    expect(campaignsStore.store.campaigns).toEqual([])
    expect(campaignsStore.store.unsavedCampaign).toBeNull([])
    expect(typeof campaignsStore.addCampaigns).toBe('function')
    expect(typeof campaignsStore.updateCampaigns).toBe('function')
    expect(typeof campaignsStore.deleteCampaigns).toBe('function')
    expect(typeof campaignsStore.setUnsavedCampaign).toBe('function')
    expect(typeof campaignsStore.getUnsavedCampaign).toBe('function')
    expect(typeof campaignsStore.resetUnsavedCampaign).toBe('function')
  })

  it('init function', () => {
    campaignsStore.init(['anand', 'swcampaigns', 'python developer'])
    const storeData = campaignsStore.get()
    expect(storeData.campaigns).toEqual(['anand', 'swcampaigns', 'python developer'])
  })

  it('getCampaigns function', () => {
    let campaigns = {
      'id': 1
    }
    campaignsStore.init([ campaigns, 'swcampaigns', 'python developer' ])
    const thecampaigns = campaignsStore.getCampaign(1)
    expect(thecampaigns).toEqual({ 'id': 1 })
  })

  it('addCampaigns function', () => {
    campaignsStore.init([ 'swcampaigns', 'python developer' ])
    let campaigns1 = {
      'id': 1
    }
    campaignsStore.addCampaigns([campaigns1])
    expect(campaignsStore.store.campaigns).toEqual(['swcampaigns', 'python developer', { id: 1 }])
    campaignsStore.clear()
    campaignsStore.addCampaigns([campaigns1])
    expect(campaignsStore.store.campaigns).toEqual([{ id: 1 }])
  })

  it('updateCampaigns function', () => {
    let campaigns1 = {
      'id': 1,
      'title': ''
    }
    campaignsStore.init([ campaigns1, 'python developer' ])
    campaignsStore.updateCampaigns([{ id: 1, title: 'uit' }])
    expect(campaignsStore.store.campaigns).toEqual([{ id: 1, title: 'uit' }, 'python developer'])
  })
  it('deleteCampaigns function', () => {
    let campaigns1 = {
      'id': 1
    }
    campaignsStore.init([ campaigns1, 'python developer' ])
    campaignsStore.deleteCampaigns([1])
    expect(campaignsStore.store.campaigns).toEqual(['python developer'])
  })

  it('setUnsavedCampaign, getUnsavedCampaign, resetUnsavedCampaign function', () => {
    let campaigns1 = {
      'id': 1
    }
    campaignsStore.init([ 'python developer' ])
    campaignsStore.setUnsavedCampaign(campaigns1)
    campaignsStore.getUnsavedCampaign()
    expect(campaignsStore.store.unsavedCampaign).toEqual(campaigns1)
    campaignsStore.resetUnsavedCampaign()
    expect(campaignsStore.store.unsavedCampaign).toEqual(null)
  })
})
