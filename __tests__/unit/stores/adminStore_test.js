import { adminStore } from 'stores'

describe('adminStore', () => {
  it('get function and assert inital store data', () => {
    expect(typeof adminStore.get).toBe('function')
    expect(typeof adminStore.set).toBe('function')
    expect(adminStore.store.users).toEqual([])
    expect(adminStore.store.companies).toEqual([])
    expect(adminStore.store.organizations).toEqual([])
    expect(adminStore.store.is_system_admin).toBeFalsy()
    expect(adminStore.store.is_company_admin).toBeFalsy()
    expect(adminStore.store.is_sourcer_admin).toBeFalsy()
    expect(adminStore.store.is_impersonating).toBeFalsy()
    expect(adminStore.store.has_init).toBeFalsy()
    expect(adminStore.store.admin_token).toBeNull()
  })

  it('set and get functions', () => {
    expect(typeof adminStore.set).toBe('function')
    adminStore.set('is_impersonating', true)
    const storeData = adminStore.get()
    expect(storeData.is_impersonating).toBeTruthy()
  })
})
