import { industriesStore } from 'stores'

describe('industriesStore', () => {
  it('get function and assert inital store data', () => {
    expect(typeof industriesStore.get).toBe('function')
    expect(typeof industriesStore.init).toBe('function')
  })
  it('init function', () => {
    let industry1 = {
      id: 1
    }
    let industry2 = {
      value: 0,
      label: 'Select Industry'
    }
    industriesStore.init([industry1])
    expect(industriesStore.store.industries).toEqual([industry2, industry1])
    industriesStore.get()
  })
})
