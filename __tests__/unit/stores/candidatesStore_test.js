import { candidatesStore } from 'stores'

describe('candidatesStore', () => {
  it('asserting store data for candidate store', () => {
    expect(typeof candidatesStore.get).toBe('function')
    const storeData = candidatesStore.get()
    expect(typeof candidatesStore.init).toBe('function')
    expect(typeof candidatesStore.mapCandidates).toBe('function')
    expect(typeof candidatesStore.addCandidates).toBe('function')
    expect(typeof candidatesStore.addCommentToCandidate).toBe('function')
    expect(typeof candidatesStore.editCommentOnCandidate).toBe('function')
    expect(typeof candidatesStore.deleteCandidates).toBe('function')
    expect(typeof candidatesStore.removeCommentFromCandidate).toBe('function')
    expect(typeof candidatesStore.clearFilters).toBe('function')
    expect(storeData.candidates).toEqual([])
    expect(storeData.totalCount).toEqual(0)
    expect(storeData.page).toEqual(1)
    expect(storeData.filters.req_ids).toEqual('')
    expect(storeData.filters.statuses).toEqual('')
    expect(storeData.filters.date_created_start).toBeNull()
    expect(storeData.filters.date_created_end).toBeNull()
    expect(storeData.filters.list_ids).toEqual('')
    // expect(storeData.filters.response_types).toEqual('')
    expect(storeData.filters.creator_ids).toEqual('')
    expect(storeData.filters.sources).toEqual('')
    expect(storeData.filters.city).toEqual('')
    expect(storeData.filters.state).toEqual('')
    expect(storeData.filters.search).toEqual('')
    expect(storeData.filters.sort_key).toEqual('first_name')
  })
  it('asserting store data for candidate model', () => {
    expect(candidatesStore.candidateModel.first_name).toEqual('')
    expect(candidatesStore.candidateModel.last_name).toEqual('')
    expect(candidatesStore.candidateModel.company_name).toEqual('')
    expect(candidatesStore.candidateModel.linkedin_url).toEqual('')
    expect(candidatesStore.candidateModel.title).toEqual('')
    expect(candidatesStore.candidateModel.industry).toEqual(-1)
    expect(candidatesStore.candidateModel.addresses).toEqual([])
    expect(candidatesStore.candidateModel.emails).toEqual([])
    expect(candidatesStore.candidateModel.lead_source).toEqual('')
    expect(candidatesStore.candidateModel.status).toEqual('')
    expect(candidatesStore.candidateModel.comment_count).toEqual(0)
    expect(candidatesStore.candidateModel.comments).toEqual([])
    expect(candidatesStore.candidateModel.profile_photo_url).toEqual('')
    expect(candidatesStore.candidateModel.resume_uploaded).toBeFalsy()
    expect(candidatesStore.candidateModel.req_ids).toEqual([])
    expect(candidatesStore.candidateModel.list_ids).toEqual([])
  })

  it('init and mappedCandidates function', () => {
    candidatesStore.init([{ first_name: 'anand' }])
    const storeData = candidatesStore.get()
    expect(storeData.candidates[0].first_name).toEqual('anand')
    expect(storeData.candidates[0].last_name).toEqual('')
  })
  it('set function', () => {
    expect(typeof candidatesStore.set).toBe('function')
    candidatesStore.set('first_name', 'Anand')
    var storeData = candidatesStore.get()
    expect(storeData.first_name).toEqual('Anand')
    expect(storeData.last_name).not.toBeDefined()
  })
  it('addCandidates function', () => {
    candidatesStore.init([{ first_name: 'anand' }])
    candidatesStore.addCandidates([{ first_name: 'Adam' }])
    const storeData = candidatesStore.get()
    expect(storeData.candidates[0].first_name).toEqual('anand')
    expect(storeData.candidates[1].first_name).toEqual('Adam')
  })
  it('addCommentToCandidate function', () => {
    candidatesStore.init([{ id: 1, first_name: 'anand', comments: ['Just a comment'], comment_count: 1 }])
    candidatesStore.addCommentToCandidate('Just another comment', { id: 1 })
    const storeData = candidatesStore.get()
    expect(storeData.candidates[0].comments[0]).toEqual('Just a comment')
    expect(storeData.candidates[0].comments[1]).toEqual('Just another comment')
    expect(storeData.candidates[0].comment_count).toEqual(2)
  })
  it('editCommentOnCandidate', () => {
    let comment1 = {
      id: 1,
      comment: 'Life is stranger'
    }
    let comment2 = {
      id: 1,
      comment: 'doobi doobi dooba'
    }
    let candidate1 = {
      'id': 1,
      'comments': [comment1],
      'comment_count': 1
    }
    candidatesStore.init([ candidate1 ])
    candidatesStore.editCommentOnCandidate(comment2, candidate1)
    candidatesStore.get()
    expect(candidate1.comments[0]).toEqual(comment1)
  })
  it('removeCommentFromCandidate', () => {
    let comment1 = {
      id: 1,
      comment: 'Latest 14 September'
    }
    let candidate1 = {
      'id': 1,
      'comments': [comment1],
      'comment_count': 1
    }
    candidatesStore.init([ candidate1 ])
    console.log(candidate1.comments[0])
    candidatesStore.removeCommentFromCandidate(comment1, candidate1)
    candidatesStore.get()
    // console.log(candidate1.comments[0])
    expect(candidate1.comments).toEqual([])
  })
  it('deleteCandidates function', () => {
    candidatesStore.init([{ id: 1, first_name: 'anands' }])
    candidatesStore.deleteCandidates([1])
    const storedeleteData = candidatesStore.get()
    expect(storedeleteData.candidates.length).toEqual(0)
    candidatesStore.clearFilters()
  })
})
