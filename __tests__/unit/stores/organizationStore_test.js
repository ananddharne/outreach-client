import { organizationStore } from 'stores'

describe('organizationStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof organizationStore.get).toBe('function')
    expect(typeof organizationStore.set).toBe('function')
    expect(typeof organizationStore.init).toBe('function')
    expect(typeof organizationStore.setCurrentOrganization).toBe('function')
    expect(typeof organizationStore.updateOrganizations).toBe('function')
    expect(organizationStore.store.organization.name).toEqual('')
    expect(organizationStore.store.organization.address).toBeNull()
    expect(organizationStore.store.organization.industry).toEqual({})
    expect(organizationStore.store.organization.linkedin_url).toEqual('')
    expect(organizationStore.store.organization.primary_contact_first_name).toEqual('')
    expect(organizationStore.store.organization.primary_contact_last_name).toEqual('')
    expect(organizationStore.store.organization.website_url).toEqual('')
    expect(organizationStore.store.organization.primary_contact_phone).toEqual('')
    expect(organizationStore.store.organization.primary_contact_email).toEqual('')
    expect(organizationStore.store.organization.logo_url).toEqual('')
    expect(organizationStore.store.organizations).toEqual([])
  })
  it('init function', () => {
    organizationStore.init(['HAPPIE'])
    const storeData = organizationStore.get()
    expect(storeData.organizations).toEqual(['HAPPIE'])
  })
  it('set function', () => {
    organizationStore.set('name', 'Anand')
    const storeData = organizationStore.get()
    expect(storeData.organization.name).toEqual('Anand')
  })
  it('setCurrentOrganization function', () => {
    let organization1 = {
      'id': 1,
      'name': 'Anand',
      'linkedin_url': 'https://linkedin.com',
      'website_url': 'https://kilo.com'
    }
    let organization2 = {
      'id': 2,
      'name': 'Anandi',
      'linkedin_url': 'https://linkedin.com/',
      'website_url': 'https://kili.com'
    }
    organizationStore.init([organization1, organization2])
    organizationStore.setCurrentOrganization(organization1)
    expect(organizationStore.store.organization.name).toEqual('Anand')
    organizationStore.updateOrganizations({ id: 2 })
  })
})
