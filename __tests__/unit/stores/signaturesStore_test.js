import { signaturesStore } from 'stores'

describe('signatureStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof signaturesStore.get).toBe('function')
    expect(typeof signaturesStore.init).toBe('function')
    expect(signaturesStore.store.signatures).toEqual([])
  })
  it('init function', () => {
    signaturesStore.init(['anand', 'swjob', 'python developer'])
    const storeData = signaturesStore.get()
    expect(storeData.signatures).toEqual(['anand', 'swjob', 'python developer'])
  })
})
