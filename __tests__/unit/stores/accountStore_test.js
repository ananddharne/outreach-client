import { accountStore } from 'stores'

describe('accountstore', () => {
  it('get function and assert inital store data', () => {
    expect(typeof accountStore.get).toBe('function')
    expect(typeof accountStore.set).toBe('function')
    expect(typeof accountStore.init).toBe('function')
    expect(accountStore.store.account.first_name).toEqual('')
    expect(accountStore.store.account.last_name).toEqual('')
    expect(accountStore.store.account.title).toEqual('')
    expect(accountStore.store.account.linkedin_url).toEqual('')
    expect(accountStore.store.account.email).toEqual('')
    expect(accountStore.store.account.phones[0].type).toEqual('')
    expect(accountStore.store.account.phones[0].number).toEqual('')
    expect(accountStore.store.account.profile_photo_url).toEqual('')
    expect(accountStore.store.account.id).toEqual('')
    expect(accountStore.store.account.company).toEqual('')
    expect(accountStore.store.account.current_organization).toEqual('')
    expect(accountStore.store.account.timezone).toEqual('')
    expect(accountStore.store.account.notification_filters).toEqual([''])
    expect(accountStore.store.account.addresses[0].street_address_1).toEqual('')
    expect(accountStore.store.account.addresses[0].street_address_2).toEqual('')
    expect(accountStore.store.account.addresses[0].city).toEqual('')
    expect(accountStore.store.account.addresses[0].state).toEqual('')
    expect(accountStore.store.account.addresses[0].postal_code).toEqual('')
  })

  it('init function', () => {
    accountStore.init({
      user: {
        company: {
          name: 'Happie'
        },
        current_organization: ''
      }
    })
    const storeData = accountStore.get()
    expect(storeData.account.company.name).toEqual('Happie')
  })
  it('init2 function', () => {
    accountStore.init({
      user: {
        company: {
          name: 'Happie'
        },
        current_organization: {
          name: 'Amala',
          is_transactional: true
        }
      }
    })
    const storeData = accountStore.get()
    expect(storeData.account.current_organization.name).toEqual('Amala')
  })
  it('init3 function', () => {
    accountStore.init({
      is_system_admin: true,
      is_company_admin: true,
      user: {
        company: {
          name: 'Happie'
        },
        current_organization: {
          name: 'Sourcing',
          is_transactional: true
        },
        is_sourced_req_admin: false,
        groups: ['Sourcers'],
        is_sourcer: true
      }
    })
    const storeData = accountStore.get()
    expect(storeData.account.current_organization.name).toEqual('Sourcing')
    expect(storeData.account.is_transactional).toBeFalsy()
  })
  it('init4 function', () => {
    accountStore.init({
      user: {
        company: 'Happie',
        current_organization: 'Sourcing',
        is_manager: false,
        is_transactional: false,
        groups: ['Account Managers']
      }
    })
    const storeData = accountStore.get()
    expect(storeData.account.current_organization).toEqual('Sourcing')
  })

  it('set function', () => {
    accountStore.init({
      user: {
        company: 'Happie',
        current_organization: 'Amala'
      }
    })
    expect(typeof accountStore.set).toBe('function')
    accountStore.set('company', 'Anand')
    const storeData = accountStore.get()
    expect(storeData.account.company).toEqual('Anand')
  })
})
