import { usersStore } from 'stores'

describe('usersStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof usersStore.get).toBe('function')
    expect(typeof usersStore.init).toBe('function')
  })

  it('init function', () => {
    usersStore.init(['Trager'])
    const storeData = usersStore.get()
    expect(storeData.users).toEqual(['Trager'])
  })
})
