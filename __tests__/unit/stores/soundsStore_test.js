import { soundsStore } from 'stores'
import notification from 'media/notification.mp3'

describe('soundsStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof soundsStore.get).toBe('function')
    expect(typeof soundsStore.playSound).toBe('function')
    expect(typeof soundsStore.pauseSound).toBe('function')
  })

  it('get function', () => {
    soundsStore.store.sounds.blep = 'true'
    soundsStore.get()
    console.log(soundsStore.store)
    expect(soundsStore.store).toEqual({ sounds: {
      pop: new Audio(notification),
      blep: 'true'
    } })
  })
})
it('playSound', () => {
  soundsStore.playSound('pop')
})
it('pauseSound', () => {
  soundsStore.pauseSound('pop')
})
