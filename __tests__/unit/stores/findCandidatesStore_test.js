import { findCandidatesStore } from 'stores'

describe('findCandidatesStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof findCandidatesStore.get).toBe('function')
    expect(typeof findCandidatesStore.set).toBe('function')
    expect(findCandidatesStore.store.candidates).toEqual([])
    expect(findCandidatesStore.store.filters.locations).toEqual([])
    expect(findCandidatesStore.store.filters.location_radius).toEqual(100)
    expect(findCandidatesStore.store.filters.titles).toEqual([])
    expect(findCandidatesStore.store.filters.current_title).toBeFalsy()
    expect(findCandidatesStore.store.filters.companies).toEqual([])
    expect(findCandidatesStore.store.filters.current_company).toBeFalsy()
    expect(findCandidatesStore.store.filters.role_duration).toEqual([])
    expect(findCandidatesStore.store.filters.experience_duration).toEqual([])
    expect(findCandidatesStore.store.filters.minimum_degree).toBeFalsy()
    expect(findCandidatesStore.store.filters.fields_of_study).toEqual([])
    expect(findCandidatesStore.store.filters.institutions).toEqual([])
    expect(findCandidatesStore.store.filters.offset).toEqual(0)
    expect(findCandidatesStore.store.query.q).toEqual('')
    expect(findCandidatesStore.store.query.total_count).toEqual(0)
  })
  it('set, get function', () => {
    findCandidatesStore.set('location_radius', 101)
    const storeData = findCandidatesStore.get()
    expect(storeData.filters.location_radius).toEqual(101)
  })
})
