import { listsStore } from 'stores'

describe('listsStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof listsStore.get).toBe('function')
    expect(typeof listsStore.init).toBe('function')
    expect(listsStore.store.lists).toEqual([])
  })

  it('init function', () => {
    listsStore.init(['anand', 'swjob', 'python developer'])
    const storeData = listsStore.get()
    expect(storeData.lists).toEqual(['anand', 'swjob', 'python developer'])
  })
})
