import { sequencesStore } from 'stores'

describe('sequencesStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof sequencesStore.get).toBe('function')
    expect(typeof sequencesStore.getSequence).toBe('function')
    expect(typeof sequencesStore.init).toBe('function')
    expect(typeof sequencesStore.getSequence).toBe('function')
    expect(typeof sequencesStore.addSequences).toBe('function')
    expect(typeof sequencesStore.updateSequences).toBe('function')
    expect(typeof sequencesStore.deleteSequences).toBe('function')
    expect(typeof sequencesStore.setUnsavedSequence).toBe('function')
    expect(typeof sequencesStore.getUnsavedSequence).toBe('function')
    expect(typeof sequencesStore.resetUnsavedSequence).toBe('function')
    expect(sequencesStore.store.sequences).toEqual([])
    expect(sequencesStore.store.unsavedSequence).toBeNull()
  })

  it('init function', () => {
    sequencesStore.init(['anand', 'swsequence', 'python developer'])
    const storeData = sequencesStore.get()
    expect(storeData.sequences).toEqual(['anand', 'swsequence', 'python developer'])
  })

  it('getSequence function', () => {
    let sequences = {
      'id': 1
    }
    sequencesStore.init([ sequences, 'swsequence', 'python developer' ])
    const thesequence = sequencesStore.getSequence(1)
    expect(thesequence).toEqual({ 'id': 1 })
  })

  it('addSequences function', () => {
    sequencesStore.init([ 'swsequence', 'python developer' ])
    let sequences1 = {
      'id': 1
    }
    sequencesStore.addSequences([sequences1])
    expect(sequencesStore.store.sequences).toEqual(['swsequence', 'python developer', { id: 1 }])
    sequencesStore.clear()
    sequencesStore.addSequences([sequences1])
    expect(sequencesStore.store.sequences).toEqual([{ id: 1 }])
  })

  it('updateSequences function', () => {
    let sequences1 = {
      'id': 1,
      'title': ''
    }
    sequencesStore.init([ sequences1, 'python developer' ])
    sequencesStore.updateSequences([{ id: 1, title: 'sequence' }])
    expect(sequencesStore.store.sequences).toEqual([{ id: 1, title: 'sequence' }, 'python developer'])
  })
  it('deleteSequences function', () => {
    let sequences1 = {
      'id': 1
    }
    sequencesStore.init([ sequences1, 'python developer' ])
    sequencesStore.deleteSequences([1])
    expect(sequencesStore.store.sequences).toEqual(['python developer'])
  })

  it('setUnsavedSequences, getUnsavedSequences, resetUnsavedSequences function', () => {
    let sequences1 = {
      'id': 1
    }
    sequencesStore.init([ 'python developer' ])
    sequencesStore.setUnsavedSequence(sequences1)
    expect(sequencesStore.store.unsavedSequence).toEqual(sequences1)
    sequencesStore.getUnsavedSequence()
    sequencesStore.resetUnsavedSequence()
    sequencesStore.get()
    expect(sequencesStore.store.unsavedSequence).toEqual(null)
  })
})
