import { authStore } from 'stores'

describe('authStore', () => {
  it('asserting functions', () => {
    expect(typeof authStore.setToken).toBe('function')
    expect(typeof authStore.getToken).toBe('function')
    expect(typeof authStore.logout).toBe('function')
    expect(typeof authStore.setRedirectPath).toBe('function')
    expect(typeof authStore.getRedirectPath).toBe('function')
    expect(typeof authStore.resetRedirectPath).toBe('function')
    expect(typeof authStore.setInviteToken).toBe('function')
    expect(typeof authStore.getInviteToken).toBe('function')
  })
})

describe('Function calls', () => {
  it('must call setToken', () => {
    // Dont need the spy keeping it just for reference to use it in future components
    authStore.setToken('816cf7613c5aa175e13ccbacd6e1d633a963bbc0')
    // spyOn(authStore, 'getToken')
    let getToken
    getToken = authStore.getToken()
    expect(getToken).toEqual('816cf7613c5aa175e13ccbacd6e1d633a963bbc0')
    authStore.logout()
  })
  it('must call AdminToken', () => {
    authStore.setAdminToken('816cf7613c5aa175e13ccbacd6e1d633a963bbc0')
    // spyOn(authStore, 'getAdminToken')
    let theAdminToken
    theAdminToken = authStore.getAdminToken()
    expect(theAdminToken).toEqual('816cf7613c5aa175e13ccbacd6e1d633a963bbc0')
    authStore.removeAdminToken()
  })
  it('must call setRedirectPath', () => {
    authStore.setRedirectPath('https://talent.staging.gethappie.me')
    // spyOn(authStore, 'setRedirectPath')
    let getRedirect = authStore.getRedirectPath()
    console.log(getRedirect)
    expect(getRedirect).toEqual('https://talent.staging.gethappie.me')
    authStore.resetRedirectPath()
  })
  it('must call setAuth0Token', () => {
    // Dont need the spy keeping it just for reference to use it in future components
    let theAuth0Token
    authStore.setAuth0Token('816cf7613c5aa175e13ccbacd6e1d633a963bbc0')
    theAuth0Token = authStore.getAuth0Token()
    expect(theAuth0Token).toEqual('816cf7613c5aa175e13ccbacd6e1d633a963bbc0')
    authStore.logout()
  })
  it('must call setInviteToken', () => {
    // Dont need the spy keeping it just for reference to use it in future components
    let theGetInviteToken
    authStore.setInviteToken('816cf7613c5aa175e13ccbacd6e1d633a963bbc0')
    theGetInviteToken = authStore.getInviteToken()
    expect(theGetInviteToken).toEqual('816cf7613c5aa175e13ccbacd6e1d633a963bbc0')
    authStore.logout()
  })
})
