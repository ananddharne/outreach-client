import { phoneNumbersStore } from 'stores'

describe('phoneNumber', () => {
  it('assertions on store', () => {
    expect(typeof phoneNumbersStore.init).toBe('function')
    expect(typeof phoneNumbersStore.get).toBe('function')
    expect(typeof phoneNumbersStore.addPhones).toBe('function')
  })
  it('init and get', () => {
    let phone1 = {
      number: '9876545567'
    }
    phoneNumbersStore.init([phone1])
    phoneNumbersStore.get()
    expect(phoneNumbersStore.store.phones).toEqual([phone1])
  })
  it('addPhones', () => {
    let phone1 = {
      number: '9876545567'
    }
    let phone2 = {
      number: '9877545567'
    }
    phoneNumbersStore.get()
    phoneNumbersStore.init([phone1])
    phoneNumbersStore.addPhones(phone2)
    expect(phoneNumbersStore.store.phones).toEqual([phone1, phone2])
  })
})
