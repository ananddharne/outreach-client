import { conversationsStore } from 'stores'

describe('conversationsStore', () => {
  it('get function and assert inital store data', () => {
    expect(typeof conversationsStore.get).toBe('function')
    expect(typeof conversationsStore.init).toBe('function')
    expect(typeof conversationsStore.add).toBe('function')
    expect(typeof conversationsStore.update).toBe('function')
    expect(typeof conversationsStore.clearFilters).toBe('function')
    expect(typeof conversationsStore.delete).toBe('function')
  })
  it('init and get', () => {
    let convo1 = {
      id: 1,
      name: 'convo'
    }
    let convo2 = {
      id: 2,
      name: 'convo2'
    }
    conversationsStore.init([convo1, convo2])
    conversationsStore.get()
    expect(conversationsStore.store.conversations).toEqual([convo1, convo2])
  })
  it('add', () => {
    let convo1 = {
      id: 1,
      name: 'convo'
    }
    let convo2 = {
      id: 2,
      name: 'convo2'
    }
    let convo3 = {
      id: 3,
      name: 'convo3'
    }
    conversationsStore.init([convo1, convo2])
    conversationsStore.add([convo3])
    conversationsStore.get()
    expect(conversationsStore.store.conversations).toEqual([convo1, convo2, convo3])
  })
  it('update', () => {
    let convo1 = {
      id: 1,
      name: 'convo'
    }
    let convo2 = {
      id: 3,
      name: 'convo2'
    }
    let convo3 = {
      id: 3,
      name: 'convo3'
    }
    conversationsStore.init([convo1, convo2])
    conversationsStore.update(convo3)
    conversationsStore.get()
    expect(conversationsStore.store.conversations).toEqual([convo1, convo3])
  })
  it('delete', () => {
    let convo1 = {
      id: 1,
      name: 'convo'
    }
    let convo2 = {
      id: 3,
      name: 'convo2'
    }
    conversationsStore.init([convo1, convo2])
    conversationsStore.delete(convo2)
    conversationsStore.get()
    expect(conversationsStore.store.conversations).toEqual([convo1])
    conversationsStore.clearFilters()
  })
})
