import { sourcedReqStore } from 'stores'

describe('sourcedReqStore', () => {
  it('establishes default values for itself', () => {
    expect(typeof sourcedReqStore.store).toBe('object')
    expect(sourcedReqStore.store.sourcedReqs).toEqual([])
    // Filters
    expect(typeof sourcedReqStore.store.filters).toBe('object')
    expect(sourcedReqStore.store.filters.sourcer).toEqual(0)
    expect(sourcedReqStore.store.filters.sales_person).toEqual(0)
    expect(sourcedReqStore.store.filters.account_manager).toEqual(0)
    expect(sourcedReqStore.store.filters.sourcing_statuses).toEqual(null)
    expect(sourcedReqStore.store.filters.account_statuses).toEqual('')
    expect(sourcedReqStore.store.filters.req_statuses).toEqual('')
    expect(sourcedReqStore.store.filters.engagement_date_start).toEqual(null)
    expect(sourcedReqStore.store.filters.engagement_date_end).toEqual(null)
    expect(sourcedReqStore.store.filters.active).toEqual('')
    expect(sourcedReqStore.store.filters.search).toEqual('')
    expect(sourcedReqStore.store.filters.offset).toEqual(0)
    expect(sourcedReqStore.store.filters.limit).toEqual(10)
    expect(sourcedReqStore.store.totalCount).toEqual(0)
  })
  it('creates helper functions for interacting with itself', () => {
    expect(typeof sourcedReqStore.init).toBe('function')
    expect(typeof sourcedReqStore.get).toBe('function')
    expect(typeof sourcedReqStore.add).toBe('function')
    expect(typeof sourcedReqStore.deleteJob).toBe('function')
    expect(typeof sourcedReqStore.addComment).toBe('function')
    expect(typeof sourcedReqStore.editComment).toBe('function')
    expect(typeof sourcedReqStore.removeComment).toBe('function')
    expect(typeof sourcedReqStore.update).toBe('function')
    expect(typeof sourcedReqStore.clearFilters).toBe('function')
  })
  it('add', () => {
    sourcedReqStore.init([ 'python developer' ])
    sourcedReqStore.add('new sourcedReq')
    const storeData = sourcedReqStore.get()
    expect(storeData.sourcedReqs).toEqual(['python developer', 'new sourcedReq'])
  })
  it('update function', () => {
    let convo1 = {
      'id': 1,
      'title': 'mi'
    }
    sourcedReqStore.init([ convo1, 'python developer' ])
    let convo2 = {
      'id': 1,
      'title': 'I am a convo'
    }
    sourcedReqStore.update(convo2)

    const storeData = sourcedReqStore.get()
    expect(storeData.sourcedReqs).toEqual([convo2, 'python developer'])
  })
  it('clear filters', () => {
    sourcedReqStore.store.filters.limit = 11
    expect(sourcedReqStore.store.filters.limit).toEqual(11)
    sourcedReqStore.clearFilters()
    expect(sourcedReqStore.store.filters.limit).toEqual(10)
  })
  it('addComment', () => {
    let sourcedReq1 = {
      'id': 1,
      'comments': [],
      'comment_count': 0
    }
    sourcedReqStore.init([ sourcedReq1, 'python developer' ])
    sourcedReqStore.addComment('Life is strange', sourcedReq1)
    const storeData = sourcedReq1.comments
    expect(storeData).toEqual(['Life is strange'])
    expect(sourcedReq1.comment_count).toEqual(1)
  })
  it('editComment', () => {
    let comment1 = {
      id: 1,
      comment: 'Life is stranger'
    }
    let comment2 = {
      id: 1,
      comment: 'doobi doobi dooba'
    }
    let sourcedReq2 = {
      'id': 1,
      'comments': [comment1],
      'comment_count': 1
    }
    sourcedReqStore.init([ sourcedReq2, 'python developer' ])
    sourcedReqStore.editComment(comment2, sourcedReq2)
    sourcedReqStore.get()
    expect(sourcedReq2.comments[0]).toEqual(comment1)
  })
  it('removeComment', () => {
    let comment1 = {
      id: 1,
      comment: 'Life is stranger'
    }
    let sourcedReq2 = {
      'id': 1,
      'comments': [comment1],
      'comment_count': 1
    }
    sourcedReqStore.init([ sourcedReq2, 'python developer' ])
    sourcedReqStore.removeComment(comment1, sourcedReq2)
    sourcedReqStore.get()
    expect(sourcedReq2.comments).toEqual([])
  })
  it('deleteJob', () => {
    let job1 = {
      id: 1,
      name: 'anands'
    }
    let sourcedReq2 = {
      'req': 1,
      'comment_count': 1
    }
    sourcedReqStore.init([ sourcedReq2 ])
    sourcedReqStore.deleteJob(1)
    const storeData = sourcedReqStore.get()
    expect(storeData.sourcedReqs).toEqual([{ req: null, comment_count: 1, active: false }])
  })
})
