import { templatesStore } from 'stores'

describe('templatesStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof templatesStore.init).toBe('function')
    expect(typeof templatesStore.get).toBe('function')
    expect(typeof templatesStore.getTemplate).toBe('function')
    expect(typeof templatesStore.addTemplates).toBe('function')
    expect(typeof templatesStore.updateTemplates).toBe('function')
    expect(typeof templatesStore.deleteTemplates).toBe('function')
    expect(typeof templatesStore.deleteTemplates).toBe('function')
    expect(typeof templatesStore.setUnsavedTemplate).toBe('function')
    expect(typeof templatesStore.getUnsavedTemplate).toBe('function')
    expect(typeof templatesStore.resetUnsavedTemplate).toBe('function')
    expect(templatesStore.store.templates).toEqual([])
    expect(templatesStore.store.unsavedTemplate).toBeNull()
  })
  it('init function', () => {
    templatesStore.init(['anand', 'swtemplates', 'python developer'])
    const storeData = templatesStore.get()
    expect(storeData.templates).toEqual(['anand', 'swtemplates', 'python developer'])
  })

  it('getTemplate function', () => {
    let template1 = {
      'id': 1
    }
    templatesStore.init([ template1, 'swtemplates', 'python developer' ])
    const thetemplate = templatesStore.getTemplate(1)
    // console.log(thetemplate)
    expect(thetemplate).toEqual({ 'id': 1 })
  })

  it('addTemplates function', () => {
    templatesStore.init([ 'swtemplates', 'python developer' ])
    let templates1 = {
      'id': 1
    }
    templatesStore.addTemplates([templates1])
    console.log(templatesStore.store.templates)
    expect(templatesStore.store.templates).toEqual(['swtemplates', 'python developer', { id: 1 }])
    templatesStore.clear()
    console.log(templatesStore.store.templates)
    templatesStore.addTemplates([templates1])
    // console.log(templatesStore.store.templates)
    expect(templatesStore.store.templates).toEqual([{ id: 1 }])
  })

  it('updateTemplates function', () => {
    let templates1 = {
      'id': 1,
      'title': ''
    }
    templatesStore.init([ templates1, 'python developer' ])
    templatesStore.updateTemplates([{ id: 1, title: 'uit' }])
    // console.log(templatesStore.store.templates)
    expect(templatesStore.store.templates).toEqual([{ id: 1, title: 'uit' }, 'python developer'])
  })
  it('deleteTemplates function', () => {
    let templates1 = {
      'id': 1
    }
    templatesStore.init([ templates1, 'python developer' ])
    templatesStore.deleteTemplates([1])
    // console.log(templatesStore.store.templates)
    expect(templatesStore.store.templates).toEqual(['python developer'])
  })

  it('setUnsavedTemplate, getUnsavedTemplate, resetUnsavedTemplate function', () => {
    let templates1 = {
      'id': 1
    }
    templatesStore.init([ 'python developer' ])
    templatesStore.setUnsavedTemplate(templates1)
    expect(templatesStore.store.unsavedTemplate).toEqual(templates1)
    templatesStore.getUnsavedTemplate()
    templatesStore.resetUnsavedTemplate()
    expect(templatesStore.store.unsavedTemplate).toEqual(null)
  })
})
