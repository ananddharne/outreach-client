import { userPreferencesStore } from 'stores'

describe('userPreferencesStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof userPreferencesStore.get).toBe('function')
    expect(typeof userPreferencesStore.set).toBe('function')
    expect(typeof userPreferencesStore.init).toBe('function')
    expect(userPreferencesStore.defaults.candidateGridType).toEqual('table')
    expect(userPreferencesStore.defaults.candidatesPerPage).toEqual(25)
    expect(userPreferencesStore.defaults.hiddenManagerColumns).toEqual([])
    expect(userPreferencesStore.defaults.hiddenSourcedReqColumns).toEqual([])
  })
  it('init function', () => {
    localStorage.clear()
    userPreferencesStore.init()
    // const storeData = userPreferencesStore.get()
    // expect(Object.keys(storeData).length).toEqual(4)
  })
  it('set function', () => {
    userPreferencesStore.set('candidatesPerPage', 35)
    const storeData = userPreferencesStore.get()
    expect(storeData.candidatesPerPage).toEqual(35)
  })
  it('init again function', () => {
    userPreferencesStore.clear()
    const storeData = userPreferencesStore.get()
    expect(storeData.candidatesPerPage).toEqual(25)
  })
})
