import { pusherStore } from 'stores'
import unitGlobals from './unitGlobals'

describe('pusherStore', () => {
  it('asserting store objects be empty and functions', () => {
    expect(typeof pusherStore.get).toBe('function')
    expect(typeof pusherStore.init).toBe('function')
    expect(pusherStore.store.app_id).toBeNull()
    expect(pusherStore.store.key).toBeNull()
    expect(pusherStore.store.secret).toBeNull()
    expect(pusherStore.store.pusher).toBeNull()
    expect(pusherStore.store.channels.user.channel).toBeNull()
    expect(typeof pusherStore.store.channels.user.promise.resolve).toBe('function')
    expect(pusherStore.store.channels.org.channel).toBeNull()
    expect(typeof pusherStore.store.channels.org.promise.resolve).toBe('function')
  })

  it('init for user function', (done) => {
    window.jasmine.DEFAULT_TIMEOUT_INTERVAL = 10000
    const storeData = pusherStore.get()
    unitGlobals.login()
    .done((response) => {
      pusherStore.init(response)
      expect(pusherStore.store.app_id).toEqual('318418')
      storeData.channels.user.promise.done(() => {
        expect(typeof pusherStore.store.channels.user.channel.bind).toBe('function')
        // done()
      })
      done()
    })
  })
  it('init for org function', () => {
    const storeData = pusherStore.get()
    pusherStore.init(unitGlobals.store.loginResponse)
    expect(pusherStore.store.app_id).toEqual('318418')
    storeData.channels.user.promise.done(() => {
      expect(typeof pusherStore.store.channels.org.channel.bind).toBe('function')
    })
  })
  it('current_organization.name edge case', () => {
    unitGlobals.store.loginResponse.user.current_organization.name = 'Sourcing'
    pusherStore.init(unitGlobals.store.loginResponse)
    const storeData = pusherStore.get()
    storeData.channels.org.promise.done(() => {
      expect(typeof pusherStore.store.channels.user.channel.bind).toBe('function')
    })
  })
  it('unsubscribe', () => {
    pusherStore.init(unitGlobals.store.loginResponse)
    const storeData = pusherStore.get()
    storeData.channels.user.promise.done(() => {
      // storeData.channels.org.promise.done(() => {
      expect(typeof storeData.channels.user.channel.bind).toBe('function')
      pusherStore.unsubscribe()
    })
  })
})
