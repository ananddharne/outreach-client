//
// import Vue from 'vue'
// import Login from 'components/Login'
// import VueRouter from 'vue-router'
//
// describe('Login', () => {
//   // Check data and methods on the un-mounted component
//   it('asserts mounted components and its methods', () => {
// // Create the router on the fly so that we can unit test the single component in
// // context of a vue router
//     Vue.use(VueRouter)
//     const router = new VueRouter({
//
//       routes: [
//
//         { path: '/logi', name: 'logintest', component: { render: h => h('div') } },
//         { path: '/logintest', name: 'loginc', component: Login }
//
//       ]
//     })
//
//     const vm = new Vue({
//       el: document.createElement('div'),
//       router: router,
//       render: h => h('router-view')
//     })
//     router.push({ name: 'loginc' })
//     Vue.nextTick(() => {
//       expect(typeof vm.lock.show).toBe('function')
//       expect(typeof vm.lock.on).toBe('function')
//       expect(typeof vm.lock.getUserInfo).toBe('function')
//       expect(vm.clientId).toBe('7VjjgoETZpxO6RHJbDXFFPnCqPlZjGxb')
//       expect(vm.domain).toBe('happie.auth0.com')
//     })
//   })
//
//   it('sets the correct default data', () => {
//     expect(typeof Login.data).toBe('function')
//     const defaultData = Login.data()
//     expect(defaultData.loaderVisible).toBe(false)
//     expect(defaultData.lock).toEqual({})
//     expect(typeof Login.mounted).toBe('function')
//   })
// })
