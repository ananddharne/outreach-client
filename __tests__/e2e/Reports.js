module.exports = {
  'Reports page testing': function (client) {
    client.url(client.launchUrl + client.globals.e2e_query_string)
    client.setToken()
    client.urlHash('reports/candidates/funnel')
    client.pause(1500)
    // and then wait for funnel api to complete.
    // GO to snapshot but wait for reports funnel to complete
    client.useCss()
    client.execute(function () {
      $('span:contains(Snapshot)').click()
    })
    client.pause(1000)
    // Until snapshot call is finished , wait and then click on status to get candidates of that status
    client.waitForAjax('/api/v1/reports/snapshot', function () {

    }, function () {
      client.waitForElementVisible('#app > div.app-container.reports-container > div.main-container > div > div:nth-child(3) > div.table-container > table > tbody > tr:nth-child(6) > td:nth-child(1) > a',1000)
      client.click('#app > div.app-container.reports-container > div.main-container > div > div:nth-child(3) > div.table-container > table > tbody > tr:nth-child(6) > td:nth-child(1) > a')
    })

    client.pause(500)
    // Navigate to one of the candidates profile and details (Ofcourse don't forget to wait for getting all candidates first)
    client.waitForAjax('/api/v1/reports/snapshot/candidates/?status=new&offset=0&limit=25&count=1', function () {

    }, function () {
      client.waitForElementVisible('table tbody td span.name-link', 1000)
      client.click('table tbody td span.name-link')
      client.pause(1000)
    })
    client.end()
  }
}
