const uuidv1 = require('uuid/v1')
const moment = require('moment')

module.exports = {
  'Inbox Page Testing': function (client) {
    client.url(client.launchUrl + client.globals.e2e_query_string)
    .setToken()
    .urlHash('outreach/candidates')
    .switchOrg('Dev')
    // Switch to the Dev org
    .urlHash('inbox')
    // Start a conversation modal tests
    .waitForElementVisible('span[role="button"].create-new', 1000)
    .click('span[role="button"].create-new')
    .pause(250)
    .waitForElementVisible('.attention-button', 5000)
    .click('.attention-button')
    .waitForAjax('/api/v1/candidates/?super_minimal=1', function () {
    }, function () {
      client.pause(250)
    })
    // Add new phone number test
    .waitForElementVisible('#test-phone-numbers-dropdown span.label', 1000)
    .click('#test-phone-numbers-dropdown span.label')
    .waitForElementVisible('#test-phone-numbers-dropdown .selectric-items li[data-index="1"]', 1000)
    .click('#test-phone-numbers-dropdown .selectric-items li[data-index="1"]')
    .pause(250)
    .waitForElementVisible('input[placeholder="Enter a name..."]', 1000)
    .setValue('input[placeholder="Enter a name..."]', 'Test Number')
    .waitForElementVisible('textarea[placeholder="Enter a description..."]', 1000)
    .setValue('textarea[placeholder="Enter a description..."]', 'Test Description')
    .waitForElementVisible('input[placeholder="Enter area code..."]', 1000)
    .setValue('input[placeholder="Enter area code..."]', '617')
    .pause(250)
    .waitForElementVisible('.form-row button.prompt-button', 1000)
    .click('.form-row button.prompt-button')
    .waitForAjax('/api/v1/tracking-numbers/search/?area_code=617', function () {
    }, function () {
      // Make sure the api call actually returns phone numbers
      client.execute(function () {
        return $('.alert.notify-message').length
      }, [], function (result) {
        if (!result.value) {
          client.waitForElementVisible('.form-row.numbers span.number:first-child', 1000)
          .click('.form-row.numbers span.number:first-child')
          .assert.containsText('.form-row button.prompt-button', 'SAVE')
        }
      })
    })
    // Close all the modals
    .click('.modal-collection-container > span .hp-modal-overlay:last-child img.hp-modal-close-button')
    .pause(500)
    .click('.modal-collection-container > span .hp-modal-overlay:last-child img.hp-modal-close-button')
    .pause(500)
    .click('.modal-collection-container > span .hp-modal-overlay:last-child img.hp-modal-close-button')
    .pause(500)
    // Test conversation filters
    .waitForElementVisible('input[placeholder="Search by name..."]', 1000)
    .setValue('input[placeholder="Search by name..."]', 'Adam Lynch')
    .waitForElementVisible('#test-jobs-dropdown span.label', 1000)
    .click('#test-jobs-dropdown span.label')
    .waitForElementVisible('#test-jobs-dropdown .selectric-items li[data-index="1"]', 1000)
    .click('#test-jobs-dropdown .selectric-items li[data-index="1"]')
    .waitForElementVisible('#test-convo-type-dropdown span.label', 1000)
    .click('#test-convo-type-dropdown span.label')
    .pause(1000)
    .waitForElementVisible('#test-convo-type-dropdown .selectric-items li[data-index="2"]', 1000)
    .click('#test-convo-type-dropdown .selectric-items li[data-index="2"]')
    .waitForAjax('/api/v1/conversations/8/?mark_read=1', function () {
    }, function () {
      // Make sure all the action buttons are there
      client.waitForElementVisible('.inbox-btn.star', 1000)
      .waitForElementVisible('.inbox-btn.clear', 1000)
      .waitForElementVisible('.inbox-btn.archive', 1000)
      .waitForElementVisible('img.dropdown-button', 1000)
      .click('img.dropdown-button')
      // Make sure there are 7 actions in the dropdown
      .waitForElementVisible('.top-button-container .selectric-items li[data-index="7"]', 1000)
    })
    // Test composing a message
    .waitForElementVisible('.fr-box .fr-element', 1000)
    .execute(function () {
      $('.fr-box').froalaEditor('html.set', 'Hey, what\'s up!!')
    })
    .click('.fr-box .fr-element')
    .sendKeys('.fr-box .fr-element', client.Keys.DOWN_ARROW)
    .sendKeys('.fr-box .fr-element', client.Keys.BACK_SPACE)
    .pause(1000)
    .assert.containsText('.characters-used .amount', '105')
    .click('#test-templates-dropdown')
    .waitForElementVisible('#test-templates-dropdown .selectric-items li[data-index="2"]', 1000)
    // Test sending and receiving a message
    .click('.send-button')
    .waitForAjax('/api/v1/conversations/8/', function () {
    }, function () {
      client.waitForElementVisible('.messages-container .message-container.user:last-child', 1000)
      client.execute(function (messageId, timeStamp) {
        // Call the webhook to simulate a text message response from the candidate
        let userId = 'u-67kaeuvr5dyp62lfvtwuv7y'
        let appId = 'a-5ztqxzssebvgjeiczhhvziq'

        $.ajax('http://talent-staging.gethappie.me/telephony/bw/sms_event/', {
          type: 'GET',
          data: {
            eventType: 'sms',
            direction: 'in',
            messageId: messageId,
            messageUri: `https://api.catapult.inetwork.com/v1/users/${userId}/messages/${messageId}`,
            from: '+12075183602',
            to: '+17812050935',
            text: 'My number is (207) 518-3602 and my email is adam@gethappie.me',
            applicationId: appId,
            time: timeStamp,
            state: 'received'
          }
        })
      }, [uuidv1(), moment().format()])
    })
    .waitForElementVisible('.messages-container .message-container.candidate:last-child', 3000)
    // Test the add email and add phone from a message
    .waitForElementVisible('.messages-container .message-container.candidate .add-to-candidate.phone', 1000)
    .click('.messages-container .message-container.candidate .add-to-candidate.phone')
    .waitForElementVisible('.add-to-candidate-container.phone', 1000)
    .waitForElementVisible('.messages-container .message-container.candidate .add-to-candidate.email', 1000)
    .click('.messages-container .message-container.candidate .add-to-candidate.email')
    .waitForElementVisible('.add-to-candidate-container.email', 1000)
    // Test the candidate info section
    .waitForElementVisible('.targeted-jobs', 1000)
    .waitForElementVisible('#test-status-dropdown span.label', 1000)
    .click('#test-status-dropdown span.label')
    .waitForElementVisible('#test-status-dropdown .selected.highlighted', 1000)
    .execute(function () {
      if (parseInt($('#test-status-dropdown .selected.highlighted').attr('data-index')) === 1) {
        $('#test-status-dropdown .selected.highlighted').next().click()
      } else {
        $('#test-status-dropdown .selected.highlighted').prev().click()
      }
    })
    .waitForElementVisible('.preview-container .candidate-name', 1000)
    .waitForElementVisible('.candidate-comment-box', 1000)
    .waitForElementVisible('.candidate-activity-list', 1000)
    // Switch back to the testing org
    .pause(1000)
    .switchOrg('Testing')
    .end()
  }
}
