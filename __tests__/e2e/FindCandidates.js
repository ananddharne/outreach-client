// module.exports = {
//   'Find candidates page testing': function (client) {
//     client.url(client.launchUrl + client.globals.e2e_query_string)
//     client.setToken()
//     client.urlHash('find-candidates')
//     .useCss()
//     .waitForElementVisible('div.filter-collapse', 1000)
//     client.pause(250)
//     .waitForElementVisible('input.location-input', 1000)
//     .setValue('input.location-input', 'San Francisco')
//     client.pause(250)
//     .sendKeys('input.location-input', client.Keys.DOWN_ARROW)
//     .sendKeys('input.location-input', client.Keys.ENTER)
//     client.pause(500)
//     .waitForElementVisible('span.remove-item', 1000)
//     .waitForElementVisible('div.vue-slider-hover.vue-slider-dot', 1000)
//     client.pause(250)
//     .moveToElement('div.vue-slider-hover.vue-slider-dot', 0, 0)
//     .mouseButtonDown(0)
//     .moveToElement('div.vue-slider-hover.vue-slider-dot', 106, 0)
//     .mouseButtonUp(0)
//     client.pause(250)
//     // Click on experience tab
//     client.execute(function () {
//       $('div:contains(EXPERIENCE)').click()
//     })
//     client.useCss()
//     client.pause(250)
//     .waitForElementVisible('input.title-input', 1000)
//     .setValue('input.title-input', 'Web Developer')
//     client.pause(250)
//     .waitForElementVisible('input.company-input', 1000)
//     .setValue('input.company-input', 'Happie')
//     client.pause(250)
//     .waitForElementVisible('span.find-candidates', 1000)
//     .execute(function () {
//       document.querySelector('span.find-candidates').click()
//     })
//     client.pause(250)
//     .waitForElementVisible('div.candidate-loader-container', 1000)
//     client.end()
//   }
// }
