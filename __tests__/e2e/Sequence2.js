module.exports = {
  'Sequence2 page Testing': function (client) {
    client.url(client.launchUrl + client.globals.e2e_query_string)
    client.setToken()
    client.urlHash('outreach/sequences/new')
    client.useCss()
    client.waitForElementVisible('input[placeholder="Enter a name for your sequence..."]', 1000)
    client.click('input[placeholder="Enter a name for your sequence..."]')
    client.pause(250)
    client.sendKeys('input[placeholder="Enter a name for your sequence..."]', client.Keys.PAGEDOWN)
    client.pause(250)
    client.sendKeys('input[placeholder="Enter a name for your sequence..."]', client.Keys.PAGEDOWN)
    client.pause(1000)
    // Add link
    .waitForElementVisible('#insertLink-1', 1000)
    .click('#insertLink-1')
    .click('#linkList-1')
    .sendKeys('#linkList-1', client.Keys.DOWN_ARROW)
    .sendKeys('#linkList-1', client.Keys.ENTER)
    .waitForElementVisible('#fr-link-insert-layer-1 button', 1000)
    .click('#fr-link-insert-layer-1 button')
    client.pause(1000)

    // Assert visibility after adding link TODO
    // Assert edit link
    .waitForElementVisible('#linkEdit-1', 1000)
    .click('#linkEdit-1')
    client.pause(250)
    .waitForElementVisible('#linkBack-1', 1000)
    .click('#linkBack-1')
    client.pause(1000)
    // Remove link
    .waitForElementVisible('#linkRemove-1', 1000)
    .click('#linkRemove-1')
    client.pause(500)
    // Use Template
    .waitForElementVisible('#test-email-template-dropdown span.label', 1000)
    .click('#test-email-template-dropdown span.label')
    client.pause(250)
    // Use a template option- this may change if you put up a new template so the li would change
    .waitForElementVisible('#test-email-template-dropdown .selectric-items li[data-index="2"]', 1000)
    .click('#test-email-template-dropdown .selectric-items li[data-index="2"]')
    client.execute(function () {
      $('#test-all-loadercontainer').remove()
    })
    // Select a template from the list of templates
    client.waitForAjax('/api/v1/templates/all/', function () {

    }, function () {
      client.waitForElementVisible('.template-list-container .templates li:nth-child(1)', 1000)
      client.click('.template-list-container .templates li:nth-child(1)')
      client.execute(function () {
        $('#test-all-loadercontainer').remove()
      })
    })
    client.pause(500) // Timeout for waiting for the response from one template
    // Default Signature
    client.execute(function () {
      $('span:contains(Default Signature)').click()
    })
    client.execute(function () {
      $('#test-all-loadercontainer').remove()
    })
    client.pause(500)
    // List of all default signatures
    .waitForElementVisible('#test-signatures-dropdown span.label', 1000)
    .click('#test-signatures-dropdown span.label')

    // Align RTE
    .waitForElementVisible('#align-1', 1000)
    .click('#align-1')
    .sendKeys('#align-1', client.Keys.ENTER)

    // Ordered list
    .waitForElementVisible('#formatOL-1', 1000)
    .click('#formatOL-1')
    client.pause(250)

    // unordered list
    .waitForElementVisible('#formatUL-1', 1000)
    .click('#formatUL-1')
    client.pause(250)

    // Increase indent
    .waitForElementVisible('#indent-1', 1000)
    .click('#indent-1')
    client.pause(250)

    // Formatting
    .waitForElementVisible('#clearFormatting-1', 1000)
    .click('#clearFormatting-1')
    client.pause(250)

    // Undo
    .waitForElementVisible('#undo-1', 1000)
    .click('#undo-1')
    client.pause(250)

    // Use preview button
    .waitForElementVisible('span.preview-button', 1000)
    .click('span.preview-button')
    client.waitForAjax('/api/v1/templates/preview/', function () {
    }, function () {
      client.pause(250)
      client.waitForElementVisible('img.hp-modal-close-button', 1000)
      client.click('img.hp-modal-close-button')
    })

    // Test span button is test
    client.pause(250)
    client.waitForElementVisible('span.test-button', 1000)
    client.click('span.test-button')
    client.pause(500)
    client.waitForElementVisible('input[placeholder="Search by name or email..."]', 1000)
    client.setValue('input[placeholder="Search by name or email..."]', 'Anand')
    client.pause(250)
    client.click('.user-list-container .users li:nth-child(1)')

    client.waitForAjax('/api/v1/emails/send/', function () {
    }, function () {
      // Click x out of modal
      client.waitForElementVisible('img.hp-modal-close-button', 1000)
      client.click('img.hp-modal-close-button')
      client.pause(250)
      // Click on Campaigns to opt out
      client.waitForElementVisible('ul.reports-nav li a[href="#/outreach/campaigns"]', 1000)
      client.click('ul.reports-nav li a[href="#/outreach/campaigns"]')
      client.pause(250)
      client.execute(function () {
        $('.prompt-button:eq(1)').click()
      })
    })
    client.end()
  }
}
