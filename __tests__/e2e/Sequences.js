module.exports = {

  'Sequence page Testing': function (client) {
    client.url(client.launchUrl + client.globals.e2e_query_string)
    client.setToken()
    client.urlHash('outreach/sequences')
    client.useCss()
    client.waitForElementVisible('span.add-block', 1000)
    client.click('span.add-block')

    client.waitForAjax('/api/v1/candidate-lists/', function () {

    }, function () {
      // Name
      client.waitForElementVisible('input[placeholder="Enter a name for your sequence..."]', 1000)
      .setValue('input[placeholder="Enter a name for your sequence..."]', 'Anand/Testing')
      client.pause(250)

      // Stop sending when Candidates
      client.waitForElementVisible('span#test-sequence-link-click', 1000)
      client.click('span#test-sequence-link-click')
      client.pause(250)

      // Send On
      client.execute(function () {
        $('.custom-checkbox-container:eq(2)').click()
      })
      client.pause(250)

      // Select Sequence Owner
      .waitForElementVisible('#test-sequence-owner-dropdown span.label', 1000)
      .click('#test-sequence-owner-dropdown span.label')
      client.pause(250)
      .waitForElementVisible('#test-sequence-owner-dropdown .selectric-items li[data-index="1"]', 1000)
      .click('#test-sequence-owner-dropdown .selectric-items li[data-index="1"]')
      client.pause(250)

      // Time zone
      .waitForElementVisible('#test-time-zone-dropdown span.label', 1000)
      .click('#test-time-zone-dropdown span.label')
      .waitForElementVisible('#test-time-zone-dropdown .selectric-items li[data-index="1"]', 1000)
      .click('#test-time-zone-dropdown .selectric-items li[data-index="1"]')

      // Default Reply to Name & E-mail
      client.pause(250)
      client.useCss()
      client.waitForElementVisible('input[placeholder="Enter the name of the person candidates reply to..."]', 1000)
      client.setValue('input[placeholder="Enter the name of the person candidates reply to..."]', 'Anand')
      // Email
      client.pause(250)
      client.waitForElementVisible('input[placeholder="Enter the email of the person candidates reply to..."]', 1000)
      client.setValue('input[placeholder="Enter the email of the person candidates reply to..."]', 'anand@gethappie.me')

      // Phone number
      client.pause(250)
      .waitForElementVisible('#test-sms-from-dropdown span.label', 1000)
      .click('#test-sms-from-dropdown span.label')
      client.pause(250)
      .waitForElementVisible('#test-sms-from-dropdown .selectric-items li[data-index="2"]', 1000)
      .click('#test-sms-from-dropdown .selectric-items li[data-index="2"]')
      client.pause(250)

      // ACTIVATE THE NEWLY CREATED LIST
      // client.useXpath()
      client.waitForElementVisible('div#test-activate-sequence-button', 1000)
      client.click('div#test-activate-sequence-button')
      client.pause(500)
      client.waitForElementVisible('button.prompt-button', 1000)
      client.click('button.prompt-button')
    })
    client.pause(250)

    // MANAGE CANDIDATES AND CLOSE IMMEDIATELY
    client.waitForAjax('/api/v1/sequences', function () {

    }, function () {
      client.useCss()
      client.waitForElementVisible('span.large-buttons.manage-candidates', 1000)
      client.click('span.large-buttons.manage-candidates')
    })
    client.pause(1000)
    // close
    .waitForElementVisible('img.hp-modal-close-button', 1000)
    .click('img.hp-modal-close-button')

    client.pause(500)

    // send to lists
    client.execute(function () {
      $('#test-sequences-lists').parent().find('input[placeholder="Select one or more lists..."]').click()
    })
    .pause(250)
    .sendKeys('input.select2-search__field', client.Keys.ENTER)
    .pause(250)
    .waitForElementVisible('button.prompt-button', 1000)
    .click('button.prompt-button')
    client.pause(2000)
    // Promote pages
    // client.execute(function () {
    //   $('#test-sequences-pages').parent().find('input[placeholder="Select one or more pages..."]').click()
    // })
    // client.pause(500)
    // client.sendKeys('#test-sequences-pages input.select2-search__field', client.Keys.ENTER)

    // Target jobs
    client.execute(function () {
      $('#test-sequences-jobs').parent().find('input[placeholder="Select one or more jobs..."]').click()
    })
    client.pause(250)
    client.waitForElementVisible('#test-sequences-jobs input.select2-search__field', 1000)
    client.sendKeys('#test-sequences-jobs input.select2-search__field', client.Keys.ENTER)
    .pause(250)
    // Custom merge tags
    client.waitForElementVisible('textarea#test-custom-merge-tag-1', 1000)
    client.setValue('textarea#test-custom-merge-tag-1', 'artster')

    // No of days
    client.useXpath()
    .waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div[2]/div[3]/div[2]/div[2]/div[1]/div[2]/select[1]', 1000)
    .click('//*[@id="app"]/div[1]/div[2]/div[2]/div[3]/div[2]/div[2]/div[1]/div[2]/select[1]')
    client.pause(250)
    .sendKeys('//*[@id="app"]/div[1]/div[2]/div[2]/div[3]/div[2]/div[2]/div[1]/div[2]/select[1]', client.Keys.DOWN_ARROW)
    .sendKeys('//*[@id="app"]/div[1]/div[2]/div[2]/div[3]/div[2]/div[2]/div[1]/div[2]/select[1]', client.Keys.ENTER)

    // Reply To
    client.useCss()
    .waitForElementVisible('input#test-candidate-reply1', 1000)
    .setValue('input#test-candidate-reply1', 'Anand')

    .waitForElementVisible('input#test-candidate-reply2', 1000)
    .setValue('input#test-candidate-reply2', 'anand.dharne24@gmail.com')

    // subject
    .waitForElementVisible('input#test-subject', 1000)
    .setValue('input#test-subject', 'test email')

    // Test for custom merge tag
    .useCss()
    .waitForElementVisible('div.fr-element.fr-view', 1000)
    .click('div.fr-element.fr-view')
    client.pause(250)
    .setValue('div.fr-element.fr-view', '{{custom1}}')
    client.pause(1000)
    .waitForElementVisible('span.preview-button', 1000)
    .click('span.preview-button')
    .pause(500)
    client.waitForAjax('/api/v1/candidate-lists/', function () {

    }, function () {
      client.pause(1500)
      client.waitForElementVisible('.preview-container', 1000)
      client.expect.element('.preview-container').text.to.contain('artster')
    })
    client.waitForElementVisible('img.hp-modal-close-button', 1000)
    client.click('img.hp-modal-close-button')
    .waitForElementVisible('div.fr-element.fr-view', 1000)
    .click('div.fr-element.fr-view')
    .waitForElementVisible('div.fr-element.fr-view', 1000)
    .click('div.fr-element.fr-view')
    .pause(250)

    // Email content, testing all except merge tags
    .useXpath()
    .waitForElementVisible('//*[@id="bold-1"]/i', 1000)
    .click('//*[@id="bold-1"]/i')
    client.pause(250)
    .waitForElementVisible('//*[@id="italic-1"]', 1000)
    .click('//*[@id="italic-1"]')
    client.pause(250)
    .waitForElementVisible('//*[@id="underline-1"]', 1000)
    .click('//*[@id="underline-1"]')
    client.pause(250)
    .waitForElementVisible('//*[@id="insertImage-1"]', 1000)
    .click('//*[@id="insertImage-1"]')
    client.pause(250)

    // Insert image by url
    .useXpath()
    .waitForElementVisible('//*[@id="imageByURL-1"]', 1000)
    .click('//*[@id="imageByURL-1"]')
    client.pause(250)
    .waitForElementVisible('//*[@id="fr-image-by-url-layer-text-1"]', 1000)
    .setValue('//*[@id="fr-image-by-url-layer-text-1"]', 'http://i.imgur.com/z9XryfN.jpg')
    client.pause(250)
    .waitForElementVisible('//*[@id="fr-image-by-url-layer-1"]/div[2]/button', 1000)
    .click('//*[@id="fr-image-by-url-layer-1"]/div[2]/button')

    client.pause(250)
    client.useXpath()
    .waitForElementVisible('//*[@id="insertLink-1"]', 1000)
    .click('//*[@id="insertLink-1"]')
    client.pause(250)

    // align
    .click('//*[@id="imageAlign-1"]')
    .sendKeys('//*[@id="imageAlign-1"]', client.Keys.DOWN_ARROW)
    .sendKeys('//*[@id="imageAlign-1"]', client.Keys.ENTER)

    // inline/break text
    .click('//*[@id="imageDisplay-1"]')
    .sendKeys('//*[@id="imageDisplay-1"]', client.Keys.ENTER)
    client.pause(250)
    // style centered or round
    .click('//*[@id="imageStyle-1"]')
    .sendKeys('//*[@id="imageStyle-1"]', client.Keys.DOWN_ARROW)
    .sendKeys('//*[@id="imageStyle-1"]', client.Keys.ENTER)
    client.pause(400)
    client.end()
    // TODO few buttons to assert
  }
}
