// Note: Has a bug which breaks the test so commented out
// module.exports = {
//   ' Settingspage testing' : function (client) {
//     client.waitForAjax('/api/v1/organizations/?all=1', function(){
//     }, function(){
//       client.urlHash('outreach/settings/profile')
//       client.pause(3000)
//       client.urlHash('outreach/settings/company')
//       client.pause(3000)
//       client.useXpath()
//       client.pause(3000)
//       client.urlHash('outreach/settings/organization')
//       client.pause(1000)
//       client.urlHash('outreach/settings/users')
//       // select company and organization
//       client.pause(6000)
//       client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/span/span',6000)
//       client.click('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[2]/div[1]/div/div/div[2]/span/span')
//       client.pause(1000)
//       client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[2]/div[1]/div/div/div[3]/div/ul/li[2]',1000)
//       client.click('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[2]/div[1]/div/div/div[3]/div/ul/li[2]')
//       client.pause(1000)
//       // Invite
//       client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[3]/div[2]/span',1000)
//       client.click('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[3]/div[2]/span')
//       client.pause(1000)
//       // Invite fields
//       client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[5]/form/div[1]/label/div/input',1000)
//       client.setValue('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[5]/form/div[1]/label/div/input','bchase126@gmail.com')
//       client.pause(1000)
//       client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[5]/form/div[4]/button',1000)
//       client.click('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[5]/form/div[4]/button')
//       client.urlHash('outreach/settings/signatures')
//       client.pause(3000)
//       client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[2]/div[2]/div',2000)
//       client.setValue('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[2]/div[2]/div','sd')
//       client.pause(3000)
//       // Click on a signature and delete it
//       client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[1]/div/div/div[2]/span',3000)
//       client.click('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[1]/div/div/div[2]/span')
//       client.pause(2000)
//       client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[1]/div/div/div[3]/div/ul/li[4]',2000)
//       client.click('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[1]/div/div/div[3]/div/ul/li[4]')
//       client.pause(2000)
//       client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[1]/img',2000)
//       client.click('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[1]/img')
//       client.pause(3000)
//       client.waitForElementVisible('//*[@id="app"]/div[2]/span/div/div/div/div/div/form/button[2]',3000)
//       client.click('//*[@id="app"]/div[2]/span/div/div/div/div/div/form/button[2]')
//       client.pause(4000)
//       // Save
//       client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[3]/button',4000)
//       client.click('//*[@id="app"]/div[1]/div[2]/div/div[2]/div[3]/button')
//     })
//   }
// }
