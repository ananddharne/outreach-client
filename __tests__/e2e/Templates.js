module.exports = {
  'Templates page testing': function (client) {
    client.url(client.launchUrl + client.globals.e2e_query_string)
    client.setToken()
    client.urlHash('outreach/templates')
    client.useXpath()
    client.waitForAjax('/api/v1/templates/all/', function () {
    }, function () {
      client.useCss()
      client.waitForElementVisible('span.add-block', 1000)
      client.click('span.add-block')
    })
    // Fill the input field
    client.pause(250)
    client.waitForElementVisible('input#test-templates-input', 1000)
    client.setValue('input#test-templates-input', 'xyx')

    // Description
    client.waitForElementVisible('textarea#test-templates-text-area', 1500)
    client.setValue('textarea#test-templates-text-area', 'sdfg')

    client.useXpath()
    client.waitForElementVisible('//*[@id="app"]/div[1]/div[2]/div[2]/div[2]/div[1]/span[2]/span[2]', 1500)
    client.click('//*[@id="app"]/div[1]/div[2]/div[2]/div[2]/div[1]/span[2]/span[2]')

    // Subject
    client.useCss()
    client.waitForElementVisible('input#test-templates-input-subject', 500)
    client.setValue('input#test-templates-input-subject', 'test email')

    // Email content, testing all except merge tags
    client.useXpath()
    client.waitForElementVisible('//*[@id="bold-1"]/i', 1000)
    client.click('//*[@id="bold-1"]/i')
    client.pause(250)
    client.waitForElementVisible('//*[@id="italic-1"]', 1000)
    client.click('//*[@id="italic-1"]')
    client.pause(250)
    client.waitForElementVisible('//*[@id="underline-1"]', 1000)
    client.click('//*[@id="underline-1"]')
    client.pause(250)
    client.waitForElementVisible('//*[@id="insertImage-1"]', 1000)
    client.click('//*[@id="insertImage-1"]')
    client.pause(250)

    // Insert image by url
    client.waitForElementVisible('//*[@id="imageByURL-1"]', 1000)
    client.click('//*[@id="imageByURL-1"]')
    client.pause(250)
    client.waitForElementVisible('//*[@id="fr-image-by-url-layer-text-1"]', 500)
    client.setValue('//*[@id="fr-image-by-url-layer-text-1"]', 'http://i.imgur.com/z9XryfN.jpg')
    client.waitForElementVisible('//*[@id="fr-image-by-url-layer-1"]/div[2]/button', 1500)
    client.click('//*[@id="fr-image-by-url-layer-1"]/div[2]/button')
    client.pause(500)
    client.useXpath()
    client.waitForElementVisible('//*[@id="insertLink-1"]', 1000)
    client.click('//*[@id="insertLink-1"]')
    client.pause(250)

    // Replace TODO
    // client.useXpath()
    // client.clearValue('//*[@id="fr-image-by-url-layer-text-1"]')
    // client.setValue('//*[@id="fr-image-by-url-layer-text-1"]', 'http://i.imgur.com/g1TF62D.jpg')
    // client.click('//*[@id="fr-image-by-url-layer-1"]/div[2]/button')

    // align
    client.click('//*[@id="imageAlign-1"]')
    client.sendKeys('//*[@id="imageAlign-1"]', client.Keys.DOWN_ARROW)
    client.sendKeys('//*[@id="imageAlign-1"]', client.Keys.ENTER)

    // inline/break text
    client.click('//*[@id="imageDisplay-1"]')
    // .sendKeys('//*[@id="imageDisplay-1"]',client.Keys.DOWN_ARROW)
    client.sendKeys('//*[@id="imageDisplay-1"]', client.Keys.ENTER)

    // style centered or round
    client.click('//*[@id="imageStyle-1"]')
    client.sendKeys('//*[@id="imageStyle-1"]', client.Keys.DOWN_ARROW)
    client.sendKeys('//*[@id="imageStyle-1"]', client.Keys.ENTER)

    // // Deliberately click on subject input field for RTE tols to be visible
    client.useCss()
    client.waitForElementVisible('input#test-templates-input-subject', 1000)
    client.click('input#test-templates-input-subject')
    client.pause(250)
    // Align RTE
    client.useXpath()
    client.waitForElementVisible('//*[@id="align-1"]', 1000)
    client.click('//*[@id="align-1"]')
    client.sendKeys('//*[@id="align-1"]', client.Keys.ENTER)

    // Ordered list
    client.waitForElementVisible('//*[@id="formatOL-1"]', 1000)
    client.click('//*[@id="formatOL-1"]')
    client.pause(250)

    // unordered list
    client.waitForElementVisible('//*[@id="formatUL-1"]', 1000)
    client.click('//*[@id="formatUL-1"]')
    client.pause(250)

    // Increase indent
    client.waitForElementVisible('//*[@id="indent-1"]', 1000)
    client.click('//*[@id="indent-1"]')
    client.pause(250)

    // Formatting
    client.waitForElementVisible('//*[@id="clearFormatting-1"]', 1000)
    client.click('//*[@id="clearFormatting-1"]')
    client.pause(250)

    // Undo
    client.waitForElementVisible('//*[@id="undo-1"]', 1000)
    client.click('//*[@id="undo-1"]')
    client.end()
  }
}
