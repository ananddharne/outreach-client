

module.exports = {
  'Candidate manager page testing': function (client) {
    client.url(client.launchUrl + client.globals.e2e_query_string)
    .setToken()
    .urlHash('outreach/candidate-manager')
    .useCss()
    .pause(250)
    .waitForElementVisible('span.options-header-block', 1000)
    .click('span.options-header-block')
    .waitForAjax('/api/v1/candidate-lists/', function () {
    //
    }, function () {
      // Filter by job
      client.pause(250)
      client.waitForElementVisible('#test-filter-jobs span.label', 1000)
      client.click('#test-filter-jobs span.label')
      client.pause(500)
      client.waitForElementVisible('#test-filter-jobs .selectric-items li[data-index="1"]', 1000)
      client.click('#test-filter-jobs .selectric-items li[data-index="1"]')
      client.pause(250)
    //   // Filter by status
    //   // cant use css coz it conflicts with the job image click
      client.waitForElementVisible('#test-filter-statuses span.label', 1000)
      client.click('#test-filter-statuses span.label')
      client.pause(250)
      client.waitForElementVisible('#test-filter-statuses .selectric-items li[data-index="1"]', 1000)
      client.click('#test-filter-statuses .selectric-items li[data-index="1"]')
    //   // Filter by list
      client.pause(2000)
      client.waitForElementVisible('#test-filter-lists span.label', 1000)
      client.click('#test-filter-lists span.label')
      client.pause(2000)
      client.waitForElementVisible('#test-filter-lists .selectric-items li[data-index="1"]', 1000)
      client.click('#test-filter-lists .selectric-items li[data-index="1"]')
    })
    // // Add new
    .waitForElementVisible('button.flat-button.green', 1000)
    .click('button.flat-button.green')
    client.pause(1000)
    client.execute(function () {
      $('.custom-multi-select:eq(0)').parent().find('input').click()
    })
    client.pause(500)
    .waitForElementVisible('li.select2-results__option.select2-results__option--highlighted', 1000)
    client.click('li.select2-results__option.select2-results__option--highlighted')
    .pause(1000)
    client.execute(function () {
      $('.custom-multi-select:eq(1)').parent().find('input').click()
    })
    client.pause(500)
    .waitForElementVisible('li.select2-results__option.select2-results__option--highlighted', 1000)
    client.click('li.select2-results__option.select2-results__option--highlighted')

    // First Name
    .waitForElementVisible('input#test-candidate-manager-fname-input', 1000)
    .setValue('input#test-candidate-manager-fname-input', 'Anand')
    // Last Name
    .waitForElementVisible('input#test-candidate-manager-lname-input', 1000)
    .setValue('input#test-candidate-manager-lname-input', 'PSG')

    // LINKEDIN URL
    .waitForElementVisible('input#test-candidate-manager-linkedin-input', 1000)
    .setValue('input#test-candidate-manager-linkedin-input', 'https://linkedin.com/in/ananddharne')

    // Facebook
    .waitForElementVisible('input#test-candidate-manager-fb-input', 1000)
    .setValue('input#test-candidate-manager-fb-input', 'https://www.facebook.com/anand.dharne')

    client.pause(250)
    // Title
    .waitForElementVisible('input#test-candidate-manager-title-input', 1000)
    .setValue('input#test-candidate-manager-title-input', 's')

    client.pause(250)
    // Tital alias
    .waitForElementVisible('input#test-candidate-manager-title-alias-input', 1000)
    .setValue('input#test-candidate-manager-title-alias-input', 'xyx')

    // Company name

    .waitForElementVisible('input#test-candidate-manager-company-name-input', 1000)
    .setValue('input#test-candidate-manager-company-name-input', 'wse')

    client.pause(250)

    // Email
    .waitForElementVisible('input#test-candidate-manager-email-input', 1000)
    .setValue('input#test-candidate-manager-email-input', 'anand@gethappie.me')

    // Alternate EMAIL

    .waitForElementVisible('input#test-candidate-manager-alternate-email-input', 1000)
    .setValue('input#test-candidate-manager-alternate-email-input', 'anand.dharne24@gmail.com')

    // ANother alternate Email
    .waitForElementVisible('input#test-candidate-manager-alternate-email-1-input', 1000)
    .setValue('input#test-candidate-manager-alternate-email-1-input', 'surveys4anand@anand@gmail.com')

    // Phone number
    .waitForElementVisible('input#test-candidate-manager-phone-number-input', 1000)
    .setValue('input#test-candidate-manager-phone-number-input', '6462098331')

    // Street address 1
    .waitForElementVisible('input#test-candidate-manager-street-address-input', 1000)
    .setValue('input#test-candidate-manager-street-address-input', '203 uni ave')

    .waitForElementVisible('input#test-candidate-manager-street-address-2-input', 1000)
    .setValue('input#test-candidate-manager-street-address-2-input', 'apt 2')

    // city
    .waitForElementVisible('input#test-candidate-manager-city-input', 1000)
    .setValue('input#test-candidate-manager-city-input', 'Lowell')

    // STate

    .waitForElementVisible('input#test-candidate-manager-state-input', 1000)
    .setValue('input#test-candidate-manager-state-input', 'Massachusetts')

    // Postal code

    .waitForElementVisible('input#test-candidate-manager-zip-code-input', 1000)
    .setValue('input#test-candidate-manager-zip-code-input', '01854')

    // Tracking info
    .pause(500)
    client.execute(function () {
      $('.button:eq(8)').click()
    })
    .pause(500)
    // Select the first option from dropdown
    client.execute(function () {
      $('.selectric-scroll:eq(8) li[data-index="1"]').click()
    })
  .pause(500)
  // Tracking info-2
  .execute(function () {
    $('.button:eq(9)').click()
  })
  .pause(500)
  // Select the first option from dropdown
  .execute(function () {
    $('.selectric-scroll:eq(9) li[data-index="1"]').click()
  })
    // // Close the profile editor
    .execute(function () {
      $('.x-out-button').click()
    })
    .pause(500)
    // // Confirmation, do you really? blah blah
    .execute(function () {
      $('.prompt-button:eq(1)').click()
    })
    // // TODO moving the candidate profile tile from/to different sections
    client.end()
  }
}
