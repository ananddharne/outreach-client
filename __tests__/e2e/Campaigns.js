module.exports = {
  'Campaigns page testing': function (client) {
    client.url(client.launchUrl + client.globals.e2e_query_string)
    client.setToken()
    client.urlHash('outreach/campaigns')
    client.waitForAjax('/api/v1/campaigns/', function () {

    }, function () {
      client.waitForElementVisible('span.add-block', 1000)
      client.execute(function () {
        document.querySelector('span.add-block').click()
      })
      client.pause(250)
      // Type text into the input container after "Add Campaign"
      client.waitForElementVisible('input#test-campaigns-input', 1000)
      client.setValue('input#test-campaigns-input', 'testanand')
      client.useCss()
      client.waitForElementVisible('input#test-campaigns-start-date', 1000)
      client.click('input#test-campaigns-start-date')
      // Select todays date
      client.pause(250)
      client.waitForElementVisible('span.cell.day.selected', 1000)
      client.click('span.cell.day.selected')
      // Description of Campaign
      client.waitForElementVisible('textarea#test-campaigns-text-area', 1000)
      client.setValue('textarea#test-campaigns-text-area', 'No brainer campaign')
      // Select end date
      client.waitForElementVisible('input#test-campaigns-end-date', 1000)
      client.click('input#test-campaigns-end-date')
      client.execute(function () {
        $('.cell.day.selected:eq(1)').click()
      })

      // // jobs
      client.execute(function () {
        $('.select2-search__field:eq(0)').click()
      })
      // Hit ENTER to select the first job
      client.keys(client.Keys.ENTER)

      // // pages
      client.execute(function () {
        $('.select2-search__field:eq(1)').click()
      })
      // Hit ENTER to select the first page
      client.keys(client.Keys.ENTER)
      // // Lists
      client.execute(function () {
        $('.select2-search__field:eq(2)').click()
      })
      // Hit ENTER to select the first list
      client.keys(client.Keys.ENTER)
      client.pause(500)
      // // Sequencees
      client.execute(function () {
        $('.select2-search__field:eq(3)').click()
      })
      // Hit ENTER to select the first list
      client.keys(client.Keys.ENTER)

      client.pause(500)
      // // Save the campaign
      client.useCss()
      client.waitForElementVisible('span.campaign-save-button', 1000)
      client.click('span.campaign-save-button')
      client.pause(500)
      // // Delete the campaign
      client.waitForElementVisible('span.campaign-delete-button', 1000)
      client.click('span.campaign-delete-button')
      client.pause(500)
      // // Confirmation for delete
      client.execute(function () {
        $('button:contains(Yes, delete it!)').click()
      })
    })
    client.end()
  }
}
