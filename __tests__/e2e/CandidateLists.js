module.exports = {
  'Candidate lists page testing': function (client) {
    client.url(client.launchUrl + client.globals.e2e_query_string)
    client.setToken()
    client.urlHash('outreach/lists')
    client.waitForAjax('/api/v1/candidate-lists/', function () {
    }, function () {
      client.pause(500)
      .useCss()
      .waitForElementVisible('span.add-block', 1000)
      .click('span.add-block')

      .execute(function () {
        $('#test-all-loadercontainer').remove()
      })
    })
    // Add block functionality and 4 divs testing
    client.pause(1500) // Safety pause cause sporadically error occurs here
    // Import a list
    client.useCss()
    .execute(function () {
      $('button.attention-button:eq(0)').click()
    })
    client.pause(1000)
    .execute(function () {
      $('#test-all-loadercontainer').remove()
    })
    client.pause(1000)
    client.useXpath()
    .waitForElementVisible('//*[@id="app"]/div[2]/span/div[2]/div/div/div/img', 1000)
    .click('//*[@id="app"]/div[2]/span/div[2]/div/div/div/img')
    client.pause(500)

    // Create Empty list
    .execute(function () {
      $('#test-all-loadercontainer').remove()
    })
    .execute(function () {
      $('button.attention-button:eq(1)').click()
    })
    .execute(function () {
      $('#test-all-loadercontainer').remove()
    })
    client.pause(250)
    client.useXpath()
    .waitForElementVisible('//*[@id="name"]', 1000)
    .setValue('//*[@id="name"]', 'TestList')
    .waitForElementVisible('//*[@id="description"]', 1000)
    .setValue('//*[@id="description"]', 'xyz')
    .waitForElementVisible('//*[@id="app"]/div[2]/span/div[2]/div/div/div/img', 1500)
    .execute(function () {
      $('#test-all-loadercontainer').remove()
    })
    .click('//*[@id="app"]/div[2]/span/div[2]/div/div/div/img')
    client.pause(250)

    // Find Candidates
    .execute(function () {
      $('button.attention-button:eq(2)').click()
    })
    .execute(function () {
      $('#test-all-loadercontainer').remove()
    })
    // Find contact info
    .execute(function () {
      $('button.attention-button:eq(3)').click()
    })
    client.pause(1500) // Safety pause cause sporadically error occurs here
    client.waitForAjax('/api/v1/emails/send/', function () {
    }, function () {
      client.useXpath()
      client.waitForElementVisible('//*[@id="app"]/div[2]/span/div[2]/div/div/div/div/div/div[1]/div[2]/span', 1500)
      client.click('//*[@id="app"]/div[2]/span/div[2]/div/div/div/div/div/div[1]/div[2]/span')
    })
    client.end()
  }
}
