// This file exends the .eslintrc.js found in the project root directory by including the testing environment namespaces.

module.exports = {
  'env': {
    'mocha': true,
    'jasmine': true,
  },
  'rules': {
    'eqeqeq': 'warn'
  }
}
