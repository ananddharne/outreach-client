var loginCommands = {
  switchToDevOrg: function () {
    this.api.pause(250)
    .waitForElementVisible('.organization-dropdown', 1000)
    .click('.organization-dropdown')
    .pause(250)
    .execute(function () {
      $('.organization-dropdown li:contains(Dev)').click()
    })
  },
  switchToTestingOrg: function () {
    this.api.pause(250)
    .waitForElementVisible('.organization-dropdown', 1000)
    .click('.organization-dropdown')
    .pause(250)
    .execute(function () {
      $('.organization-dropdown li:contains(Testing)').click()
    })
  }
}

module.exports = {

  url: function () {
    return this.api.launchUrl + '/#/?e2e_testing_mode=1'
  },
  commands: [loginCommands],
  elements: {
    username: {
      selector: '//*[@id="identifierId"]',
      locateStrategy: 'xpath'
    },
    password: {
      selector: '//*[@id="password"]/div[1]/div/div[1]/input',
      locateStrategy: 'xpath'
    }
  }
}
